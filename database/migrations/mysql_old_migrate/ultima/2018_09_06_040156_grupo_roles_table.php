<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GrupoRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_roles', function (Blueprint $table) {
            $table->integer('id_role')->unsigned();
            $table->foreign('id_role')
            ->references('id')->on('roles')
            ->onDelete('cascade');
            $table->integer('id_grupo')->unsigned();
            $table->foreign('id_grupo')
            ->references('id')->on('grupopermisos')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('grupo_roles', function (Blueprint $table) {
            //
        });
    }
}
