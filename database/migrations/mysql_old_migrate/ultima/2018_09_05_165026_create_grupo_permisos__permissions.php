<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupoPermisosPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_permission')->unsigned();
            $table->integer('id_grupo')->unsigned();
            $table->foreign('id_permission')
            ->references('id')->on('permissions')
            ->onDelete('cascade');
            $table->foreign('id_grupo')
            ->references('id')->on('grupopermisos')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_permiso');
    }
}
