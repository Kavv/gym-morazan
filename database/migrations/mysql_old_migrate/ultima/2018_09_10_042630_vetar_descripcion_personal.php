<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VetarDescripcionPersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vetado', function (Blueprint $table) {
            $table->renameColumn('descripcion','descripcionCliente');
            $table->string('descripcionPersonal',300)->after('descripcion')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vetado', function (Blueprint $table) {
            //
        });
    }
}
