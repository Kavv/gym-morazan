<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePrimaryKeyPersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Eliminamos las restricciones foraneas
        Schema::table('vetado',function(Blueprint $table){
            $table->dropForeign('fk_veta_per');
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->dropForeign('articulo_danado_ibfk_3');
        });
        Schema::table('personal_evento', function (Blueprint $table) {
            $table->dropForeign('personal_evento_ibfk_2');
        }); 
        Schema::table('cargo_personal', function (Blueprint $table) {
            $table->dropForeign('fk_ced_personal');
        });
        //Eliminar la restriccion de pk
        Schema::table('personal', function(Blueprint $table){
            $table->dropPrimary('Cedula_Personal');
        });
        //Creamos la nueva PK
        Schema::table('personal', function(Blueprint $table){
            $table->increments('id')->first();
        });

        //Crear una nueva columna para la FK
        Schema::table('vetado', function (Blueprint $table) {
            $table->integer('personal_id')->unsigned()->after('ID_Vetado')->nullable();
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->integer('personal_id')->unsigned()->after('cliente_id')->nullable();//Cambiar a not null luego
        });
        Schema::table('personal_evento', function (Blueprint $table) {
            $table->integer('personal_id')->unsigned()->first()->nullable();//Cambiar a not null luego
        });
        Schema::table('cargo_personal', function (Blueprint $table) {
            $table->integer('personal_id')->unsigned()->first()->nullable();//Cambiar a not null luego
        });

        //Asignar las nuevas restricciones de la FK
        Schema::table('vetado',function(Blueprint $table){
            $table->foreign('personal_id')->references('id')->on('personal');
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->foreign('personal_id')->references('id')->on('personal');
        });
        Schema::table('personal_evento', function (Blueprint $table) {
            $table->foreign('personal_id')->references('id')->on('personal');
        });
        Schema::table('cargo_personal', function (Blueprint $table) {
            $table->foreign('personal_id')->references('id')->on('personal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
