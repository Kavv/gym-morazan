<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyOfClient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Creamos los campos para las llaves foraneas
        Schema::table('vetado', function (Blueprint $table) {
            $table->integer('cliente_id')->unsigned()->after('IdPersonal')->nullable();
        });
        Schema::table('reservacion', function (Blueprint $table) {
            $table->integer('cliente_id')->unsigned()->after('ID_Reservacion')->nullable();//Cambiar a not null luego
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->integer('cliente_id')->unsigned()->after('ID_Objeto')->nullable();//Cambiar a not null luego
        });
        //Agregamos las restricciones de llave foranea
        Schema::table('vetado', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('cliente');
        });
        Schema::table('reservacion', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('cliente');
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
