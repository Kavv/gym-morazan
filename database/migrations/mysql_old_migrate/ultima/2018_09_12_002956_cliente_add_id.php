<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClienteAddId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //eliminamos las restricciones de las llaves foraneas
        Schema::table('vetado', function (Blueprint $table) {
            $table->dropForeign('fk_veta_cli');
        });
        Schema::table('reservacion', function (Blueprint $table) {
            $table->dropForeign('fk_ced_client');
        });
        Schema::table('articulo_danado', function (Blueprint $table) {
            $table->dropForeign('articulo_danado_ibfk_2');
        });
        //Eliminamos la pripiedad de llave primaria
        Schema::table('cliente', function (Blueprint $table) {
            $table->dropPrimary('Cedula_Cliente');
        });
        //Agregamos una nueva llave primaria
        Schema::table('cliente', function (Blueprint $table) {
            $table->increments('id')->first();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cliente', function (Blueprint $table) {
            //
        });
    }
}
