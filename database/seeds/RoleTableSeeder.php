<?php

use Caffeinated\Shinobi\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'SuperAdmin',
            'slug' => 'super_admin',
            'description' => 'Adminstrador del sistema',
            'special' => 'all-access',
        ]);

        Role::create([
            'name' => 'Administrador',
            'slug' => 'administrador',
            'description' => 'Usuario con todos los permisos',
            'special' => null,
        ]);
    }
}
