<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'kavv',
            'name' => 'Kevin Antonio Valverde Varela',
            'description' => 'SuperAdmin',
            'password' => Hash::make('123456'),
        ]);
        User::create([
            'email' => 'fguzman',
            'name' => 'Felipe Genaro Guzman Vilchez',
            'description' => 'SuperAdmin',
            'password' => Hash::make('123456'),
        ]);
        User::create([
            'email' => 'jromero',
            'name' => 'Juan Ramon Junior Romero Cano',
            'description' => 'SuperAdmin',
            'password' => Hash::make('123456'),
        ]);
    }
}
