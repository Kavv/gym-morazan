<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Contracts\Repositories\HostnameRepository;
use Hyn\Tenancy\Contracts\Repositories\WebsiteRepository;
use Hyn\Tenancy\Environment;
use Hyn\Tenancy\Database\Connection;
use App\cliente;

class CreateTenant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:create-tenant {uuid} {fqdn}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creacion de website y hostname';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        parent::__construct();
    } */

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $uuid = $this->argument("uuid");
        $fqdn = $this->argument("fqdn");
        //$email = $this->argument('email');
        $website = new Website;
        $website->uuid = $uuid;
        $website->managed_by_database_connection = 'system';
        app(WebsiteRepository::class)->create($website);

        $hostname = new Hostname;
        $hostname->fqdn = $fqdn;
        app(HostnameRepository::class)->attach($hostname, $website);

        $tenancy = app(Environment::class);

        $tenancy->hostname($hostname);

        $tenancy->hostname(); // resolves $hostname as currently active hostname

        $tenancy->tenant($website); // switches the tenant and reconfigures the app

        $tenancy->website(); // resolves $website
        $tenancy->tenant(); // resolves $website

        $tenancy->identifyHostname(); // resets resolving $hostname by using the Request 
    }
}
