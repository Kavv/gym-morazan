<?php

namespace App\DatabaseConnection;

use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Environment;
class Connection {
    protected $hostname;
    protected $website;
    protected $environment;
    public function __construct(Hostname $hostname, Website $website)
    {
        $this->hostname = $hostname;
        $this->website = $website;
        $this->environment = app(Environment::class);
        $this->environment->hostname($this->hostname);
        $this->environment->tenant($this->website);
    }
}