<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class cargo_personal extends Model
{
    
    protected $table="positions_staff";
    public $incrementing = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=[
        'staff_id','position_id'
    ];
}
