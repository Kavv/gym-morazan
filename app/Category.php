<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    protected $table='categories';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'title', 'slug'
    ];

    //Retornar la categoria padre
    public function Imagenes()
    {
        return $this->hasMany('App\MultimediaWeb','categories_id','id');
    }
}
