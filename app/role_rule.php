<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class role_rule extends Model
{
    protected $table = 'roles_rules';
    protected $fillable = [
        'rule_id','role_id'
    ];
}
