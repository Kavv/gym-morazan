<?php

namespace App\Http\Controllers;

use App\Phone;
use App\Phone_staff;
use Illuminate\Http\Request;
use App\Position;
use App\cargo_personal;
use App\Staff;
use App\Http\Requests\CargoAdd;
use Session;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class CargosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    //Muestra todos los cargos
    public function index($msj=null,$nombre="")
    {

        //cuidado se te olvida borrar lo de arriba
        $cargos=Position::all();

        if($msj=="2")
        {
            Session::flash('message','Cargo editado exitosamente');
            Session::flash('tipo','info');
        }
        if($msj=="1")
        {
            Session::flash('message','Cargo agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$nombre);
        return view('cargo.index',compact('cargos'));
    }

    //listado de personal por el cargo
    public function list(request $request){

        $id = $request->id;
        //consulta de la position
        $position = Position::where('id', '=', $id)->first();
        //se utiliza la relacion para pasar la tabla pivote y obtener los personales de ese cargo
        $data = $position->personal;
        //no recuerdo si lo uso
        $data->toArray();
        //eto si
        $cont = count($data);

        for($i = 0; $i < $cont; $i++){
            //obtener los datos del personal
            $personal[$i] = Staff::where('id', '=', $data[$i]->id)->first();
            //obtener los telefonos del personal con la relacion de la tabla pivote
            $collect[$i] = $personal[$i]->phones;
        }

        foreach ($data as $i=>$datum) {
            // definir el tamaño para ver si tiene mas de un numero
            $cont_inter = count($collect[$i]);
            try{
                //si tiene mas de un numero
                if($cont_inter > 1){
                    for($j = 0; $j < $cont_inter; $j++){
                        //creo un arreglo para insertarlo directamente al json que cree
                        $b[$j] = $collect[$i][$j]->number;
                        //creacion de json
                        $resp [$i] = [
                            'dni' => $datum->dni,
                            'name' => $datum->name,
                            'last_name' => $datum->last_name,
                            'phone' => $b
                        ];
                    }
                }
                // si solo tiene uno
                else{
                    //creacion del json
                    $resp [$i] = [
                        'dni' => $datum->dni,
                        'name' => $datum->name,
                        'last_name' => $datum->last_name,
                        'phone' => $collect[$i][0]->number,
                    ];
                }
            //si no tiene elementos que hace que se rompa la consulta
            }catch (\Exception $e){
                //creacion del json
                $resp [$i] = [
                    'dni' => $datum->dni,
                    'name' => $datum->name,
                    'last_name' => $datum->last_name,
                    'phone' => 0,
                ];
            }
        }

        return response()->json($resp);
    }

    //Muestra la vista de crear
    public function create()
    {
        $Cargos=Position::all();
        $personal=Staff::all();
        return view("cargo.create", compact('Cargos','personal'));
    }
    //Crea un nuevo cargo
    public function store(Request $request)
    {
        $this->validacion($request);
        $consulta= Position::where('name','ilike',$request['Nombre_Cargo'])->Where('deleted_at','=',null)->get();
        if(count($consulta)>0)
            return 0;
        Position::create([
            'name' => $request['Nombre_Cargo'],
        ]);
        return 1;
    }
    //Asigna cargos a los empleados indivualmente o en grupo
    public function guardar(Request $cargos_personal)
    {
        //si hay cargos y personal
        if(count($cargos_personal['personal']) > 0 && count($cargos_personal['cargos']) > 0)
        {
            //Recorremos la lista de personal
            foreach($cargos_personal['personal'] as $id)
            {
                $persona = Staff::find($id);//Buscamos el registro del personal
                $persona->cargos()->syncWithoutDetaching($cargos_personal['cargos']);//Relacionamos los cargos (si ya existe no pasa nada, si no existe lo agrega)
            }
            return 1;
        }
    }
    //Retorna la lista de cargos
    public function recargarlista()
    {
        $Cargos=Position::all();
        return view('cargo.listacargo.listacargo',compact('Cargos'));
    }
    //Elimina cargo
    public function destroy($id)
    {
        if($id == 1)
            return response()->json(['Para mantener el correcto funcionamiento del sistema, Está prohibida la eliminación de este cargo']);
        $cargo=Position::find($id);
        $cargo->delete();
        return 1;
    }
    //Actualiza cargo
    public function update($id, Request $request)
    {
        $this->validacion($request);
        $cargo=Position::find($id);
        if($cargo['name']!=$request['Nombre_Cargo'])
        {
            $consulta=Position::where('name','ilike',$request['Nombre_Cargo'])->get();
            if(count($consulta)==0)
            {
                $cargo['name'] = $request['Nombre_Cargo'];
                $cargo->save();
            }
            else
            {
                return 0;
            }
        }
        return 1;
    }
    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            //Lo cambie el 10-02-19
            //'Nombre_Cargo'=>'required|alpha_spaces|max:40',
            $rule = [
                'Nombre_Cargo'=>'required|max:40',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
