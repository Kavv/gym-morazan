<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use Calendar;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index()
    {
        $reservaciones=Reservation::all();
        $events = [];

        foreach($reservaciones as $re)
        {
            if($re->cliente)
            {
                $events[] = Calendar::event(
                    $re->cliente->name, //event title
                    true, //full day event?
                    new \DateTime($re->begin_date), //start time (you can also use Carbon instead of DateTime)
                    new \DateTime($re->end_date), //end time (you can also use Carbon instead of DateTime)
                    $re->id
                );
            }
        } 

        $calendar = Calendar::addEvents($events,['color'=>'#FFC300']) //add an array with addEvents
            ->setOptions([ //set fullcalendar options
                'firstDay' => 1,
                'selectable' => true
                //'selectHelper' => true,
                //'eventLimit' => true
            ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'eventClick' => 'function(event, jsEvent, view) {
                    cargardatos(event.id);
                    $("#infore").modal("show");
                }'
            ]);

        return view('calendario.index', compact('calendar','reservaciones'));
    }
}
