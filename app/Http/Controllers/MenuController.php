<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Http\Requests\MenuAdd;
use App\Http\Requests\MenuUpdate;
use Session;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
use Aws\S3\S3Client;

class MenuController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($msj=null,$nombre="")
    {
        $menu=Menu::all();
        $carpeta = Auth::user()->hostname->fqdn;
        if($msj=="1")
        {
            Session::flash('message','Comida agregada exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$nombre);
        return view('menu.index',compact('menu','carpeta'));
    }
    public function create()
    {
        return view('menu.create');
    }
    public function store(MenuAdd $request)
    {
        if($request->hasFile('path'))
        {
            $carpeta = Auth::user()->hostname->fqdn;
            $now= new \DateTime();//Capturamos el tiempo actual
            //return 10;
            //La variable name definira el nombre del archivo almacenado
            $name=$now->format('d-m-Y_H_i_s_').$request->file('path')->getClientOriginalName();
            //return 11;
            $s3=\Storage::disk('s3');//Hacemos referencia al disco en la nube
            //return 12;
            $s3->put($carpeta.'/'.$name,\File::get($request->file('path')),'public');
            //\Storage::disk('local')->put($name, \File::get($request->file('path')));
            Menu::create([
                'description'=>$request->get('descripcion'),
                'price'=>$request->get('costo'),
                'image_name'=>$name
            ]);
        }
        else
        {
            Menu::create([
                'description'=>$request->get('descripcion'),
                'price'=>$request->get('costo'),
            ]);
        }
        return 1;
    }
    public function update($id,MenuUpdate $request)
    {
        $comida=Menu::find($id);
        if($request->get('sinimagen')!=null)
        {
            $comida['description']=$request->get('descripcion');
            $comida['price']=$request->get('costo');
            
            //Obtenemos el nombre de la carpeta en el S3 que es equivalente al fqdn del tenant
            $carpeta = Auth::user()->hostname->fqdn;
            //Si la carpeta y el archivo existen, entonces lo elimina
            if(\Storage::disk('s3')->exists($carpeta.'/'.$comida['image_name'])){
                \Storage::disk('s3')->delete($carpeta.'/'.$comida['image_name']);
            }

            $comida['image_name']="vacio.jpg";
            $comida->save();
            return response()->json(
                "vacio.jpg"
            );
        }
        if($request->hasFile('path'))
        {    
            $carpeta = Auth::user()->hostname->fqdn;
            $now= new \DateTime();//Capturamos el tiempo actual
            $name=$now->format('d-m-Y_H_i_s_').$request->file('path')->getClientOriginalName();//La variable name definira el nombre del archivo almacenado
            $s3=\Storage::disk('s3');//Hacemos referencia al disco en la nube
            $s3->put($carpeta.'/'.$name,\File::get($request->file('path')),'public');
            $comida['description']=$request->get('descripcion');
            $comida['price']=$request->get('costo');
            $comida['image_name']=$name;
            $comida->save();
            return response()->json(
                $name
            );
        }
        else
        {
            $comida['description']=$request->get('descripcion');
            $comida['price']=$request->get('costo');
            $comida->save();
            return 1;
        }
    }
    public function destroy($id)
    {
        $comida=Menu::find($id);
        if($comida['image_name']!="vacio.jpg")
        {
            //Obtenemos el nombre de la carpeta en el S3 que es equivalente al fqdn del tenant
            $carpeta = Auth::user()->hostname->fqdn;
            //Si la carpeta y el archivo existen, entonces lo elimina
            if(\Storage::disk('s3')->exists($carpeta.'/'.$comida['image_name'])){
                \Storage::disk('s3')->delete($carpeta.'/'.$comida['image_name']);
            }
        }
        $comida->delete();
        return 1;
    }
}
