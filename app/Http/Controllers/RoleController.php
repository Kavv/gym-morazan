<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Caffeinated\Shinobi\Models\Role;
use App\Role;
use Caffeinated\Shinobi\Models\Permission;
use App\permission_rule;
use App\Rule;
use App\role_rule;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class RoleController extends Controller
{
    public function index()
    {
        //$permisos = Permission::all();
        $roles = Role::where('name','!=','SuperAdmin')->get();
        $permisos = Rule::all();
        $valor = null;
        return view('role.index',compact('roles','permisos','valor'));
    }

    public function create()
    {
        // Busca todos los permisos existentes
        $permisos = Rule::all();
        return view('role.create', compact('permisos'));
    }

    public function store(Request $request)
    {
        $this->validacion($request);

        $hostname_id = Auth::user()['hostname_id'];
        $existe = Role::where('name','ilike', $request['nombre'])->get();
        
        if(count($existe) != 0)
        {
            return response()->json(['El nombre del role ya existe! ingrese un nombre diferente']);
        }

        $role = new Role;
        $role->name = $request['nombre'];
        $role->slug = str_replace(" ", "_",strtolower($request['nombre']));
        $role->description = $request['descripcion'];
        $role->special = null;
        
        if($request->get('permiso'))
        {
            $Arreglo = [];
            foreach($request->get('permiso') as $indice=>$permiso)
            {
                $Arreglo[$indice] = permission_rule::select('permission_id')->where('rule_id','=', $permiso)->get();
            }
            $permisos= [];
            foreach($Arreglo as $fila)//recorre cada "fila"
            {
                foreach($fila as $p)
                {
                    array_push($permisos,$p->permission_id);
                }
            } 
            $role->save();
            $role->permissions()->sync($permisos); 
            $role->grupos()->sync($request->get('permiso')); 
        }
        $role->save();
        return 1;

    }

    public function show(Role $role)
    {
        return response()->json($role->grupos);
    }

    public function update(Request $request, Role $role)
    {
        $this->validacion($request);

        $hostname_id = Auth::user()['hostname_id'];
        $existe = Role::where('name','ilike',$request['nombre'])
                        ->where('id','!=',$role['id'])->get();
        if(count($existe) != 0)
        {
            return response()->json(['No se edito! El nombre del rol esta en uso']);
        }

        $role->name = $request['nombre'];
        $role->slug = str_replace(' ', '_', strtolower($request['nombre']));
        $role->description = $request['descripcion'];
        $Arreglo = [];
        $permisos= [];
        if($request->get('permiso'))
        {
            foreach($request->get('permiso') as $indice=>$permiso)
            {
                $Arreglo[$indice] = permission_rule::select('permission_id')->where('rule_id','=', $permiso)->get();
            }
            foreach($Arreglo as $fila)//recorre cada "fila"
            {
                foreach($fila as $p)
                {
                    array_push($permisos,$p->permission_id);
                }
            } 
        }
        $role->permissions()->sync($permisos); 
        $role->grupos()->sync($request->get('permiso'));
        $role->save();
        return 1;
    }


    public function destroy(Role $role)
    {
        $role->delete();
        return 1;
    }

    public function ver($valor=null)
    {
        $roles = Role::where('name','!=','SuperAdmin')->get();
        $permisos = Rule::all();
        return view('role.index',compact('roles','permisos','valor'));
    }

    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [                
                'nombre' => "required | max:191",
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
