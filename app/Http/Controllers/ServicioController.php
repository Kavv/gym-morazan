<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Http\Requests\ServicioAdd;
use Session;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
class ServicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($msj=null,$nombre="")
    { 
        $servicios=Service::all();
        if($msj=="2")
        {
            Session::flash('message','Servicio editado exitosamente');
            Session::flash('tipo','info');
        }
        if($msj=="1")
        {
            Session::flash('message','Servicio agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$nombre);
        return view('servicio.index',compact('servicios'));
    }
    
    public function create()
    {
        return view('servicio.create',['message'=>""]);
    }
    public function store(Request $request)
    { 
        $this->validacion($request);
        $servicio = Service::where('name','ilike',$request['Nombre'])->first();
        if($servicio)
            return response()->json(['El nombre del servicio ya existe!']);
        Service::create([    
            'name'=> $request['Nombre'],
            'amount'=> $request['monto'],
        ]);

        return 1;
    }

    public function destroy($id)
    {
        $servicio=Service::find($id);
        $servicio->delete();
        return 1;
    }

    public function update($id,Request $request)
    {
        $this->validacion($request);
        $servicio=Service::find($id);
        if($servicio['name']!=$request['Nombre'])
        {
            $consulta=Service::where('name','ilike',$request['Nombre'])->get();
            if(count($consulta)==0)
            {
                $servicio['name'] = $request['Nombre'];
                $servicio->save();
            }
            else
            {
                return 0;
            }
        }
        return 1;
    }
    
    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Nombre'=>'required|max:50',
                'monto'=>'required'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
