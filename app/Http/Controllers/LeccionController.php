<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Staff;
use App\Day;
use App\Hour;
use App\Lesson;
use App\Calendar;
use App\Discipline;


class LeccionController extends Controller
{
    public function index()
    {
        $lessons = Lesson::join('staff', 'staff_id', '=', 'staff.id')
        ->join('disciplines', 'discipline_id', '=', 'disciplines.id')
        ->select('lessons.id', 'lessons.code', 'lessons.base', 'lessons.max', 
        'staff.name', 'staff.last_name', 'staff.dni', 'disciplines.name as discipline')
        ->get();
        $services = Service::all();
        $staff = Staff::all();
        $days = Day::all();
        $disciplines = Discipline::all();
        return view('leccion.index', compact('lessons','services', 'staff', 'days', 'disciplines'));
    }

    public function create()
    {
        $services = Service::all();
        $staff = Staff::all();
        $days = Day::all();
        $hours = Hour::all();
        $disciplines = Discipline::all();
        return view('leccion.create', compact('services', 'staff', 'days', 'hours', 'disciplines'));
    }

    public function store(Request $request)
    {
        // Validamos la clase
        $this->validacion($request);

        $exist = false;
        // Validamos que si especifico la hora de inicio tambien especifico la hora de fin y viceversa
        foreach($request['desde'] as $index => $begin)
        {
            $end = $request['hasta'][$index];
            if( ($begin != null && $end == null) || ($begin == null && $end != null) )
            {
                abort(response()->json([
                    'code' => 2,
                    'msg' => ["Si especificó la hora de inicio, tambien debe especificar hora de fin"] ,
                ], 200));
            }
            // Verificamos si almenos un par de horas exista
            if($begin != null && $end != null)
                $exist = true;
        }
        // Si no existe ningun rango de horas valido entonces retornamos un error
        if(!$exist)
        {
            abort(response()->json([
                'code' => 2,
                'msg' => ["Debe especificar almenos 1 horario"] ,
            ], 200));
        }
        // Creamos el registro de la lección
        $lesson = Lesson::create([
            'code' => $request['codigo'],
            'base' => $request['alumnos_base'],
            'max' => $request['alumnos_excedentes'],
            'staff_id' => $request['profesor'],
            'discipline_id' => $request['disciplina'],
        ]);
        // Si existen los guardamos en la variable, si no entonces inicializamos un arreglo vacio
        if(isset($request['servicios']))
            $services = $request['servicios'];
        else
            $services = [];
        // Sincronizamos los servicios de la lección
        $lesson->services()->sync($services);
        
        // Recorremos cada uno de los díás
        foreach($request['dias'] as $index => $day)
        {
            // Obtenemos el rango de hora correspondiente al día
            $begin = $request['desde'][$index];
            $end = $request['hasta'][$index];
            // si el inicio es null no se asignara el día a la sesión
            if($begin != null)
            {
                $lesson->calendars()->create([
                    'day' => $day,
                    'begin' => $begin,
                    'end' => $end,
                ]);
            }
        }

        return response()->json([
            'code' => 1,
        ]);
    }

    public function show($id)
    {
        $lesson = Lesson::find($id);
        $calendar = $lesson->calendars()
                    ->select('calendars.id', 'day', 'begin', 'end')
                    ->orderBy('day', 'asc')->get();
        $services = $lesson->services()->get();
        return response()->json([
            'lesson' => $lesson,
            'services' => $services,
            'calendar' => $calendar,
        ]);
    }

    public function update(Request $request, $id)
    {

        // Validamos la clase
        $this->validacion($request);

        $exist = false;
        // Validamos que si especifico la hora de inicio tambien especifico la hora de fin y viceversa
        foreach($request['desde'] as $index => $begin)
        {
            $end = $request['hasta'][$index];
            if( ($begin != null && $end == null) || ($begin == null && $end != null) )
            {
                abort(response()->json([
                    'code' => 2,
                    'msg' => ["Si especificó la hora de inicio, tambien debe especificar hora de fin"] ,
                ], 200));
            }
            // Verificamos si almenos un par de horas exista
            if($begin != null && $end != null)
                $exist = true;
        }
        // Si no existe ningun rango de horas valido entonces retornamos un error
        if(!$exist)
        {
            abort(response()->json([
                'code' => 2,
                'msg' => ["Debe especificar almenos 1 horario"] ,
            ], 200));
        }
        // Creamos el registro de la lección
        $lesson = Lesson::find($id);
        $lesson->code = $request['codigo'];
        $lesson->base = $request['alumnos_base'];
        $lesson->max = $request['alumnos_excedentes'];
        $lesson->staff_id = $request['profesor'];
        $lesson->discipline_id = $request['disciplina'];
        $lesson->save();
        
        // Si existen los guardamos en la variable, si no entonces inicializamos un arreglo vacio
        if(isset($request['servicios']))
            $services = $request['servicios'];
        else
            $services = [];
        // Sincronizamos los servicios de la lección
        $lesson->services()->sync($services);
        
        // Recorremos cada uno de los díás
        foreach($request['id_calendar'] as $index => $calendar)
        {
            $begin = $request['desde'][$index];
            $end = $request['hasta'][$index];
            if($calendar != null)
            {
                $data_calendar = Calendar::where('id', '=', $calendar)->withTrashed()->first();
                if($begin == "" || $begin == null)
                {
                    $data_calendar->delete();
                }
                else
                {
                    if($data_calendar->delete_at == null)
                        $data_calendar->restore();

                    $data_calendar->begin = $begin;
                    $data_calendar->end = $end;
                    $data_calendar->save();
                }
            }
            else
            {
                if($begin != "")
                {
                    $day = $request['dias'][$index];
                    $lesson->calendars()->create([
                        'day' => $day,
                        'begin' => $begin,
                        'end' => $end,
                    ]);
                }
            }
        }

        return response()->json([
            'code' => 1,
        ]);
    }

    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        $lesson->delete();

        return response()->json([
            'code' => 1,
        ]);
    }

    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'codigo' => 'required|max:20',
                'alumnos_base' => 'required|numeric',
                'alumnos_excedentes' => 'required|numeric',
                'disciplina' => 'required',
                'servicios' => 'required',
                'profesor' => 'required',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

    function calendar($id)
    {
        $lesson = Lesson::find($id);
        $calendars = $lesson->calendars()
                    ->select('day', 'begin', 'end')
                    ->orderBy('day', 'asc')->get();
        // Como los id de los días comienzan desde 1, el indice 0 no se utiliza
        $days = ['', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
        return view('leccion.recargable.listarcalendario', compact('calendars', 'days'));
    }

    function services($id, $discipline = false)
    {
        if(!$discipline)
        {
            $lesson = Lesson::find($id);
            $services = $lesson->services()->get();
        }
        else
        {
            $discipline = Discipline::find($id);
            $services = $discipline->services()->get();
        }
        
        return view('leccion.recargable.listarservicios', compact('services'));
    }
}
