<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\Descripcion;
use App\Service;
use App\Item;
use App\DesRe;
use App\Customer;
use App\Menu;
use App\invenDes;
use App\menudes;
use App\Http\Requests\AddReserva;
use App\Http\Requests\MenuAdd;
use Redirect;
use PDF;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class ReservacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    //retornamos todos los servicios, menu y cliente a la vista para agregar una reservacion nueva
    public function index()
    {
        $reservas=Reservation::all();
        return view('reservacion.index',compact('reservas'));
    }

    public function create($cedula=null)
    {
        /*Retorna todos los datos de la tabla reservacion e inventario de la BD a index*/
        $servicios = Service::all();
        $menu = Menu::all();
        $carpeta = Auth::user()->hostname->fqdn;
        $cliente = ["dni"=>null, "name"=>null, "address"=>null];
        if($cedula != null)
            $cliente = Customer::where('dni','=',$cedula)->first();
        return view('reservacion.create',compact('servicios','cliente','menu','carpeta'));
    }


    //Guarda la reservacion o genera el pdf de la factura
    public function store(AddReserva $request)
    { 
        //return response()->json($request);
        if($request['accion'])//Si es guardar
        {
            $Arreglo=$request['lista'];
            $cliente = Customer::where('dni','=',$request['Cedula_Cliente'])->get();
            //Agregamos a la tabla reservacion
            $reservacion = Reservation::create([
                'customer_id' => $cliente[0]['id'],
                'event_address'=>$request['Direccion_Local'],
                'tax'=>$Arreglo[count($Arreglo)-1][1],
                'discount'=>$Arreglo[count($Arreglo)-1][4],
                'invoice_row'=>$Arreglo[count($Arreglo)-1][3],
                'begin_date'=>$request['Fecha_Inicio'],
                'end_date'=>$request['Fecha_Fin'],
            ]);
            //Hacemos un recorrido en el arreglo que posee toda la descripcion de la factura para poder agregar cada unda de las descripciones
            for($i=0;$i<(count($Arreglo)-1);$i++)//restamos 1 porque la ultima fila es especial
            {
                if(substr($Arreglo[$i][5], -4)=="Arti")
                {
                    $articulo_id = substr($Arreglo[$i][5],0,-4);
                    $reservacion->items()->attach([
                        $articulo_id => [
                            'quantity' => $Arreglo[$i][1],
                            'unit_price' => $Arreglo[$i][2],
                            'day' => $Arreglo[$i][3],
                            'amount' => $Arreglo[$i][4],
                            'index' => $Arreglo[$i][6]
                        ]
                    ]);
                }
                else
                {
                    $menu_id = substr($Arreglo[$i][5],0,-4);
                    $reservacion->menu()->attach([
                        $menu_id => [
                            'quantity' => $Arreglo[$i][1],
                            'unit_price' => $Arreglo[$i][2],
                            'day' => $Arreglo[$i][3],
                            'amount' => $Arreglo[$i][4],
                            'index' => $Arreglo[$i][6]
                        ]
                    ]);
                }
            }
            return 1;
        }
        else
        {
            $x = explode(",", $request["lista"]);//Guardamos como arreglo unidimencional
            $i = 0;
            $Arreglo = [];
            $fila = [];
            $CC = $request['Cedula_Cliente'];
            $NC = $request['Nombre_Contacto'];
            $DL = $request['Direccion_Local'];
            $FI = $request['Fecha_Inicio'];
            $FF = $request['Fecha_Fin'];
            $facactual = $request['facactual'];
            //return $x;
            foreach($x as $elemento)//Recorremos el arreglo
            {
                if($i==7)//hacemos la agregacion de fila cada 6 elementos
                {
                    array_push($Arreglo,$fila);
                    $fila=[];//inicializamos
                    $i=0;
                }
                if($i!=5)//No almacenamos el id
                    array_push($fila,$elemento);//Arreglo de cada fila
                $i++;
            }
            //return $fila;
            array_push($Arreglo,$fila);//agregamos la ultima fila que posee 3 elementos
            //cargamos la vista
            //return $Arreglo;
            $pdf=PDF::loadView('reservacion.fac', compact('CC', 'NC', 'DL','Arreglo','facactual'));
            $now = new \DateTime();
            //definimos el nombre por defecto del archivo
            return $pdf->stream('proforma'.$now->format('Y-m-d_H_i_s').'.pdf');
//            return view('reservacion.fac', compact('CC', 'NC', 'DL','FI', 'FF','Arreglo','facactual'));
        }
    }


    public function edit($id)
    {
        $servicios=Service::all();
        $menu=Menu::all();
        $carpeta = Auth::user()->hostname->fqdn;
        $reservacion=Reservation::find($id);
        $inventario=Item::where('rental_price','>',0)->get();

        $reservaciones = Reservation::whereBetween('begin_date',[$reservacion->begin_date, $reservacion->end_date])->get();
        $reserved_i = [];
        foreach($reservaciones as $reservas)
        {
            $items = $reservas->items;
            foreach($items as $item)
            {
                $item_id = $item->id;
                $quantity = $item->qu;
                if(array_key_exists($item_id, $reserved_i))
                    $reserved_i[$item_id] += $quantity;
                else
                    $reserved_i[$item_id] = $quantity;
            }
        }
        $item_list = $reservacion->items()->select('items_reservations.*')->orderBy('index')->get();
        $menu_list = $reservacion->menu()->select('menu_reservations.*')->orderBy('index')->get();
        
        $reservacion['cliente'] = $reservacion->cliente()->select('name','last_name','dni')->first();
        //$reservacion['cliente'] = $reservacion['cliente'][0];
        
        //Cambio de formato para las fechas
        $begin = strtotime($reservacion['begin_date']);
        $reservacion['begin_date'] = date('Y-m-d',$begin);
        $end = strtotime($reservacion['end_date']);
        $reservacion['end_date'] = date('Y-m-d',$end); 

        $articulo = view('reservacion.tablas.articulos',compact('inventario','reserved_i'));
        return view('reservacion.edit',compact('reservacion','servicios','menu','articulo','item_list','menu_list','carpeta'));
    }
    public function update(AddReserva $request,$id)
    {
        //actualizar datos de la reservacion
        $Arreglo = $request['lista'];
        $cliente = Customer::where('dni',$request['Cedula_Cliente'])->get();
        $reservacion = Reservation::find($id);
        $reservacion['customer_id'] = $cliente[0]['id'];
        $reservacion['begin_date'] = $request['Fecha_Inicio'];
        $reservacion['end_date'] = $request['Fecha_Fin'];
        $reservacion['event_address'] = $request['Direccion_Local'];
        $reservacion['tax'] = $Arreglo[count($Arreglo)-1][1];
        $reservacion['discount'] = $Arreglo[count($Arreglo)-1][4];
        $reservacion['invoice_row'] = $Arreglo[count($Arreglo)-1][3];

        $now = new \DateTime();
        $listaItems = [];
        $reservacion->items()->detach();
        $reservacion->menu()->detach();
        
        for($i=0;$i<(count($Arreglo)-1);$i++)//restamos 1 porque la ultima fila es especial
        {
            if(substr($Arreglo[$i][5], -4)=="Arti")
            {
                $articulo_id = substr($Arreglo[$i][5],0,-4);
                $reservacion->items()->attach([
                    $articulo_id => [
                        'quantity' => $Arreglo[$i][1],
                        'unit_price' => $Arreglo[$i][2],
                        'day' => $Arreglo[$i][3],
                        'amount' => $Arreglo[$i][4],
                        'index' => $Arreglo[$i][6]
                    ]
                ]);
            }
            else
            {
                $menu_id = substr($Arreglo[$i][5],0,-4);
                $reservacion->menu()->attach([
                    $menu_id => [
                        'quantity' => $Arreglo[$i][1],
                        'unit_price' => $Arreglo[$i][2],
                        'day' => $Arreglo[$i][3],
                        'amount' => $Arreglo[$i][4],
                        'index' => $Arreglo[$i][6]
                    ]
                ]);
            }
        }
        $reservacion->save();
        return 1;
 
    }

    public function destroy($id)
    {
        $reservacion =  Reservation::find($id);
        if($reservacion)
            $reservacion->delete();
        return 1;
    }

    public function show($id)
    {
        $reservacion = Reservation::find($id);
        $begin = strtotime($reservacion['begin_date']);
        $reservacion['begin_date'] = date('Y-m-d',$begin);
        $end = strtotime($reservacion['end_date']);
        $reservacion['end_date'] = date('Y-m-d',$end); 

        if($reservacion)
        {
            $cliente = $reservacion->cliente;
            $descripcionItems = $reservacion->items()->orderBy('index')->get();
            $descripcionMenu = $reservacion->menu()->orderBy('index')->get();
            $datos = [
                'reservacion' => $reservacion,
                'cliente' => $cliente,
                'd_items' => $descripcionItems,
                'd_menu' => $descripcionMenu
            ];
            return $datos;
        }
        return 0;
    }

    //Guardamos la nueva comida 
    public function menu(MenuAdd $request)
    {    
        if($request->hasFile('path'))
        {
            $carpeta = Auth::user()->hostname->fqdn;
            $now= new \DateTime();//Capturamos el tiempo actual
            //La variable name definira el nombre del archivo almacenado
            $name=$now->format('d-m-Y_H_i_s_').$request->file('path')->getClientOriginalName();
            $s3=\Storage::disk('s3');//Hacemos referencia al disco en la nube
            $s3->put($carpeta.'/'.$name,\File::get($request->file('path')),'public');
            //\Storage::disk('local')->put($name, \File::get($request->file('path')));
            $nuevo = Menu::Create([
                'description' => $request->get('descripcion'),
                'price' => $request->get('costo'),
                'image_name' => $name
            ]);
        }
        else
        {
            $nuevo = Menu::Create([
                'description' => $request->get('descripcion'),
                'price' => $request->get('costo'),
                'image_name' => 'vacio.jpg'
            ]);
        }
        return response()->json($nuevo); 
    }
    //Recarga los articulos del inventario con la cantidad maxima rentada de cada uno
    public function recargaarticulos($FI,$FF)
    {
        //Obtenemos las reservaciones existentes en ese rango de fecha
        $reservaciones = Reservation::whereBetween('begin_date',[$FI,$FF])->get();
        $reserved_i = [];
        //Recorremos cada una de las reservaciones
        foreach($reservaciones as $reserva)
        {
            //Obtenemos los items de inventario utilizados en la reservación
            $items = $reserva->items;
            //Recorremos los items
            foreach($items as $item)
            {
                $item_id = $item->id;
                $quantity = $item->qu;
                //El indice del arreglo es equivalente al id de un item
                //Por lo tanto si ya existe es porque otra reservación rento este articulo
                //De no existir entonces lo agrega
                if(array_key_exists($item_id, $reserved_i))
                    $reserved_i[$item_id] += $quantity;
                else
                    $reserved_i[$item_id] = $quantity;

            }
        }
        //Retorna todos los items del inventario
        $inventario = Item::where('rental_price','>',0)->get();
        //Enviamos los items del inventario y las cantidades de items reservados
        return view('reservacion.tablas.articulos', compact('inventario','reserved_i'));
    }

    public function proforma(Request $data)
    {
        $reservation = Reservation::find($data["reservation"]);
        $items = $reservation->items()->orderBy('index')->get();
        $menu = $reservation->menu()->orderBy('index')->get();
        $index_invoice = $data["facactual"];
        $customer = $reservation->cliente;
        $Arreglo = [];
        
        $pdf=PDF::loadView('reservacion.proforma', compact('reservation', 'index_invoice','customer','items','menu'));
        $now = new \DateTime();

        //definimos el nombre por defecto del archivo
        return $pdf->stream('proforma'.$now->format('Y-m-d_H_i_s').'.pdf');
    }
}
