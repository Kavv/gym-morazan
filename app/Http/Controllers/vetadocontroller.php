<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vetoed;
use App\Customer;
use App\Staff;
use App\Item;
use App\Service;
use Caffeinated\Shinobi\Models\Role;
use App\Http\Requests\VetadoAdd;
use App\Discharged;
use DB;
use Auth;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant

class VetadoController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        //Enviamos el host y el website relacionado al usuario actual
//        $this->middleware(function ($request, $next) {
//            $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
//            return $next($request);
//        });
    }
    public function index($tipo='cliente',$view="vetado.indexc",$cedu=null)
    {
        //Si mostrase en la lista tambien la direccion, en el js no tendria que hacer una consulta al metodo descripcion ya que tendria los datos desde la misma tabla
        if($tipo == "cliente")
        {
            $vetados=Customer::where('vetoed_id','!=',null)->get();
        }
        if($tipo == "personal")
        {
            $vetados = Staff::where('vetoed_id','!=',null)->get();
        }
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('vetados','inventario','servicios'));

    }

    public function update($cedu,request $datos)//Actualizar la descripcion del vetado
    {
        $this->validacion($datos);
    
        if($datos['tipo']=="cliente")
        {
            $cliente = Customer::where('dni','=',$cedu)->get();
            $cliente[0]->vetado()->update([ 'description' => $datos['descripcion'] ]);
            /* $vetado=Vetoed::where('IdCliente','=',$cedu)->get();
            $vetado[0]['descripcionCliente']=$datos['descripcion'];
            $vetado[0]->save(); */
            return 1;
        }
        if($datos['tipo']=='personal')
        {
            $personal = Staff::where('dni','=',$cedu)->get();
            $personal[0]->vetado()->update([ 'description' => $datos['descripcion'] ]);
            return 1;
        }
        return "error";
    }
    
    public function listapersonal($view="vetado.createp")
    {
        /* $arreglo=[];
        $personalvetado=Vetoed::where('IdPersonal','!=',null)->get();//obtenemos la lista de clientes vetados
        foreach($personalvetado as $pv)//recorremos la lista con el fin de:
        {
            array_push($arreglo,$pv['IdPersonal']);//almacenar las cedulas de clientes en un arreglo
        }
        //ejecutamos una consulta diciendo que tomara todos los registros menos los que esten contenidos en el arreglo
        $personal = DB::table('personal')
                    ->where('deleted_at','=',null)
                    ->whereNotIn('Cedula_Personal', $arreglo)->get();
        return view($view,compact('personal')); */
        $personal = Staff::where('vetoed_id','=',null)->get();
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('personal','inventario','servicios'));
    }
    public function listacliente($view="vetado.createc")
    {
/*         $arreglo=[];
        $clientevetado=Vetoed::where('IdCliente','!=',null)->get();//obtenemos la lista de clientes vetados
        foreach($clientevetado as $cv)//recorremos la lista con el fin de:
        {
            array_push($arreglo,$cv['IdCliente']);//almacenar las cedulas de clientes en un arreglo
        }
        //ejecutamos una consulta diciendo que tomara todos los registros menos los que esten contenidos en el arreglo
        $clientes = DB::table('cliente')
                    ->where('deleted_at','=',null)
                    ->whereNotIn('Cedula_Cliente', $arreglo)->get(); */
        $clientes = Customer::where('vetoed_id','=',null)->get();
        $inventario = Item::all();
        $servicios = Service::orderBy('name','asc')->get();
        return view($view,compact('clientes','inventario','servicios'));
    }
    public function vetar(Request $datos)
    {
        
        $this->validacion($datos);
        /* 
        //Creamos el arreglo con los id de los items
        $items = [];
        $i_cant = $datos["i_cant"];
        $i_des= $datos["i_des"];
        $length = count($i_cant);
        for($i = 0; $i < $length; $i++)
        {
            if($i_cant[$i] != null)
            {
                if(isset($i_des[$i]))
                    array_push($items,[$i,$i_cant[$i], $i_des[$i]]);//id, cantidad, descripción
                else
                    array_push($items,[$i,$i_cant[$i], '']);
                
            }
        } */
            
        if($datos->get("tipo")=="cliente")
        {

            // Obtenemos el cliente al cual se vetara
            $cliente = Customer::where('dni','=',$datos->get("dni"))->first();
            /* $length = count($items);
            //Realizamos la agregacion de articulos dañados
            for($i = 0; $i < $length; $i++)
            {
                Discharged::create([
                    'item_id' => $items[$i][0],
                    'customer_id' => $cliente['id'],
                    'quantity' => $items[$i][1],
                    'description' => $items[$i][2]
                ]);
            } */
            //Creamos el registro vetado y luego lo asociamos al cliente
            $cliente->vetado()->associate(Vetoed::create([
                'description' => $datos['descripcion']
            ]));
            $cliente->save();
            return 1;
        }
        if($datos->get("tipo") == "personal")
        {
            // Obtenemos el personal al cual se vetara
            $personal=Staff::where('dni','=',$datos->get("dni"))->first();
            
            /* $length = count($items);
            //Realizamos la agregacion de articulos dañados
            for($i = 0; $i < $length; $i++)
            {
                Discharged::create([
                    'item_id' => $items[$i][0],
                    'staff_id' => $personal['id'],
                    'quantity' => $items[$i][1],
                    'description' => $items[$i][2]
                ]);
            } */

            //Creamos el registro vetado y luego lo asociamos al cliente
            $personal->vetado()->associate(Vetoed::create([
                'description' => $datos['descripcion']
            ]));
            $personal->save();
            return 1;
        }
        return "error";
    }

    public function descripcion($tipo="cliente",$cedula=null)
    {
        if($tipo=="cliente")
        {
            /* $vetado=Vetoed::select('c.Nombre','c.Apellido','c.Cedula_Cliente','c.Edad','c.Sexo','c.Direccion','vetado.descripcionCliente','vetado.ID_Vetado')
            ->join('cliente as c','c.Cedula_Cliente','=','vetado.IdCliente')
            ->where('IdCliente','=',$cedu)->get(); */
            $vetado = Customer::where('dni','=',$cedula)->first();
        }
        if($tipo=="personal")
        {
            $vetado = Staff::where('dni','=',$cedula)->first();
        }
        if($vetado)
        {
            $items = $vetado->discharged;
            return response()->json([
                'vetado' => $vetado,
                'items' => $items
                ]);
        }
        else
            return response()->json([
                'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
            ]);
    }
    public function destroy($cedu,request $datos)
    {
        if($datos['tipo']=='cliente')
        {
            $cliente=Customer::where('dni','=',$cedu)->get();
            $cliente[0]->vetado()->delete();
            $cliente[0]->vetado()->dissociate();
            $cliente[0]->save();
        }
        if($datos['tipo']=='personal')
        {
            $personal=Staff::where('dni','=',$cedu)->get();
            $personal[0]->vetado()->delete();
            $personal[0]->vetado()->dissociate();
            $personal[0]->save();
        }
        return 1;
    }
    
    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'descripcion' => 'required|max:200'
            ];
        }
        $this->validate($objeto, $rule, $message);
    }
}
