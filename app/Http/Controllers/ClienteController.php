<?php

namespace App\Http\Controllers;
use App\Exports\CustomerExport;//clase creada con unn comando de exports
use App\Exports\ReservationExport;
use App\Reservation;
use App\Staff;
use Illuminate\Http\Request;
use App\Customer;
use App\Phone;
use App\Service;
use App\Status;
use App\Lesson;
use App\Movement;
use App\Calendar;
use App\Http\Requests\clienteAdd;
use App\Http\Requests\clienteUpdate;
use Maatwebsite\Excel\Facades\Excel;//paquete para realizar los exports
use Session;
use App\DatabaseConnection\Connection;//Clase que realiza la conneccion con el tenant
use Auth;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ClienteController extends Controller
{
    protected $Connection;
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($msj=null,$cedula="")
    { 
        $clientes=Customer::join('status', 'status.id', '=', 'status_id')
                            ->leftJoin('customers_lessons', 'customers_lessons.customer_id', '=', 'customers.id')
                            ->leftJoin('lessons', 'customers_lessons.lesson_id', '=', 'lessons.id')
                            ->select('customers.*', 'status.name as state', 'lessons.code')->get();
        $services = Service::all();
        $status = Status::all();
        if($msj=="2")
        {
            Session::flash('message','Cliente editado exitosamente');
            Session::flash('tipo','info');
        }
        if($msj=="1")
        {
            Session::flash('message','Cliente agregado exitosamente');
            Session::flash('tipo','info');
        }
        Session::flash('valor',$cedula);
        return view('cliente.index',compact('clientes', 'services', 'status'));
    }
    public function create()
    {
        $lessons = Lesson::withCount('customers')->get();
        return view('cliente.create',compact('lessons'));
    }
    public function store(Request $request)
    { 
        // Validación general de los datos recibidos
        $this->validacion($request);
        // VALIDACIONES DEL CLIENTE
        // Consultamos si existe un cliente con la cedula recibida
        $cliente = Customer::where('dni','ilike',$request['Cedula_Cliente'])->first();
        // Si existe un cliente entonces retornamos un msj de error
        if($cliente)
        {
            // Si el cliente es vetato entonces retornamos esa información en el msj
            if($cliente->vetoed_id != null)
                return response()->json([
                    'La cedula del cliente ya habia sido registrada!',
                    'El cliente con esta cedula se encuentra vetado' 
                ]);
            else
                return response()->json(['La cedula del cliente se encuentra actualmente en uso!']);
        }
        
        // VALIDACIONES DEL PAGO
        // Esta funcion devuelve el numero de recibo que se utilizara para el movimiento
        $receipt = $this->receipt_number($request['recibo']);
        // Validamos
        // Si el # de recibo es diferente de 0 y diferente de vacio entonces
        if($receipt != 0 && $receipt != "")
        {
            // Verificamos si existe otro movimiento con ese mismo numero de recibo
            $exist = Movement::where('document_id', 1)->where('number', $receipt)->first();
            // En caso de existir devolvemos un mensaje de error
            if($exist)
            {
                return response()->json([
                    'code' => 2,
                    'msg' => "El numero de recibo $receipt ya se encuentra en uso" ,
                ]);
            }
        }
        else
        {
            return response()->json([
                'code' => 2,
                'msg' => "Ocurrio un error al generar el numero de recibo" ,
            ]);
        }
        // AproBamos todas las validaciones

        $password = Hash::make($request['password']);
        // Procedemos a crear el nuevo cliente
        $customer = Customer::create([
            'dni'=>$request['Cedula_Cliente'],    
            'name'=> $request['Nombre']." ",
            'last_name'=>$request['Apellido'],
            'age'=>($request['Edad']),
            'gender'=> $request['Sexo'],
            'address'=> $request['Direccion'],
            'status_id'=> 1,
            'password' => $password,
        ]);

        // Creamos los numeros de telefono
        $this->syncPhones($request,$customer);
        
        // Asignamos la leccion asociada al cliente
        $customer->lessons()->attach($request['leccion']);

        // Asignamos el servicio asociado al cliente
        $customer->services()->attach($request['servicio']);
        
        // Obtenemos todos los horarios de la lección seleccionada
        // Para así poder asigar como base dicho horario al cliente (DE MOMENTO OMITIDO)
        /* $schedules = Calendar::where('lesson_id', '=', $request['leccion'])->get();
        foreach ($schedules as $key => $schedule) {
            $customer->calendars()->attach($schedule->id); 
        } */

        // Obtenemos el usuario actual
        $digitizer = Auth::user()->id;

        // Generamos el primer movimiento/pago del cliente
        $movimiento = Movement::create([
            'document_id' => 1,
            'number' => $receipt,
            'amount' => $request['monto'],
            'date' => $request['fecha_de_pago'],
            'user_id' => $digitizer,
            'customer_id' => $customer->id,
            'lesson_id' => $request['leccion'],
            'service_id' => $request['servicio'],
            'start' => $request['servicio_inicio'],
            'end' => $request['servicio_corte'],
        ]);

        return 1;
    }

    public function destroy($id)
    {
        $usuario=Customer::find($id);
        $usuario->delete();
        return 1;
    }
    public function update($id,Request $request)
    {
        $rules = [
            'Cedula_Cliente'=>'required|max:30',
            'Nombre'=>'required|alpha_spaces|max:30',
            'Apellido'=>'required|alpha_spaces|max:30',
            'Edad'=>'nullable|numeric',
            'Sexo'=>'nullable|alpha|max:15',
            'Direccion' => 'max:200',
        ];
        $this->validacion($request, $rules);
        $customer=Customer::find($id);
        if($customer['dni']!=$request['Cedula_Cliente'])
        {
            // Verificamos si esta disponible
            $consulta=Customer::Where('dni','ilike',$request['Cedula_Cliente'])->get();
            if(count($consulta)==0)
            {
                // editamos el dni
                $customer['dni']=$request['Cedula_Cliente'];
            }
            else
                return 0;
        }
        // Editamos los demas valores
        $customer['name'] = $request['Nombre'];
        $customer['last_name'] = $request['Apellido'];
        $customer['age'] = $request['Edad'];
        $customer['gender'] = $request['Sexo'];
        $customer['address'] = $request['Direccion'];
        $customer['status_id'] = $request['estado'];

        if($request['password'] != "")
        {
            $password = Hash::make($request['password']);
            $customer['password'] = $password;
        }
        $customer->save();

        $this->syncPhones($request,$customer);

        return 1;
    }
    public function show($cedula)
    {
        $cliente=Customer::where('dni','=',$cedula)->first();
        if($cliente)
        {
            if($cliente->vetoed_id != null)
                return response()->json([
                    'data' => $cliente,
                    'message' => ['El cliente ya existe, no podra ser registrado!',
                                  'El cliente con esta cedula se encuentra vetado' ]
                ]);
            else
                return response()->json([
                    'data' => $cliente,
                    'message' => ['El cliente ya existe, no podra ser registrado!']
                ]);
        }
        return response()->json([
            'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
        ]);
    }
    public function detail($id)
    {
        $cliente = Customer::find($id);
        $telefonos = $cliente->phones()->get();
        $servicios = $cliente->services()->get();
        if($cliente)
        {
            return response()->json([
                'data' => $cliente,
                'phones' => $telefonos,
                'services' => $servicios,
            ]);
        }
        return response()->json([
            'message' => ['Opss!... Ocurrio un error, vuelve a intentarlo!']
        ]);
    }


    public function validacion($objeto=null, $rule=null, $message=[])
    {
        if($rule == null)
        {
            $rule = [
                'Cedula_Cliente'=>'required|max:30',
                'Nombre'=>'required|alpha_spaces|max:30',
                'Apellido'=>'required|alpha_spaces|max:30',
                'password'=>'required',
                'Edad'=>'nullable|numeric',
                'Sexo'=>'nullable|alpha|max:15',
                'Direccion' => 'max:200',
                'leccion' => 'required',
                'servicio'=>'required',
                'monto'=>'required|numeric',
                'recibo'=>'numeric|nullable',
                'fecha_de_pago'=>'required|date',
                'servicio_inicio'=>'required|date',
                'servicio_corte'=>'required|date',
            ];
        }
        $this->validate($objeto, $rule, $message);
    }

    public function syncPhones($request=null, $customer)
    {
        $phone_id = [];
        for($i=0; $i < count($request['telefono']); $i++)
        {
            //No poseo una validacion para el telefono, pero si excede los 24 caracteres no se guardara (no da aviso) CAMBIAR A FUTURO
            if($request['telefono'][$i] != "" && strlen($request['telefono'][0])<25 )
            {
                // Verificamos si el telefono existe
                $phone = Phone::where('number', $request['telefono'][$i])->first();
                // En caso de existir solo guardamos en id en un arreglo
                if($phone)
                   $phone_id[$i] = $phone->id;
                else
                {
                    // Si no existe entonces creamos el registro
                    $phone_id[$i] = Phone::create([
                        'number' => $request['telefono'][$i]
                    ]);
                    //y guardamos el id en un arreglo
                    $phone_id[$i] = $phone_id[$i]->id;
                }
            }
        }
        // Si phone_id es diff a null entonces enlazamos los telefonos existentes o nuevos al cliente recien creado
        if($phone_id)
            $customer->phones()->sync($phone_id);
        
    }

    public function telefonos($id)
    {
        $customer = Customer::find($id);
        $phones = $customer->phones;
        return response()->json($phones);
    }

    public function receipt_number($number, $id = 0)
    {
        // Almacenamos el numero de recibo que se envio desde la vista
        $receipt = $number;
        // Si es diferente a 0 y diferente a vacio entonces
        if($receipt != 0 && $receipt != "")
        {
            // Si id es 0 entonces significa que estamos creando agregando un nuevo pago
            // Si id es diff de 0 entonces significa que estamos editando un pago
            if($id == 0)
                // Verificamos si existe un movimiento que ya tenga este numero de recibo en uso
                $exist = Movement::where('document_id', 1)->where('number', $receipt)->first();
            else
                // Verificamos si existe un movimiento diferente al que estamos editando con el mismo numero de recibo
                $exist = Movement::where('document_id', 1)->where('number', $receipt)->where('id', '!=', $id)->first();
            // Si existe entonces retornamos un mensaje de error
            if($exist)
            {
                abort(response()->json([
                    'code' => 2,
                    'msg' => ["El numero de recibo $receipt ya se encuentra en uso"],
                ], 200));
            }
        }
        else
        {
            // Si el numero de recibo es 0 o vacio entonces hacemos uso de "sec_recibo_negativo"
            // Una secuencia creada en pgsql para generar numeros en negativo de manera decreciente (-1, -2, -3)
            // Ya que la creación del movimiento requiere un numero de recibo
            $receipt = DB::select("select nextval('sec_recibo_negativo')");
            $receipt = $receipt[0]->nextval;
        }
        // Retornamos el numero de recibo que se asignara al movimiento
        return $receipt;
    }

}
