<?php

namespace App\Http\Controllers;

use App\Exports\ArtExport;
use App\Exports\EmptyExport;
use App\Exports\itemsExport;
use App\Exports\EventsExport;
use App\Item;
use App\Reservation;
use App\Exports\CustomerExport;//clase creada con unn comando de exports
use Maatwebsite\Excel\Facades\Excel;//paquete para realizar los exports
use App\Exports\ReservationExport;
use Illuminate\Http\Request;

class ReportLController extends Controller
{
    public function index()
    {
        return view("reporte.index2");
    }
    //metodo solo para clientes agregados y eliminados
    public function excel($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);
        $now = new \DateTime();
        if(($valor3 == 'clientes_agregados') || ($valor3 == 'clientes_eliminados'))
            return Excel::download(new CustomerExport($start,$end,$valor3), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
        else
            return Excel::download(new CustomerExport($start,$end,$valor3), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
    }
    //metodo solo para reservaciones agregadas, eventos y ingresos
    public function excel_res($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);
        $now = new \DateTime();


//        $data = Reservation::withTrashed()->whereBetween('created_at',[$start,$end])->get();
//        dd($data);
        if(($valor3 == "reservaciones_agregadas") || ($valor3 == "ingresos"))
        {
            $reservaciones = Reservation::withTrashed()
                ->whereBetween('created_at',[$start,$end])
                ->get();

            $cant = count($reservaciones);
//            dd($end);
//            dd($cant);
            return Excel::download(new ReservationExport($reservaciones, $start, $end, $valor3, $cant), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
        }

    }
    //metodo para eventos
    public function excel_even($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);
        $now = new \DateTime();


//        $data = Reservation::withTrashed()->whereBetween('created_at',[$start,$end])->get();
//        dd($data);
        if(($valor3 == "eventos"))
        {
            $reservaciones = Reservation::withTrashed()
                ->whereBetween('begin_date',[$start,$end])
                ->orWhere(function ($query) use($start, $end) {
                    $query->whereBetween("end_date", [$start, $end]);
                })->get();

            $cant = count($reservaciones);
//            dd($reservaciones);
//            dd($cant);
            return Excel::download(new EventsExport($reservaciones, $start, $end, $valor3, $cant), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');

        }
    }
    //metodo funcional del excel de inventario
    public function excel_inv($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);
        $services = $name = $cant = $p1 = $p2 = $full = [];
        $tests = Item::withTrashed()
//            ->select('name', 'quantity', 'rental_price', 'buy_price')
            ->whereBetween('created_at', [$start, $end])
            ->get();
        $cont = count($tests);
        foreach ($tests as $test)
        {
            array_push($name, $test->name);
            array_push($cant, $test->quantity);
            array_push($p1, $test->rental_price);
            array_push($p2, $test->buy_price);
            array_push($services, $test->servicio->name);
//            array_push($full, $test->name . ' ' . $test->quantity . ' ' . $test->rental_price . ' ' . $test->buy_price . ' ' . $test->servicio->name);
        }
//        dd($full);
        $now = new \DateTime();
        return Excel::download(new itemsExport($start,$end,$valor3,$name,$cant, $p1, $p2, $services, $cont), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
    }

    public function artMasUsado($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos las reservaciones creadas en el rango de tiempo aunque esten borrados
        $reservaciones = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
            ->orWhere(function ($query) use($start,$end) {
                $query->whereBetween("end_date", [$start, $end]);
            })->get();
        //creacion de una coleccion de datos
        $origi  = $ori_cant = $item_fin = $cant_fin = [];

        foreach ($reservaciones as $i => $articulos)
        {
            $cont = count($articulos->items);
            for($j = 0; $j < $cont; $j++)
            {
                array_push($origi, $articulos->items[$j]->name);
                array_push($ori_cant, $articulos->items[$j]->qu);
            }
        }

        //forma en php de quitar elementos repetidos en un arreglo
        $item_fin = array_unique($origi);
        $cont = count($origi);
        //busqueda lineal
        foreach ($item_fin as $i => $item)
        {
            //reset de variable cuando cambia de elemento
            $suma = 0;
            for($j = 0; $j < $cont; $j++)
            {
                //comparacion de elementos
                if($item_fin[$i] == $origi[$j])
                {
                    $suma += $ori_cant[$j];
                    $cant_fin[$i] = $suma;
                }
            }
        }
        //combinacion de arreglos
        $fin = array_combine($cant_fin, $item_fin );

        //forma de ordenar en php de forma descendiente, mantieniendo los datos intactos
        krsort($fin);
        $now = new \DateTime();
        //envio de datos
        return Excel::download(new ArtExport($start,$end,$valor3, $fin), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
    }

    public function buffMasUsado($valor1 = null, $valor2 = null, $valor3 = null)
    {
        //Convertimos la cadena en tiempo
        $start = strtotime($valor1);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($valor2);
        $end = date('Y-m-d G:i:s',$end);

        //retornamos todos las reservaciones creadas en el rango de tiempo aunque esten borrados
        $reservaciones = Reservation::withTrashed()->whereBetween('begin_date',[$start,$end])
            ->orWhere(function ($query) use($start,$end) {
                $query->whereBetween("end_date", [$start, $end]);
            })->get();
        //creacion de una coleccion de datos
        $origi  = $ori_cant = $item_fin = $cant_fin = [];

        foreach ($reservaciones as $i => $buffet)
        {
            $cont = count($buffet->menu);
            for($j = 0; $j < $cont; $j++)
            {
                array_push($origi, $buffet->menu[$j]->description);
                array_push($ori_cant, $buffet->menu[$j]->qu);
            }
        }

        //forma en php de quitar elementos repetidos en un arreglo
        $item_fin = array_unique($origi);
        $cont = count($origi);
        //busqueda lineal
        foreach ($item_fin as $i => $item)
        {
            //reset de variable cuando cambia de elemento
            $suma = 0;
            for($j = 0; $j < $cont; $j++)
            {
                //comparacion de elementos
                if($item_fin[$i] == $origi[$j])
                {
                    $suma += $ori_cant[$j];
                    $cant_fin[$i] = $suma;
                }
            }
        }
        //combinacion de arreglos
        $fin = array_combine($cant_fin, $item_fin );

        //forma de ordenar en php de forma descendiente, mantieniendo los datos intactos
        krsort($fin);
        //envio de datos
        $now = new \DateTime();
        return Excel::download(new ArtExport($start,$end,$valor3, $fin), $valor3 . ' ' . $now->format('Y-m-d_H_i_s'). '.xlsx');
    }

    public function DateValidation($start, $end)
    {
        if($start == null || $end == null || !strtotime($start) || !strtotime($end))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function grafica_inventario($start = null, $end = null, $valor3 = null)
    {
        if(!$this->DateValidation($start, $end))
        {
            return response()->json([
                "state"=>false,
            ]);
        }
        //Convertimos la cadena en tiempo
        $start = strtotime($start);
        //Aplicamos un formato de fecha especifico al tiempo
        //ej: 2019-05-23 15:30:10
        $start = date('Y-m-d G:i:s',$start);
        $end = strtotime($end);
        $end = date('Y-m-d G:i:s',$end);
        //retornamos todos los clientes creados en el rango de tiempo
        $inventarios = Item::withTrashed()
            ->whereBetween('created_at', [$start, $end])
            ->get();

        //Llamamos a la funcion obtenerfechas enviando una lista y retornando un arreglo de fechas ordenadas
        $dataDays = $this->ObtenerFechas($inventarios);
    }

    public function ObtenerFechas($lista)
    {
        //declaramos el arreglo
        $dataDays = [];
//        $ingresos = [];
        //Desde 0 hasta la cantidad de clientes
        for($i = 0; $i < count($lista); $i++ )
        {
            //Hacemos conversion a tiempo
            $fullday = strtotime($lista[$i]->created_at);
            //Convertimos a una fecha con un formato de Año-mes-dia
            $day = date("Y-m-d", $fullday);

            //Existe la key ? como key(indice) utilizamos el dia de creacion del cliente
            if(array_key_exists($day, $dataDays))
            {
                //Si ya existe entonces hay que aumentar la cantidad y asignar el resto de valores
                $dataDays[$day]["cantidad"] = $dataDays[$day]["cantidad"] + 1;
                array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            }
            else
            {
                //Si no existe entonces se deben declarar los indices y asignar el resto de valores
                $dataDays[$day]["cantidad"] = 1;
                $dataDays[$day]["nombre"] = [];
                array_push($dataDays[$day]["nombre"], $lista[$i]->name);
            }
        }
        //Ordenamos de menor a mayor segun la key
        ksort($dataDays);
        dd($dataDays);
    }
}
