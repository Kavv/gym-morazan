<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Auth;
class usuarioAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function messages()
    {
        return [
            'userName.required' => 'El nombre de usuario es requerido.',
            'userName.unique' => 'El nombre del usuario ya esta en uso! ingrese uno diferente.',
            'userName.max' => 'El nombre no debe ser mayor a 30 caracteres.',
            'name.required' => 'El nombre y apellido son requerido.',
            'name.max' => 'El nombre y apellido no debe ser mayor a 30 caracteres.',
            'name.alpha_space' => 'El nombre y apellido debe contener solamente letras y espacios.',
            'password.required' => 'La contraseña es requerida.',
            'password.min' => 'La contraseña debe tener un minimo de 6 caracteres.',
            'description.max' => 'La descripcion no debe ser mayor a 100 caracteres.'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        
        return [
            'userName'=>[Rule::unique('users','email')->where(function ($query) {
                $query->where('deleted_at', null)->where('hostname_id', Auth::user()['hostname_id']);
            }),'required','max:30'],
            'name' => 'required|max:30|alpha_spaces',
            'password'=>'required|min:6',
            'description'=>'max:100',
        ];
    }
}
