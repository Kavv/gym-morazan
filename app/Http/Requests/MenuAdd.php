<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'path.image' => 'El archivo debe ser una imagen',
            'path.max' => 'El archivo es muy pesado, debe ser inferior a 5MB',
        ];
    }

    public function rules()
    {
        return [
            'descripcion'=>'required|string',
            'costo'=>'nullable|numeric',
            'path'=>'image|max:5000',
        ];
    }
}
