<?php

namespace App\Http\Middleware;

use App\DatabaseConnection\Connection;
use Closure;
use Auth;

class ConnectionTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $Connection;

    public function handle($request, Closure $next)
    {
        $this->Connection = new Connection(Auth::user()->hostname,Auth::user()->hostname->website);
        return $next($request);
    }
}
