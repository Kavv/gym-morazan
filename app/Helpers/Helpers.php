<?php 

function active($path, $collapse = null){
    if($collapse == null)
        return request()->is($path) ? 'active' : '';
    else
        return request()->is($path) ? 'show' : '';
}
