<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Staff extends Model
{
    
    protected $table='staff';
    public $primaryKey = 'id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','dni', 'name', 'last_name', 'address', 'age', 'password'
    ];
    //Deberia ser singular "cargo"
    public function cargos()
    {
        return $this->belongsToMany('App\Position','positions_staff','staff_id','position_id');
    }
    
    public function vetado()
    {
        return $this->belongsTo('App\Vetoed','vetoed_id','id');
    }
    
    //Deberia ser singular "phone"
    public function phones()
    {
        return $this->belongsToMany('App\Phone', 'phones_staff', 'staff_id', 'phone_id');
    }
    public function discharged()
    {
        return $this->hasMany('App\discharged','staff_id','id');
    }
    public function lessons()
    {
        return $this->hasMany('App\Lesson','staff_id','id');
    }
}
