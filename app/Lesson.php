<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    protected $table='lessons';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'id','code','base', 'max','service_id','staff_id', 'discipline_id',
    ];

    public function days()
    {
        return $this->belongsToMany('App\Day', 'days_lessons', 'lesson_id', 'day_id');
    }
    
    public function hours()
    {
        return $this->belongsToMany('App\Hour', 'hours_lessons', 'lesson_id', 'hour_id');
    }
    
    public function services()
    {
        return $this->belongsToMany('App\Service', 'lessons_services', 'lesson_id', 'service_id');
    }

    public function customers()
    {
        return $this->belongsToMany('App\Customer', 'customers_lessons', 'lesson_id', 'customer_id');
    }

    public function calendars()
    {
        // Uno a muchos
        return $this->hasMany('App\Calendar','lesson_id','id');
    }
    public function staff()
    {
        // Uno a muchos
        return $this->belongsTo('App\staff','staff_id','id');
    }
}
