<?php

namespace App\Exports;

use App\Reservation;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReservationExport implements WithMultipleSheets, ShouldAutoSize
{

    protected $reservation, $start, $end, $opt, $cant, $data = [];
    public function __construct($reservation, $start,$end,$valor3, $cant)
    {
//        if(($this->opt == "reservaciones_agregadas")) {
        $this->reservation = Reservation::withTrashed()
            ->whereBetween('created_at',[$start,$end])
            ->get();
//            $this->test = 1;
//        }
//        else{
//            $this->reservation = Reservation::withTrashed()
//                ->whereBetween('begin_date', [$start, $end])
//                ->orWhere(function ($query) use ($start, $end) {
//                    $query->whereBetween("end_date", [$start, $end]);
//                })->get();
//            $this->test = 0;
//        }
        $this->start = $start;
        $this->end = $end;
        $this->opt = $valor3;
        $this->cant = $cant;
    }

    public function sheets(): array
    {
        $cant = $this->cant;
        $m_total = 0;
        $ingresos =  $nomb = $cedula = $dni = $res_date =[];

        if($this->opt == "ingresos")
        {
            for ($k = 0; $k < $cant; $k++)
            {
                $factura = ($this->reservation[$k]->invoice_row * 0);
                $items = $this->reservation[$k]->items()->orderBy('index')->get();
                $menu = $this->reservation[$k]->menu()->orderBy('index')->get();
                $filas_totales = count($items) + count($menu);
                $pago = 0;
                $total = 0;
                $index_i = 0; $index_m = 0;
                for($j = 1; $j <= $filas_totales; $j++)
                {
                    if(isset($items[$index_i]))
                    {
                        if($items[$index_i]->index == $j)
                        {
                            $pago += ($items[$index_i]->qu * $items[$index_i]->up * $items[$index_i]->day);
                            $index_i++;
                        }
                    }

                    if(isset($menu[$index_m]))
                    {
                        if($menu[$index_m]->index == $j)
                        {
                            $pago += ($menu[$index_m]->qu * $menu[$index_m]->up * $menu[$index_m]->day);
                            $index_m++;
                        }
                    }
                }

                $iva = $this->reservation[$k]->tax;
                $descuento = $this->reservation[$k]->discount;

                if ($iva == 0 && $descuento == 0) {
                    $total = $pago;
                    //empty porque imprime a como esta,
                    //sin descuento y sin iva
                }
                if ($iva != 0 && $descuento == 0) {
                    $temp_iva = $pago * $iva;
                    $total = $pago + $temp_iva;
                }
                if ($iva == 0 && $descuento != 0) {
                    $temp_descuento = $pago * $descuento;
                    $total = $pago - $temp_descuento;
                }
                if ($iva != 0 && $descuento != 0) {
                    $temp_iva = $pago * $iva;
                    $temp_descuento = $pago * $descuento;
                    $total = $pago - $temp_descuento + $temp_iva;
                }

                array_push($nomb, $this->reservation[$k]->clienteTrash->name." ".$this->reservation[$k]->clienteTrash->last_name);
                array_push($dni, $this->reservation[$k]->clienteTrash->dni);
                array_push($res_date, $this->reservation[$k]->created_at);
                array_push($ingresos, $total);
                $m_total += $total;
            }
        }



        // todo lo que esta abajo de esto es funcional
        $sheets = [];
        if($cant > 0) {
            foreach ($this->reservation as $i => $reservas) {
                //variable ingresos. nombre. dni . rest_date son solo para ingresos
                if ($this->opt == "ingresos")
                    $sheets[] = new PerReservationExport($reservas, $this->start, $this->end, $this->opt, $cant, $i, $ingresos, $m_total, $nomb, $dni, $res_date);
                else
                    $sheets[] = new PerReservation2Export($reservas, $this->start, $this->end, $this->opt, $cant, $i);
                $i++;
            }
            if ($this->opt == "ingresos")
                $sheets[] = new PerReservationExport($reservas, $this->start, $this->end, $this->opt, $cant, $i, $ingresos, $m_total, $nomb, $dni, $res_date);

            return $sheets;
        }
        else{
            $i = 0;
            if ($this->opt == "ingresos")
                for($i = 0; $i<2; $i++)
                    $sheets[] = new EmptyExport($this->start, $this->end, $this->opt, $i);
            else
                $sheets[] = new EmptyExport($this->start, $this->end, $this->opt, $i);

            return $sheets;
        }
    }
}
