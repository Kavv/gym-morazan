<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Rule extends Model
{
    protected $table = 'rules';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $fillable = ['name','description'];
}
