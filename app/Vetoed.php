<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Vetoed extends Model
{
    
    protected $table='vetoed';
    public $primaryKey='id';
    public $incrementing = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [	
        'description',
    ];

    public function cliente()
    {
        return $this->hasOne('App\Customer','vetoed_id','id');
    }
}
