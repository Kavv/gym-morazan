var Farticulo;
var A;
var facturas;
var diff;
//limpia el valor del campo cantidad y agrega el precio preestablesido 
function mostrar(btn)
{
    Farticulo=btn.parentNode.parentNode;//Guardamos la fila seleccionada "<tr>"
    $("#cant").val("");
    $("#precio").val(Tarti.row(Farticulo).data()[2]);//Escribimos el valor por defecto
}
$('#Add').on('shown.bs.modal', function (e) {
    $("#cant").select();
});
function AddToListArticulo()
{
    var articulo = Tarti.row(Farticulo).data();//Guardamos los datos de  la fila
    var cantidadOr=articulo[1];//Cantidad original
    var cantAdd=$("#cant").val()*1;//Cantidad a agregar
    if(cantAdd>0 && cantAdd<=cantidadOr && $("#manual").prop('checked')==false)//validamos el no agregar mas que la existencia
    {
        $("#Add").modal('toggle');//ocultamos el modal
        //Actualizamos la cantidad total del articulo
        Tarti.cell(Farticulo.children[1]).data((cantidadOr)-(cantAdd));
        var id=Farticulo.children[3].children[0].value;
        AddToList({Tabla:TA,articulo:articulo,cantAdd:cantAdd,id:id});
        location.href="#TablaA";
    }
    else
    {
        console.log(cantAdd);
        if(cantAdd=="" && $("#manual").prop('checked')==false)
            message(["No ha ingresado la cantidad"],{objeto:$("#mensajearticulo"),tipo:"danger",manual:true}); 
        else
        {
            if($("#precio").val() == "")
                message(["No ha ingresado el precio"],{objeto:$("#mensajearticulo"),tipo:"danger",manual:true}); 
            else
            {
                if(cantAdd>cantidadOr && $("#manual").prop('checked')==false)
                    message(["Sobre pasa la cantidad existente"],{objeto:$("#mensajearticulo"),tipo:"danger",manual:true});
                else
                {
                    $("#Add").modal('toggle');//ocultamos el modal
                    //Actualizamos la cantidad total del articulo
                    Tarti.cell(Farticulo.children[1]).data((cantidadOr)-(cantAdd));
                    var id=Farticulo.children[3].children[0].value;
                    articulo[2]=$("#precio").val();
                    AddToList({Tabla:TA,articulo:articulo,cantAdd:cantAdd,id:id});
                    location.href="#TablaA";
                }
            }
        }
    }
}

function AddToList({Tabla=null,articulo=null,cantAdd=null,indexname=0,tipo='Arti',id=""}={})
{
    console.log(articulo);
    //verificamos si ya fue agregado el articulo
    if(TA.cell('td[id='+articulo["DT_RowId"]+']').length>0)
    {
        //sumamos la cantidad agregada anteriormente con la actual
        var cantidad= TA.cell('td[id='+articulo["DT_RowId"]+']').data()*1 + cantAdd;//SOLO cantAdd si no quisiera sumar lo previo agregado
        var indexrow=TA.cell('td[id='+articulo["DT_RowId"]+']').index().row;
        var costo=articulo[2];//TA.cell(indexrow,2).data();
        TA.cell(indexrow,1).data(cantidad);//Agregamos la nueva cantidad
        TA.cell(indexrow,2).data(costo);//Agregamos el precio
        TA.cell(indexrow,4).data(costo*diff*cantidad);
    }
    else
    {
        //Agregar la fila
        var newrow = TA.row.add(
        [
            articulo[indexname],
            cantAdd,
            articulo[2],
            $("#dias").val(),
            cantAdd*($("#dias").val()*1)*articulo[2],
            '<button onclick="EraseRow({boton: this,pos:\''+articulo["DT_RowId"]+'\',tipo:\''+tipo+'\'});" value="'+id+tipo+'"class="btn btn-danger">Eliminar</button>'
        ]).draw().node();
        newrow.children[1].id=articulo["DT_RowId"];//Agregamos la clase para la columna de cantidad
        newrow.children[3].className="day";
    }
}

function EraseRow({boton=null,pos=null,tipo=""}={})
{
    deletedrow=TA.row(boton.parentNode.parentNode);
    if(tipo!="Menu")
    {
        valororiginal=Tarti.cell('td[id=c_'+pos+']').data()*1 + deletedrow.data()[1]*1;
        Tarti.cell('td[id=c_'+pos+']').data(valororiginal);
    }
    deletedrow.remove().draw( false ); 
}

function AddToListMenu(boton)
{
    console.log(boton.value);
    if($("#c_menucant"+boton.value).val()*1)
    {
        var articulo = table.row(boton.parentNode.parentNode).data();//Guardamos los datos de  la fila
        var cantAdd=$("#c_menucant"+boton.value).val()*1;//Cantidad a agregar
        AddToList({Tabla:table,articulo:articulo,cantAdd:cantAdd,indexname:1,tipo:"Menu",id:boton.value});
        $("#c_menucant"+boton.value).val("");
        message(["Se agrego a la lista"],{objeto:$("#mensajemenu"), manual:true});
        location.href="#mensajemenu";
    }
}

function Eliminar(btn, menu=false)
{  
        if(menu!=false)
        {
            var cantoriginal=$("#tablaarticulos").children('tr[id="'+ btn.value +'"]').children('td[class="can"]');
            var cantagregada=$("#articuloren").children('tr[id="'+ btn.value +'"]').children('td[class="itemaddcant"]').text()*1;
            cantoriginal.text((cantoriginal.text()*1)+(cantagregada));
            $("#articuloren").children('tr[id="'+ btn.value +'"]').remove();
        }
        else
            $("#articuloren").children('tr[id="'+ btn.value +'"]').remove();
       // document.getElementById("tableid").deleteRow(i);
}
function Enviar()
{
    var f = new Date();
    var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $("#lce").text($("#Cedu").val());
    $("#lnom").text($("#Nom").val());
    $("#ldir").text($("#Dir").val());
    $("#lfi").text($("#datepicker").val());
    $("#lff").text($("#datepicker2").val());
    var lf = $("#lf").text("Fecha: "+f.getDate() + " de " + (meses[f.getMonth()]) + " de " + f.getFullYear());

    row_quantity = $("#filafactura").data().value;
    if(row_quantity == null)
        $("#filafactura").val(TA.data().length);
    else
        $("#filafactura").val(row_quantity);

    if($("#filafactura").val()>0)
    {
        tablafactura();
        A[A.length-1][2]=lf.text();
    }
    else
    {
        $("#artifin").empty();
        A= [[]];
    }
    CambioPag();
}


function tablafactura()
{
    //Limpia la tabla final
    filafac=$("#filafactura").val()*1;
    if(filafac>0 )
    {
        A= [[]];
        $("#artifin").empty();
        aux=filafac;
        cantfac=0;
        var i=0;
        facturas=[[""]];
        html="";
        var pago=0;
        for(i=0; i<TA.data().length; i++)//se repetira hasta la cantidad maxima de filas en las facturas
        {
            if(i==filafac)
            {
                facturas.push(['']);
                filafac+=aux;
            }
            A[i].push(
                TA.row(i).data()[0],
                TA.row(i).data()[1],
                TA.row(i).data()[2],
                TA.row(i).data()[3],
                TA.row(i).data()[4],
                TA.row(i).node().children[5].children[0].value,
                i+1
            );
            html+=
                '<tr>'+
                    '<td>'+(A[i][0])+'</td>'+
                    '<td>'+(A[i][1])+'</td>'+
                    '<td>'+(A[i][2])+'</td>'+
                    '<td>'+(A[i][3])+'</td>'+
                    '<td>'+formatNumber(A[i][4])+'</td>'+
                '</tr>';
            pago+=parseInt(TA.row(i).data()[4]);
            A.push([]);
            if(i==filafac-1)//Si es la ultima linea definida por factura
            {
                facturas[cantfac][0]=html;
                facturas[cantfac].push(pago);
                cantfac++;
                pago=0;
                html="";
            }  
        }
        if(i<filafac)
        {
            for(;i<filafac; i++)
                html+='<tr><td></td><td></td><td></td><td></td><td></td></tr>';
                
            facturas[cantfac][0]=html;
            facturas[cantfac].push(pago);
        }
        $("#artifin").append(facturas[0][0]);
        //en este ultimo push agregamos Pago, iva, fecha, numero de filas, descuento
        A[A.length-1].push(pago,0,$("#lf").text(),aux,0);
        facactual=0;
        $("#numfac").text("Factura #1");
        $("#facactual").val(0);
        eliminardetalles();
    }
    
}

//Elimina las filas subtota, iva y total segun convenga
function eliminardetalles()
{
    var x = $("#artifin");
    var pago=0, descuento = 0, iva = 0;
    
    if($("#iva").prop('checked')==true)//si esta aprovado el uso de iva
        iva = ($("#valoriva").val()*1)/100;//obtenemos el valor del iva a aplicar

    if($("#descuento").prop('checked')==true)//si esta aprovado el uso de descuento
        descuento = ($("#valorDescuento").val()*1)/100;//obtenemos el valor del descuento a aplicar

    x.children('tr[id]').remove();//removemos todas las filas con id (son maximo 4 de esa tabla)
    pago=facturas[facactual][1];//almacenamos el valor del pago total de la factura actual
    ultimodetalle(pago,iva,descuento);//funcion que agregar a la tabla el subtotal, descuento, iva y total segun convenga (pago,iva,descuento)
    
}
//IMPLEMENTAR EL IVA
function ultimodetalle(pago,iva,descuento)
{
    var temp_descuento, temp_iva, total;
    
    if(iva == 0 && descuento == 0)
    {
        $("#artifin").append("</tr >"+
            '<tr id="tftotal"><td> </td><td> </td><td></td><td><b>Gran Total:</b></td><td>'+formatNumber(pago)+'</td></tr>');
        //Almacenamos en decimal el iva actual
        A[A.length-1][1]=iva;
        //Almacenamos en decimal el descuento actual
        A[A.length-1][4]=descuento;
    }
    if(iva != 0 && descuento == 0)
    {   
        temp_iva = (pago*iva).toFixed(2);
        total = ((pago*1)+(temp_iva*1)).toFixed(2);
        $("#artifin").append("</tr >"+
        '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+formatNumber(pago)+'</td> </tr>'+
        '<tr id="tfiva"> <td></td> <td></td> <td> </td><td><b>IVA:</b></td><td>'+formatNumber(temp_iva)+'</td> </tr>'+
        '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+formatNumber(total)+'</td> </tr>');
    
        A[A.length-1][1]=iva;
        A[A.length-1][4]=descuento;
    }
    if(iva == 0 && descuento != 0)
    {
        temp_descuento = (pago*descuento).toFixed(2);
        total = ((pago*1)-(temp_descuento*1)).toFixed(2);
        $("#artifin").append("</tr >"+
        '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+formatNumber(pago)+'</td> </tr>'+
        '<tr id="tfdecuento"> <td></td> <td></td> <td></td> <td> <b>Descuento:('+ (descuento*100) +'%)</b></td><td>'+formatNumber(temp_descuento)+'</td> </tr>'+
        '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+formatNumber(total)+'</td> </tr>');
        
        A[A.length-1][1]=iva;
        A[A.length-1][4]=descuento;
    }
    if(iva != 0 && descuento != 0)
    {
        temp_descuento = (pago*descuento).toFixed(2);
        temp_iva = ((pago-(temp_descuento*1))*iva).toFixed(2);
        total = ((pago*1)-(temp_descuento*1)+(temp_iva*1)).toFixed(2);
        $("#artifin").append("</tr >"+
        '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+formatNumber(pago)+'</td> </tr>'+
        '<tr id="tfdecuento"> <td></td> <td></td> <td></td> <td> <b>Descuento:('+ (descuento*100) +'%)</b></td><td>'+formatNumber(temp_descuento)+'</td> </tr>'+
        '<tr id="tfiva"> <td></td> <td></td> <td> </td><td><b>IVA:</b></td><td>'+formatNumber(temp_iva)+'</td> </tr>'+
        '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+formatNumber(total)+'</td> </tr>');
        
        A[A.length-1][1]=iva;
        A[A.length-1][4]=descuento;
    }
    $("#arre").val(A);
}
function CambioPag()
{
    if($("#parte1").hasClass('active'))
    {
        $("#parte1").removeClass("active");
        $("#parte2").addClass("active");
    }
    else
    {
        $("#parte2").removeClass("active");
        $("#parte1").addClass("active");
        $("#filafactura").val("");
    }
}

facactual=0;
function cambiofac(valor)
{
    temp= facactual + valor * 1;
    if(temp >= 0 && temp < facturas.length)
    {
        facactual=temp;
        tf=$("#artifin");
        tf.empty();
        tf.append(facturas[facactual][0]);
        eliminardetalles();
        $("#numfac").text("Factura #"+(facactual+1));
        $("#facactual").val(facactual);
    }
}
/*  */

function buscar()
{
    cedula=0;
    if($("#Cedu").val() != "")
    {
        cedula=$("#Cedu").val();
        var ruta="/cliente/show/"+cedula;
        $("#loading").css('display','block');

        $.get(ruta,function(res){
            $("#loading").css('display','none');
            if(res.data)
            {
                $("#Nom").val(res.data.name + " " + res.data.last_name);
                $("#Dir").val(res.data.address);
                if(res.message.length > 1)
                    message(['El cliente se encuentra vetado! De igual modo podra realizar la reservacion'],{objeto:$("#mensajereserva"),tipo:"danger",manual:true});
                else
                    message(['El cliente esta registrado!, puede continuar! :)'],{objeto:$("#mensajereserva"),tipo:"success",manual:true});
            }
            else
            {
                $("#Nom").val("");
                $("#Dir").val("");
                message(['Cedula no registrada, debe registrar antes al cliente o no podra guardar la reservacion'],{objeto:$("#mensajereserva"),tipo:"danger",manual:true});
            }
        });
    }
}

function limpiarCliente()
{
  $("#Cedu").val("");
  $("#Nom").val("");
  $("#Dir").val("");
}

$('#datepicker').datepicker({
    locale: 'es-es',
    format: 'yyyy-mm-dd',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    header: true,
});

$('#datepicker2').datepicker({
    locale: 'es-es',
    format: 'yyyy-mm-dd',
    uiLibrary: 'bootstrap4',
    iconsLibrary: 'fontawesome',
    header: true,
});



var $start = $("#datepicker");
var $end = $("#datepicker2");
var temp_start, temp_end;
$('#datepicker').on( 'change', function (){
    SuccessDay();
});

$('#datepicker2').on( 'change', function (){
    SuccessDay();
});

function SuccessDay()
{
    
    if($start.val() != "" && $end.val() != "")
    {
        if($start.val() != temp_start || $end.val() != temp_end)
        {
            temp_start = $start.val();
            temp_end = $end.val();
            if(dias())
            {
                $("#dias").val(diff);
                $("#tablas").show();
                CargarTablaArticulo();
                diastabla();
                $( "#reser" ).prop('disabled',false);
            }
            else
            {
                $("#tablas").hide();
                $( "#reser" ).prop('disabled',true);
            }
        }
    }
}

//Evalua si la diferencia de dias es valida
function dias()
{
  var fechaInicio = new Date($("#datepicker").val()).getTime();
  var fechaFin    = new Date($("#datepicker2").val()).getTime();

  diff = fechaFin - fechaInicio;
  diff=diff/(1000*60*60*24);
  console.log(diff);
  if(diff==0)
    diff=1;
  if(diff>=1)
    return true;
  else
    return false;
}

function CargarTablaArticulo()
{
    ruta="/reservacion/recargaarticulos/"+$("#datepicker").val()+"/"+$("#datepicker2").val();
    columArticulos=[{"targets": [ 4 ],"visible": false}];
    
    $("#loading").css('display','block');

    $.get(ruta,function(res){
        $("#panelarticulo").empty();//Limpiamos el div
        $("#panelarticulo").append(res);//Agregamos la tabla
        Tarti=createdt($('#tablaarti'),{buscar:'',dom:"f",columdef:columArticulos,searchC:4});//Datatable articulos
        Tarti.column(4).search(busqueda).draw();//Hacemos filtros por columna
        for(i=0; i<TA.rows().count(); i++)
        {
            TA.cell(i,1).data(0);
            TA.cell(i,4).data(0);
        }
        
        $("#loading").css('display','none');
    });
}


//ARREGLAR ESTO USAR LA DATATABLE
function diastabla()
{
    //recorremos cada una de las filas
    for(var i=0; i<TA.rows().count() ;i++)
    {
        cant=TA.rows(i).data()[0][1];
        console.log('asd'+cant);
        costo=TA.rows(i).data()[0][2];
        TA.cell(i,3).data(diff);//establecemos la nueva cantidad de dias
        TA.cell(i,4).data(cant*costo*diff);//Establecemos el total
    }
}

function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    console.log(patron);
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

//JS DEL MENU
function CambioPagMenu()
{
    console.log(table);
    if($("#menuparte1").hasClass('active'))
    {
        $("#btnGuardarComida").css("display","block");
        $("#btnCambio").text("Regresar al Menu");
        $("#menuparte1").removeClass("active");
        $("#menuparte2").addClass("active");
    }
    else
    {
        $("#btnGuardarComida").css("display","none");
        $("#btnCambio").text("Agregar nuevo plato");
        $("#menuparte2").removeClass("active");
        $("#menuparte1").addClass("active");
    }
}

function limpiarMenu()
{
  $("#descripcion").val("");
  $("#costo").val("");
  $("#path").val("");
  $("#imagen").empty();
}
//Vista previa de la imagen
$('#path').change(function(e) {
    $("#imagen").empty();
    if(e.target.files.length)
    addImage(e); 
});

function addImage(e){
    var file = e.target.files[0],
    imageType = /image.*/;
    
    if (!file.type.match(imageType))
    return;

    var reader = new FileReader();
    reader.onload = fileOnload;
    reader.readAsDataURL(file);
}

var result;//USAR ESTO PARA MOSTRAR DIRECTAMENTE LA IMAGEN SIN TENER QUE CARGARLA DESDE LA NUBE
function fileOnload(e) {
  result=e.target.result;
  $("#imagen").append('<image class="col-md-10" style="border-radius: 40px;" src="'+result+'">');
}
var y;
function RecargarMenu(contenido)
{
    //Identificamos y establecemos el html que se asignara para la imagen
    var img;
    if(contenido["image_name"] != "vacio.jpg")
        img = '<img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/'+carpeta+'/'+contenido["image_name"]+'" style="height:10em;border-radius: 40px;" value="">';
    else
        img = '<img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/'+contenido["image_name"]+'" style="height:10em;border-radius: 40px;" value="">';
    //Agregamos la nueva fila en la tabla
    var newRow = table.row.add(
    [
        img,
        contenido["description"],
        contenido["price"],
        '<input type="text" onkeypress="return valida(event);" class="form-control" style="width:100%" id="c_menucant'+contenido["id"]+'">',
        '<button class="btn btn-success" onclick="AddToListMenu(this)" value="'+contenido["id"]+'">Añadir</button>'
    ]).node();
    newRow.id = "menucant"+contenido["id"];
    newRow.style.height = "5em";
    //Realizamos la busqueda del producto recien agregado
    table.search(contenido["description"]).draw();
    //Mensaje
    message(["Guardado correctamente!"],{objeto:$("#mensajemenu"),manual:true})
  
}

function RecargarReserva()
{
    //Limpiamos filtros
    var busqueda="";
    var senddata={};
    //Limpiamos diferencia de dias
    var diff=null;
    //Limpiamos datos del cliente
    limpiarCliente();
    //Limpiamos las fechas de reservacion
    $("#datepicker").val('');
    $("#datepicker2").val('');
    $("#tablas").hide();
    //Recargamos la lista de articulos sin rango de fechas
    SuccessDay()
    //Limpiamos la tablaA
    TA.clear().draw()
    //Regresamos al primer step
    CambioPag();
    //Limpiamos la tabla del segundo step
    $("#artifin").empty();
}

var busqueda="";
$(".serviciobuscar").on('click',function(){
  busqueda=$(this)[0].id+"servicio";
  Tarti.column(4).search(busqueda).draw();
  $("#panelarticulo").show();  
});

$("#manual").on("click",function(){
  if($("#manual").prop('checked')==true)
  {
      $("#rowprice").css('visibility','visible');
  }
  else
  {
      $("#rowprice").css('visibility','hidden');
  }
});

function BlockEnter(e){
  tecla = (document.all) ? e.keyCode :e.which; 
  return (tecla!=13); 
}

$("#formulario").submit(function(e) {
    if(TA.data().length == 0)
    {
        e.preventDefault();
    }
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}