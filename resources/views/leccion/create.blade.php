@extends('layouts.dashboard')
@section('css')
  {!! Html::style('css/gijgo2/css/gijgo.css') !!}
  {!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css') !!}
  <style>
      .dias{
        display: flex!important;
        align-items: center!important;
        justify-content: center!important;
      }
      .bootstrap-select > .dropdown-toggle, .filter-option-inner-inner {
        height: 35px;
      }
      
  </style>
@stop
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar clase</h4>
    <div class="card-body">
      <div id="mensaje"></div>
      <form  id="data" autocomplete="off">

        @include('leccion.formulario.datos')

        <div class="text-center mt-2">
          <input href="#" class="btnSent btn btn-primary col-md-2" onclick="lesson_save('guardar');" value="Guardar">
        </div >
        
      </form>
    </div>
  </div>
  {{-- Modal de cuando busca un cliente y este ya existe --}}
  @include('cliente.formulario.modalBusqueda')
  @include('layouts.cargando.cargando')
@stop

@section("script")
  {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js') !!}
  {!! Html::script("js/jskevin/tiposmensajes.js")!!} 
  {!! Html::script("js/gijgo2/js/gijgo.js")!!}
  {!! Html::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') !!}
  {!! Html::script('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') !!}
  {!! Html::script('js/jskevin/kavvdt.js') !!}

  <script>
    
    $('#datepicker').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });
    
    var $lesson_msg = $("#mensaje");
    function lesson_save(decision)
    {
      var ruta = "/leccion";
      var token = $("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      $('body').animate({scrollTop:0}, 'fast');
      return $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(res){
          if(res.code === 1)
          {
            if(decision=="guardar")
            {
              message(["Se agrego la clase correctamente"],{objeto:$lesson_msg, manual:true})
              limpiar();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
            }
          }
          else
          {
            message(res.msg,{objeto:$lesson_msg, manual:true,tipo:"danger"});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
          message(jqXHR,{objeto:$lesson_msg, tipo:"danger"});
          $(".btnSent").attr("disabled",false);
          $("#loading").css('display','none');
      });
    }

    function limpiar()
    {
      $("#code").val("");
      $("#base").val("");
      $("#max").val("");
      $("#disciplina").val('').selectpicker("refresh");
      $("#services").val('').selectpicker("refresh");
      $("#teacher").val("");
      $(".hours").val("")
    }

    $("#disciplina").change(function(){ 
      var leccion = $(this).val();
      if(leccion != "")
      {
        var route = '/leccion/servicios/'+ leccion + '/true';
        $("#loading").css('display','block'); 
        $("#services").empty();
        $.get(route, function(res){
            $("#services").append(res);
            $('#services').selectpicker('refresh');
            $("#loading").css('display','none');
        }).fail(function() {
            message(['Ocurrio un error al cargar los servicios'],{tipo:"danger", manual:true});
            $("#loading").css('display','none');
        });
      }
    });
  </script>
@stop