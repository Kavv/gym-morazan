<table class="table-sm" style="width: 100%; color: #fff;" >
    @foreach ($calendars as $calendar)
        <tr>
            <td>{{$days[$calendar->day]}}</td>
            <td>{{$calendar->begin . ' - ' . $calendar->end}}</td>
        </tr>
    @endforeach
</table>