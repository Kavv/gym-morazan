<!DOCTYPE html>
<html>
	<head>
	 	<title></title>
		{!!Html::style("css/cssF/errores/style500.css")!!}
	 	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	</head>
	<body>
		<div class="main_body">
 			<div class="center_body">
				  <h1>500</h1>
				  <h2>ERROR INTERNO DEL SERVIDOR</h2>
				  <p>
				  	¡Oh oh! ¡Ha habido un error en el servidor!
				  </p>
				  <a href="/"> Pagina Principal </a>
 			</div>
		</div>
	</body>
</html>
