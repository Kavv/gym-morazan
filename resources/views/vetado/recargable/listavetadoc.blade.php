
                @foreach($clientes as $cli)
                    <tr>
                        <td>{{$cli->dni}}</td> 
                        <td>{{$cli->name}}</td>    
                        <td>{{$cli->last_name}}</td>
                        <td>
                            @can('vetado.create')
                            <button data-toggle="modal" data-target="#Add" class="btn btn-danger vetar" value="{{$cli->dni}}">Vetar</button>
                            @endcan
                        </td>
                    </tr>
                @endforeach 