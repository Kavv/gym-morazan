@extends('layouts.dashboard')
@section('css')
    <style>
        .pass {
            -webkit-text-security: disc;
        }
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">

        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead>
                <th class="text-center">Usuario</th>
                <th class="text-center">Nombre</th>
                <th class="text-center">Descripcion</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('usuario.recargable.listausuarios')
            </tbody>
        </table>
    </div>
    @can('role.index')
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->

                        <div class="row ">
                            <div class="col-md-2 "></div>
                            <div class="col-md-8 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Usuario:')!!}
                                    {!!Form::text('userName',null,['autocomplete'=>'off','id'=>'userName','class' => 'form-control border border-warning','placeholder'=>'ejemplo@hotmail.com'])!!}
                                    
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><a>Nombre y Apellido:</a></label>
                                    {!!Form::text('name',null,['autocomplete'=>'off','id'=>'fullName','class'=>'form-control border border-warning','placeholder'=>'Ejemplo123'])!!}
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Descripcion:')!!}
                                    {!!Form::text('description',null,['id'=>'description','class'=>'form-control','placeholder'=>'Usuario creado para...'])!!}
                                </div>
                            </div>
                        </div>
                        <hr class="bg-info"/>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="0" id="defaultCheck1" name="restablecerpass">
                                    <label class="form-check-label " for="defaultCheck1">
                                        Cambiar Contraseña
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr class="bg-info"/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Cambiar Contraseña:')!!}
                                    <input type="text" class="form-control pass"  name="password" id="password" placeholder="*******" onkeyup="coinciden();" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Confirmar Nueva Contraseña:')!!}
                                    <input type="text" class="form-control pass"  name="confcontraseña" id="confPassword" placeholder="*******" onkeyup="coinciden();" disabled>
                                    <span id="msjpass" style="color:red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 " style="height:20em;width:auto; overflow:scroll">
                                <table class="table table-hover table-dark text-center " cellspacing="0" id="roles" style="width:100%;">
                                <thead>
                                    <th>Nombre</th>
                                </thead>
                                <tbody id="lista">
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>
                                        <label>
                                            {{ Form::checkbox('role[]', $role->id, null)}}
                                            {{ $role->name }}
                                            <em> ({{ $role->description ?: 'sin descripcion' }}) </em>
                                        </label>
                                        </td>
                                    </tr>
                                    @endforeach 
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        @can('users.edit')
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar();" id="actualizar">Actualizar</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
    
    @can('users.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
        var table;
        var fila;
        
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1, cant:[10,20,-1],cantT:[10,20,"Todo"]});
            @can('role.index')
            createdt($('#roles'),{dom:''});
            @endcan
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });

        @can('users.edit')
        
        $('.edit').on( 'click', function () {
            $('input:checked').prop('checked',false);
            
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            row = table.row(fila).data();//Tomamos el contenido de la fila s
            $("#userName").val(row[0]);
            $("#fullName").val(row[1]);
            $("#description").val(row[2]);
            $("#id").val($(this).val());

            ruta="/usuarios/show/"+$(this).val();
            $("#loading").css('display','block');
            $.get(ruta,function(res){
                $("#loading").css('display','none');
                res[0].forEach(function(role) {
                  $('input[value="'+ role.id +'"]').prop('checked',true);
                });
            });

        });
        function actualizar()
        {
            route="/usuarios/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    console.log(res);
                    if(res==1)
                    {
                        message(['Usuario editado correctamente'],{manual:true});
                        table.cell(fila.children('td')[0]).data( $("#userName").val());
                        table.cell(fila.children('td')[1]).data( $("#fullName").val());
                        table.cell(fila.children('td')[2]).data( $("#description").val());
                        table=$("#Datos").DataTable().draw();

                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        message(res,{objeto:$("#modalMessage"), manual:true,tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            });
        }
        @endcan
        var email=$("#userName").val();
        var user=$("#fullName").val();
        var correcto=true;
        function coinciden()
        {
            var contra1=$("#password").val();
            var contra2=$("#confPassword").val();
            /*si estan vacios los campos*/
            if(contra1.length==0)
            {
                $("#msjpass").empty();
                $("#agregar").attr("disabled", true);
                correcto=true;/*enrealidad no esta correcto, es para no crear conflicto*/
                return;
            }
            /*validar que sean iguales*/
            if(contra1!=contra2)
            {
                if(correcto==true)/*condicion con el fin de evitar sobre escribir cada momento en #msjpass*/
                {
                    jQuery("#msjpass").append("No coinciden las contraseñas!");
                    correcto=false;
                    $("#agregar").attr("disabled", true);
                    return;
                }
            }
            else
            {
                $("#msjpass").empty();
                /*Validar longitud*/
                if(contra1.length < 6)
                {
                    jQuery("#msjpass").append("La longitud de la contraseña debe ser mayor a 5");
                    correcto=false;
                    $("#agregar").attr("disabled", true);
                    return;
                }
                else
                {
                    correcto=true;
                    $("#agregar").attr("disabled", false);
                }
            }
        }
        //si la casilla "cambiar contraseña" es clickeada
        $('#defaultCheck1').click(function(){
            //habilitamos los campos para la contraseña
            if($("#defaultCheck1").prop('checked')==true)
            {
                $("#password").attr("disabled",false);
                $("#confPassword").attr("disabled",false);
                $("#defaultCheck1").val(1);
            }
            else
            {
                $("#password").attr("disabled",true);
                $("#confPassword").attr("disabled",true);
                $("#defaultCheck1").val(0);
            }
        });


        @can('users.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            var route="/usuarios/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){
                    message(['Se elimino al usuario correctamente'],{manual:true});
                    table.row(row).remove().draw( false );

                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan
    </script>
@stop