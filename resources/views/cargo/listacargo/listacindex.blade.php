@foreach($cargos as $cargo)
    <tr>
        <td class="text-center">{{$cargo->name}}</td>
        <td class="text-center botones">
            {{--datos--}}
            <button type="button" data-toggle="modal" data-target="#List"  class="btn btn-info lista" value="{{$cargo->id}}"> Personal </button>

            @can('cargo.edit')
            <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$cargo->id}}">Editar</button>
            @endcan

            @can('cargo.destroy')
            <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$cargo->id}}"></button>
            @endcan
        </td>
    </tr>
@endforeach