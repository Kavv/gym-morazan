<!-- Modal -->
<div class="modal fade" id="modalCustomerAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"> Agregar un nuevo cliente </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="customer_msg"></div>
                <form  id="data" autocomplete="off">

                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                    <!-- Fila de la cedula -->
                    <div class="row ">
                        <div class="col-md-12 ">
                            <div class="form-group text-center">
                                {!!Form::label('Cedula','Cedula:')!!}
                                <div  class="input-group ">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                    <input type="checkbox" id="nacional_customer" checked>
                                    <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                                        Nica
                                    </label>
                                    </div>
                                </div>

                                <input class="form-control border border-warning" placeholder="xxx-xxxxxx-xxxxx" autocomplete="off" 
                                onkeypress="return CharCedula(event,this, '#nacional_customer');" onkeyup="formatonica(this, '#nacional_customer')" id="CC" name="Cedula_Cliente" type="text">

                                <div class="input-group-append">
                                    <button class="btn btn-primary fa fa-eraser" onclick="limpiar();"  type="button"></button>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('cliente.formulario.datos_to_modal')

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btnSent" onclick="customer_save('guardar');">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>  

