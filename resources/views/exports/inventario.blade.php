<table>
    <thead>
    <tr>
        <th style="text-align: center"><b> Se agregaron {{ $count }} articulos </b></th>
        <th>  </th>
        <th>  </th>
        <th style="text-align: center"><b> Fecha de Emision: </b></th>
    </tr>
    <tr>
        <th style="text-align: center"> Desde: {{ $start }}</th>
        <th>  </th>
        <th>  </th>
        <th style="text-align: center"> {{ date("d - m - y") }} </th>
    </tr>
    <tr>
        <th style="text-align: center">  Hasta: {{ $end }}</th>
    </tr>
    </thead>
</table>

<table>
    <thead>
    <tr>
        <th><b>Nombre</b></th>
        <th><b>Cantidad</b></th>
        <th><b>Precio de Alquiler</b></th>
        <th><b>Precio de Adquisicion</b></th>
        <th><b>Servicio</b></th>
    </tr>
    </thead>
    <tbody>
        @for($i = 0; $i < $count; $i++)
            <tr>
                <td>{{ $name[$i] }}</td>
                <td style = "text-align: center">{{ $cant[$i] }}</td>
                <td style = "text-align: center">{{ $p1[$i] }}</td>
                <td style = "text-align: center">{{ $p2[$i] }}</td>
                <td>{{ $services[$i] }}</td>
            </tr>
        @endfor
    </tbody>
</table>