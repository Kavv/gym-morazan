<?php $ingreso = []; ?>
@if($i == 0)
    <table>
        <thead>
        <tr>
            <th style="text-align: center"> Se agregaron {{ $count }} reservaciones </th>
            <th>  </th>
            <th>  </th>
            <th style="text-align: center"><b> Fecha de Emision: </b></th>
        </tr>
        <tr>
            <th style="text-align: center"> Desde: {{ $start }}</th>
            <th>  </th>
            <th>  </th>
            <th style="text-align: center"> {{ date("d - m - y") }} </th>
        </tr>
        <tr>
            <th style="text-align: center">  Hasta: {{ $end }}</th>
        </tr>
        </thead>
    </table>
@endif
@if( ($i < $count) )
    <table>
        <thead>
        <tr>
            <th style="text-align: center"><b> Cliente: {{$NC[0]}} / {{$CC[0]}} </b></th>
            <th> </th>
            <th> </th>
        </tr>
        <tr>
            <th style="text-align: center"><b> Direccion: {{$DL[0]}} </b></th>
            <th> </th>
            <th> </th>

        </tr>
        <tr>
            <th style="text-align: center"><b> Se reservo durante: </b></th>
        </tr>
        <tr>
            <th style="text-align: center">
                {{$FI[0]}} / {{$FF[0]}}
            </th>
        </tr>

        <tr>
            <th style="text-align: center"><b> DIA - MES - AÑO </b></th>
            {{--<th style="text-align: center"> DIA </th>--}}
            {{--<th style="text-align: center"> MES </th>--}}
            {{--<th style="text-align: center"> AÑO </th>--}}
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="text-align: center"> {{$dates[0]}} - {{$dates[1]}} - {{$dates[2]}} </td>
            {{--<td style="text-align: center"> {{$dates[0]}} </td>--}}
            {{--<td style="text-align: center"> {{$dates[1]}} </td>--}}
            {{--<td style="text-align: center"> {{$dates[2]}} </td>--}}
        </tr>
        </tbody>
    </table>

    <table>
        <thead>
        <tr>
            <th style="text-align: center"><b>Cant </b></th>
            <th style="text-align: center"><b>Descripcion </b></th>
            <th style="text-align: center"><b>P/Unitario </b></th>
            <th style="text-align: center"><b>TOTAL </b></th>
        </tr>
        </thead>

        <tbody>
        <?php
        //La cantidad de filas multiplicado por la pagina de factura actual
        $factura = ($reservaciones->invoice_row * 0);
        //
        $items = $reservaciones->items()->orderBy('index')->get();
        $menu = $reservaciones->menu()->orderBy('index')->get();
        $filas_totales = count($items) + count($menu);
        $pago = 0;
        $total = 0;
        $ingreso = [];
        $index_i = 0; $index_m = 0;
        for($j = 1; $j <= $filas_totales; $j++)
        {
            if(isset($items[$index_i]))
            {
                if($items[$index_i]->index == $j)
                {
                    echo
                        '<tr>'.
                        '<td style="text-align: center;">'.$items[$index_i]->qu.'</td>'.
                        '<td style="text-align: center;">'.$items[$index_i]->name.' - dias('.$items[$index_i]->day.')</td>'.
                        '<td style="text-align: center;">'.$items[$index_i]->up.'</td>'.
                        '<td style="text-align: center;">'.intval($items[$index_i]->qu * $items[$index_i]->up * $items[$index_i]->day).'</td>'.
                        '</tr>';
                    $pago += ($items[$index_i]->qu * $items[$index_i]->up * $items[$index_i]->day);
                    $index_i++;
                }
            }
                if(isset($menu[$index_m]))
                {
                    if($menu[$index_m]->index == $j)
                    {
                        echo
                            '<tr>'.
                            '<td style="text-align: center;">'.$menu[$index_m]->qu.'</td>'.
                            '<td style="text-align: center;">'.$menu[$index_m]->description.' - dias('.$menu[$index_m]->day.')</td>'.
                            '<td style="text-align: center;">'.$menu[$index_m]->up.'</td>'.
                            '<td style="text-align: center;">'.intval($menu[$index_m]->qu * $menu[$index_m]->up * $menu[$index_m]->day).'</td>'.
                            '</tr>';
                        $pago += ($menu[$index_m]->qu * $menu[$index_m]->up * $menu[$index_m]->day);
                        $index_m++;
                    }
                }
        }
        ?>

        </tbody>
        <tfoot>
        <?php

        $iva = $reservaciones->tax;
        $descuento = $reservaciones->discount;


        if($iva == 0 && $descuento == 0)
        {
            echo
                '<tr>
                                <td colspan="3" style="text-align: right"> <b> Gran Total </b></td>
                                <td style="text-align: center">'.$pago.'</td>
                        </tr>';
        }

        if($iva != 0 && $descuento == 0)
        {
            $temp_iva = $pago*$iva;
            $total = $pago + $temp_iva;
            echo
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                '<td style="text-align: center">'.$pago.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                '<td style="text-align: center">'.$temp_iva.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                '<td style="text-align: center">'.$total.'</td>'.
                '</tr>';
        }

        if($iva == 0 && $descuento != 0)
        {

            $temp_descuento = $pago*$descuento;
            $total = $pago - $temp_descuento;
            echo
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                '<td style="text-align: center">'.$pago.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>Descuento</b></td>'.
                '<td style="text-align: center">'.$temp_descuento.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                '<td style="text-align: center">'.$total.'</td>'.
                '</tr>';
        }

        if($iva != 0 && $descuento != 0)
        {
            $temp_iva = $pago*$iva;
            $temp_descuento = $pago*$descuento;
            $total = $pago-$temp_descuento+$temp_iva;
            echo
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                '<td style="text-align: center">'.$pago.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>Descuento</b></td>'.
                '<td style="text-align: center">'.$temp_descuento.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                '<td style="text-align: center">'.$temp_iva.'</td>'.
                '</tr>'.
                '<tr>'.
                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                '<td style="text-align: center">'.$total.'</td>'.
                '</tr>';
        }
//        array_push($ingresos, $total);
        ?>
        </tfoot>
    </table>
@endif

@if( $OPC == "ingresos")
    @if($i == $count)
        <table style="width:100%; ">
            <thead>
            <tr style="text-align: center">
                <th style="text-align: center"><b> Nombre </b></th>
                <th style="text-align: center"><b> Cedula </b></th>
                <th style="text-align: center"><b> Fecha de Emisión </b></th>
                <th style="text-align: center"><b> Total </b></th>
            </tr>
            </thead>
            <tbody>
            @for($l = 0; $l < $count; $l++)
                <tr>
                    <td style="text-align: center"> {{ $nomb[0][$l] }} </td>
                    <td style="text-align: center"> {{ $dni[0][$l] }} </td>
                    <td style="text-align: center"> {{ $res_date[$l] }} </td>
                    <td style="text-align: center"> {{ $monto_total = $ingresos[0][$l] }} </td>
                </tr>
            @endfor
            <tr>
                <td colspan="3" style="text-align: center"> Ingresos totales durante {{$start}} / {{$end}}  </td>
                <td style="text-align: center">{{$m_total}}</td>
            </tr>
            </tbody>
        </table>
    @endif
@endif

