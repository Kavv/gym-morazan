@extends('layouts.dashboard')
@section('css')
    {!! Html::style('css/gijgo2/css/gijgo.css') !!}
    <style>
        .content-wrapper {
            background: #343a40 !important;
        }

        .border-skyblue {
            border: #4a75b4 solid 2px;
        }

        #titulo-editar-pago,
        #btns-edit {
            display: none;
        }
        .gj-datepicker{
            width: auto;
        }

    </style>
@stop
@section('content')

    <!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">

        <div id="msg_dates"></div>

        <div class="row ml-4 mb-2">
            <div class="col-md-12">
                <div class="form-group text-center">
                    <div class="input-group d-flex justify-content-center">
                        <div class="row">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <label for="" class="font-weight-bold">Filtrar corte desde</label>
                                </div>
                            </div>
                            <input id="begin_date" placeholder="año-mes-dia" class="border border-skyblue">
                            <div class="input-group-append">
                                <span class="input-group-text font-weight-bold">Hasta</span>
                                <input id="end_date" placeholder="año-mes-dia" class="border border-skyblue">
                                <button id="btn-filtrar" class="btn btn-primary fa fa-search" onclick="actualizar_lista();"
                                    type="button"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('alert.mensaje')
        <div id="mensaje"></div>
        <!--class="table table-striped table-bordered"-->
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead>
                <tr>
                    <th class="text-center" data-orderable="false" style="width:5%;"></th>
                    <th class="text-center">Corte</th>
                    <th class="text-center">Lección</th>
                    <th class="text-center">Servicio</th>
                    <th class="text-center">Estado</th>
                    <th class="text-center" style="width:20%;">Cedula</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Apellido</th>
                </tr>
            </thead>
            <tbody class="text-center" id="lista">
                @include('pago.recargable.listaclientes')
            </tbody>
        </table>
    </div>


    @can('cliente.edit')

        <!-- Modal -->
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <form id="data">
                            <input type="hidden" id="id" name="id">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                            <div id="msgmodal"></div>
                            
                            <div class="row ">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Estado</span>
                                          </div>
                                        <select name="estado" id="estado" class="form-control border-skyblue">
                                            @foreach ($status as $stade)
                                                <option value="{{ $stade->id }}">{{ $stade->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-info" id="btn-actualizar"
                                                onclick="actualizar_cliente()">Actualizar estado</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        {!! Form::label('Cedula', 'Cedula:') !!}
                                        {!! Form::text('Cedula_Cliente', null, ['class' => 'form-control border ', 'id' => 'cedula', 'disabled' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Nombre:') !!}
                                        {!! Form::text('Nombre', null, ['id' => 'nombre', 'class' => 'form-control border ', 'disabled' => 'true']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('Apellido:') !!}
                                        {!! Form::text('Apellido', null, ['id' => 'apellido', 'class' => 'form-control border ', 'disabled' => 'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('*Direccion:') !!}
                                        {!! Form::text('Direccion', null, ['id' => 'direccion', 'class' => 'form-control', 'placeholder' => 'Direccion del Cliente', 'disabled' => 'true']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::label('Numero de teléfono:') !!}
                                </div>
                            </div>

                            <div id="telefonos" class="row">
                            </div>

                        </form>
                        <form id="data-pago">
                            <input type="hidden" id="id2" name="id">
                            <input type="hidden" id="pago-id" name="pago-id">
                            <div class="row ">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <div class="row py-2" style="background: #f0d467;">
                                            <div class="col-md-12 text-center">
                                                <h5 for="">Pagos anteriores - Abonado: <span id="total"></span></h5>
                                            </div>
                                        </div>
                                        <div id="msj-pago-table"></div>
                                        <table id="tabla-pagos" class="table table-bordered table-hover" cellspacing="0"
                                            style="width:100%;">
                                            <thead>
                                                <th>Lección</th>
                                                <th>Servicio</th>
                                                <th>Abono</th>
                                                <th>Recibo</th>
                                                <th>Fecha pago</th>
                                                <th>Digitador</th>
                                                <th></th>
                                            </thead>
                                            <tbody id="tb-pagos">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div id="titulo-nuevo-pago" class="row py-2" style="background: #f0d467;">
                                <div class="col-md-12 text-center">
                                    <h5 for="">Aplicar nuevo pago</h5>
                                </div>
                            </div>
                            <div id="titulo-editar-pago" class="row py-2" style="background: #f0d467;">
                                <div class="col-md-12 text-center">
                                    <h5 for="">Editar pago</h5>
                                </div>
                            </div>
                            <div id="msj-pago-add"></div>
                            
                            @include('pago.formulario.datos_pago')

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-block btn-success" id="btn-pago"
                                        onclick="aplicar_pago()">Aplicar pago</button>
                                    <div class="row" id="btns-edit">
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-block btn-danger" id="btn-cancelar"
                                                onclick="cancelar_edicion()">Cancelar</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-block btn-info" id="btn-editar-pago"
                                                onclick="actualizar_pago()">Actualizar pago</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!! Html::script('js/jskevin/cedulanica.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('js/jskevin/tiposmensajes.js') !!}
    {!! Html::script('js/jskevin/kavvdt.js') !!}
    {!! Html::script('js/gijgo2/js/gijgo.js') !!}




    <script>
        var table;
        var fila;

        let first_time_click = true;
        var abono_total;
        $(document).ready(function() {
            table = createdt($('#Datos'), {
                buscar: '{!! session('valor') !!}',
                cant: [10, 20, -1],
                cantT: [10, 20, "todo"], 
                col: 1
            });
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index', 1060);
        });

        @can('cliente.create')
            function aplicar_pago(){
                var route = "/pago";
                var token = $("#token").val();
                $("#btn-pago").attr("disabled",true);
                $("#loading").css('display','block');
                return $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data: $("#data-pago").serialize(),
                    success: function(res){
                        if(res.code === 1)
                        {
                            message(["Se agrego el pago correctamente"],{objeto:$("#msj-pago-add"), manual:true});
                            limpiar();
                        
                            cargar_tabla_pagos(res.payments);

                            actualizar_lista();

                            $("#btn-pago").attr("disabled",false);
                            $("#loading").css('display','none');
                        }
                        else
                        {
                            message(res.msg,{objeto:$("#msj-pago-add"), manual:true,tipo:"danger"});
                            $("#btn-pago").attr("disabled",false);
                            $("#loading").css('display','none');
                        }
                    }
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    message(jqXHR,{objeto:$("#msj-pago-add"), tipo:"danger"});
                    $("#btn-pago").attr("disabled",false);
                    $("#loading").css('display','none');
                });
            }
        @endcan

        @can('cliente.destroy')
            var delete_row;
            $('#deleteModal').on('show.bs.modal', function (event) {
                {{-- Mantenemos el id del elemento seleccionado --}}
                var button = $(event.relatedTarget); // Button that triggered the modal
                var value = button.data('value'); // Extract info from data-* attributes
                $("#pago-id").val(value);
            
                {{-- Mantenemos la fila del elemento seleccionado --}}
                delete_row = button.parents('tr');
            });
        
            $('#delete').on('click', function () {
                var route = "/pago/"+$("#pago-id").val();
                var token = $("#token").val();
                $("#delete").attr('disabled',true);
                $("#loading").css('display','block');
                $.ajax({
                    url: route,
                    headers:{'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    dataType: 'json',
                    success: function(res){
                        if(res.code == 1)
                        {
                            message(['Se elimino el pago correctamente'],{objeto:$("#msj-pago-table"), manual:true});
                            table_payments.row(delete_row).remove().draw( false );
    
                            abono_total -= parseInt(res.amount);
                            $("#total").text(abono_total);
                            
                            actualizar_lista();
                        }
                        if(res.code == 2)
                        {
                            message(res.msg,{objeto:$("#msj-pago-table"), manual:true,tipo:"danger"});
                        }
                        change_mode('hide');
                        $("#delete").attr('disabled',false);
                        $("#loading").css('display','none');
                        $("#deleteModal").modal('toggle');
                        $("#Edit").css('overflow-y', 'auto');
                    }
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    message(jqXHR,{objeto:$("#msj-pago-table"), tipo:"danger"});
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                });
            });
        @endcan

        // Editar cliente
        @can('cliente.edit')
            function actualizar()
            {
                route="/cliente/"+$("#id").val();
                var token=$("#token").val();
                $("#actualizar").attr('disabled',true);
                $("#loading").css('display','block');
                $.ajax({
                    url: route,
                    headers:{'X-CSRF-TOKEN': token},
                    type: 'PUT',
                    dataType: 'json',
                    data:$("#data").serialize(),
                    success: function(res){
                        if(res==1)
                        {
                            message(['Cliente editado correctamente'],{manual:true});
                            table.cell(fila.children('td')[0]).data( $("#cedula").val());
                            table.cell(fila.children('td')[1]).data( $("#nombre").val());
                            table.cell(fila.children('td')[2]).data( $("#apellido").val());
                            table.cell(fila.children('td')[3]).data( $("#edad").val());
                            table.cell(fila.children('td')[4]).data( $("#sexo").val());
                            table.cell(fila.children('td')[5]).data( $("#direccion").val());
                            /* fila.children('td.botones').children('input.delete').removeAttr( "onclick");//Eliminamos la funcion onclick del boton
                            eliminar
                            fila.children('td.botones').children('input.delete').attr( "onclick",'erase(\''+$("#cedula").val()+'\');');//Agregamos
                            la funcion onclick con el nuevo parametro */
                            table=$("#Datos").DataTable().draw();
                            $("#Edit").modal('toggle');
                            $('body').animate({scrollTop:0}, 'fast');
                        }
                        else
                        {
                            message(['No se actualizo: La cedula ingresada ya esta en uso'],{objeto:$("#modalMessage"), manual:true,tipo:"danger"});
                        }
                        $("#actualizar").attr('disabled',false);
                        $("#loading").css('display','none');
                    }
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                    message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
                });
            }
        
            function actualizar_cliente()
            {
                var route = "/pago/actualizarinfo";
                var token = $("#token").val();
                $("#btn-actualizar").attr("disabled", true);
                $("#loading").css('display', 'block');
                return $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'POST',
                    dataType: 'json',
                    data: {'id': $("#id").val(), 'state': $('#estado').val()},
                    success: function(res){
                        if(res.code === 1)
                        {
                            message(["¡Se actualizo el estado correctamente!"],{objeto:$("#msgmodal"), manual:true});
                        
                            table.cell(fila.children('td')[4]).data( $("#estado option:selected").text());
                        
                            $("#btn-actualizar").attr("disabled", false);
                            $("#loading").css('display','none');
                        }
                        else
                        {
                            message(res.msg,{objeto:$("#msgmodal"), manual:true,tipo:"danger"});
                            $("#btn-actualizar").attr("disabled", false);
                            $("#loading").css('display','none');
                        }
                    }
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    message(jqXHR,{objeto:$("#msgmodal"), tipo:"danger"});
                    $("#btn-actualizar").attr("disabled", false);
                    $("#loading").css('display','none');
                });
            }
        @endcan

        var fila_pago = null;
        // Editar pago
        @can('cliente.edit')
            function editar_pago(pago, element) {
                fila_pago = $(element).parents('tr');
                let route = '/pago/show/'+pago;
                $("#loading").css('display','block');
                $.get(route, function(res){
                    if(res.code == 1)
                    {
                        let data = res.data;

                        if (data.lesson_id != null) {
                            if(data.service_id != null)
                                select_service = data.service_id;
                            $("#leccion").val(data.lesson_id);
                            $("#monto").val(data.amount);
                            $("#leccion").change();
                        } 
                        else 
                            $("#leccion").val("");

                        $("#recibo").val(data.number);
                        $("#fecha_pago").val(data.date);
                        $("#inicio").val(data.start);
                        $("#corte").val(data.end);
                        $("#pago-id").val(data.id);
                        change_mode('show');
                    }
                    else
                        message(res.msg,{objeto:$("#msj-pago-add"), manual:true,tipo:"danger"});
                    $("#loading").css('display','none');
                });
            }
            function cancelar_edicion()
            {
                change_mode('hide');
                limpiar();
                $("#servicios").val("");
                $("#fecha_pago").val("");
            }
            function actualizar_pago()
            {
                var route = "/pago/"+$("#pago-id").val();
                var token = $("#token").val();
                $("#btn-editar-pago").attr("disabled", true);
                $("#loading").css('display', 'block');
                return $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'PUT',
                    dataType: 'json',
                    data: $("#data-pago").serialize(),
                    success: function(res){
                        if(res.code === 1)
                        {
                            message(["¡Se actualizo la información correctamente!"],{objeto:$("#msj-pago-add"), manual:true});
                            var text_service = $("#servicios option:selected").data().name;
                            table_payments.cell(fila_pago.children('td')[0]).data( $("#leccion option:selected").text());
                            table_payments.cell(fila_pago.children('td')[1]).data( text_service);
                            table_payments.cell(fila_pago.children('td')[2]).data( $("#monto").val());
                            table_payments.cell(fila_pago.children('td')[3]).data( res.receipt);
                            table_payments.cell(fila_pago.children('td')[4]).data( $("#fecha_pago").val());
                        
                            abono_total -= parseInt(res.diff);
                            $("#total").text(abono_total);

                            limpiar();
                            actualizar_lista();
                            $("#btn-editar-pago").attr("disabled", false);
                            change_mode('hide');
                            $("#loading").css('display','none');
                        }
                        else
                        {
                            message(res.msg,{objeto:$("#msj-pago-add"), manual:true,tipo:"danger"});
                            $("#btn-editar-pago").attr("disabled", false);
                            $("#loading").css('display','none');
                        }
                    }
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    message(jqXHR,{objeto:$("#msj-pago-add"), tipo:"danger"});
                    $("#btn-editar-pago").attr("disabled", false);
                    $("#loading").css('display','none');
                });
            }
        @endcan


        let table_payments = null;
        var select_service = 0;
        function details(element) {
            limpiar_msj();
            change_mode('hide');

            fila = $(element).parents('tr'); //Dejamos almacenada temporalmente la fila en la que clickeamos editar
            route = "/pago/payments/" + $(element).val();
            // Si es la primera vez que se da click en un detalle entonces preestablecemos la fecha de hoy en el la fecha de cobro
            if (first_time_click) {
                $("#fecha_pago").val($("#aux-fc").val());
                first_time_click = false;
            }

            $.get(route, function(res) {
                var phones = res.phones;
                var data = res.data;
                var services = res.services;
                var lesson = res.lesson;
                var payments = res.payments;

                $("#cedula").val(data.dni);
                $("#estado").val(data.status_id);
                $("#nombre").val(data.name);
                $("#apellido").val(data.last_name);
                $("#edad").val(data.age);
                $("#sexo").val(data.gender);
                $("#direccion").val(data.address);
                $("#id").val(data.id);
                $("#id2").val(data.id);

                //Limpiamos la casilla del telefono
                $("#telefono").val("");
                // Limpiamos todos los telefonos que se estubiesen mostrando
                $("#telefonos").empty();
                for (var i = 0; i < phones.length; i++) {
                    //enviamos el numero y especificamos que la agregacion sera manual(true)
                    addPhone(phones[i].number, true);
                }
                
                limpiar();
                if (lesson != null) {
                    if(services != null)
                        select_service = services.id;
                    $("#leccion").val(lesson.id);
                    $("#monto").val(services.amount);
                    $("#leccion").change();
                } 

                cargar_tabla_pagos(payments);

                $("#actualizar").attr('disabled', false);
                $("#loading").css('display', 'none');
            });
        }

        function cargar_tabla_pagos(payments) {
            if (table_payments != null) {
                table_payments.destroy();
                $('#tb-pagos').empty();
            }
            abono_total = 0;
            // Agregamos a la tabla cada uno de los pagos efectuados por el cliente
            payments.forEach(function(payment) {
                add_payment(payment);
                
                abono_total += parseInt(payment.amount);
            });

            $("#total").text(abono_total);
            
            table_payments = createdt($('#tabla-pagos'), {
                col: 4,
                cant: [-1],
                cantT: ["todo"],
                dom: bootstrapDesingSlim
            });
        }

        function add_payment(p) {
            var html = "" +
                "<tr>" +
                "<td>" + p.code + "</td>" +
                "<td>" + p.service + "</td>" +
                "<td>" + p.amount + "</td>" +
                "<td>" + p.number + "</td>" +
                "<td>" + p.date + "</td>" +
                "<td>" + p.digitizer + "</td>" +
                "<td>" +
                    "<button type='button' class='btn btn-warning fa fa-pencil' onclick='editar_pago(" + p.id + ", this)'>" +
                    "<button type='button' data-toggle='modal' data-target='#deleteModal'  class='btn btn-danger fa fa-trash ' data-value='" + p.id + "'></button>" +
                "</td>" +
                "</tr>";
            $("#tb-pagos").append(html);
        }

        function actualizar_lista() {
            let begin_date = $("#begin_date").val();
            let end_date = $("#end_date").val();
            if (begin_date == "")
                begin_date = 0;
            if (end_date == "")
                end_date = 0;
                

            $("#loading").css('display','block');
            route = "/pago/f-payday/";
            return $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'GET',
                dataType: 'html',
                data: {'begin': begin_date ,'end': end_date},
                success: function(res) {
                    
                        table.destroy();
                        $("#lista").empty();
                        $("#lista").append(res);

                        table = createdt($('#Datos'), {
                            cant: [10, 20, -1],
                            cantT: [10, 20, "todo"],
                            col: 1
                        });
                    if(typeof res.code !== 'undefined')
                    {
                        message(res.msg, {objeto:$("#msg_dates"), manual:true, tipo:"danger"});
                    }
                    $("#loading").css('display','none');
                    console.log('good');
                }
            });
        }

        function change_mode(state) {
            if (state == 'show') {
                $("#titulo-nuevo-pago").css('display', 'none');
                $("#btn-pago").css('display', 'none');

                $("#titulo-editar-pago").css('display', 'flex');
                $("#btns-edit").css('display', 'flex');
            } else {
                $("#titulo-nuevo-pago").css('display', 'flex');
                $("#btn-pago").css('display', 'block');

                $("#titulo-editar-pago").css('display', 'none');
                $("#btns-edit").css('display', 'none');
            }
        }

        function limpiar() {
            $("#monto").val("");
            $("#recibo").val("");
            $("#leccion").val("");
            $("#servicios").val("");
        }

        function limpiar_msj() {
            $("#msgmodal").empty();
            $("#msj-pago-add").empty();
            $("#msj-pago-table").empty();
        }

        function addPhone(telefono = null, manual = false) {
            if (telefono == null)
                telefono = $("#telefono").val();
            if (telefono != "") {
                html = '<div class="telefono-clone col-md-4">' +
                    '<div class="form-group">' +
                    '<input value="' + telefono +
                    '" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel" disabled>' +
                    '</div>' +
                    '</div>';

                $("#telefono").val("");
                $("#telefonos").append(html);
                if (manual == false)
                    $("#telefono").focus()
            }
        }


        $("#f-dia-cobro").keypress(function(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 13)
                actualizar_lista()
        });

        
        
        
        $('#begin_date').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        
        $('#end_date').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });



        $('#fecha_pago').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        $('#inicio').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        $('#corte').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        
        $("#leccion").change(function(){

            console.log('entra');
            var leccion = $(this).val();
            var route = '/leccion/servicios/'+ leccion;
            $("#loading").css('display','block');
            $("#mensaje").empty();
            $("monto").val("");
            $("#servicios").empty();
            $.get(route, function(res){
                $("#servicios").append(res);
                
                if(select_service != 0)
                {
                    $("#servicios").val(select_service);
                    select_service = 0;
                }
                
                $("#loading").css('display','none');
            }).fail(function() {
                message(['Ocurrio un error al cargar los servicios'],{tipo:"danger", manual:true});
                $("#loading").css('display','none');
            });
        });

        $("#servicios").change(function(){
            var monto = $("#servicios option:selected").data().amount;
            $("#monto").val(monto);
        });



    </script>
@stop
