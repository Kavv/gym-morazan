@extends('layouts.dashboard')
@section('css')
    {!! Html::style('css/gijgo2/css/gijgo.css') !!}
    <style>
        .content-wrapper {
            background: #343a40 !important;
        }

        .border-skyblue {
            border: #4a75b4 solid 2px;
        }

        #titulo-editar-pago,
        #btns-edit {
            display: none;
        }
        .gj-datepicker{
            width: auto;
        }
    </style>
@stop
@section('content')

    <!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="msg_dates"></div>
        <div class="row ml-4 mb-2">
            <div class="col-md-12">
                <div class="form-group text-center">
                    <div class="input-group d-flex justify-content-center">
                        <div class="row">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <label for="" class="font-weight-bold">Pagos Desde</label>
                                </div>
                            </div>
                            <input id="begin_date" placeholder="año-mes-dia" class="border border-skyblue">
                            <div class="input-group-append">
                                <span class="input-group-text font-weight-bold">Hasta</span>
                                <input id="end_date" placeholder="año-mes-dia" class="border border-skyblue">
                                <button id="btn-filtrar" class="btn btn-primary fa fa-search" onclick="actualizar_lista();"
                                    type="button"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 for="" id="total"></h3>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    <div id="msj-pago-table"></div>
                    <table id="tabla-pagos" class="table table-bordered table-hover table-dark" cellspacing="0" style="width:100%;">
                        <thead>
                            <th>Fecha pago</th>
                            <th>Cliente</th>
                            <th>Lección</th>
                            <th>Servicio</th>
                            <th>Monto</th>
                            <th>Recibo</th>
                            <th>Inicio</th>
                            <th>Corte</th>
                            <th></th>
                        </thead>
                        <tbody id="tb-pagos">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    @can('cliente.edit')

        <!-- Modal -->
        <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Editar pago</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="modalMessage"></div>
                        <form id="data-pago">
                            <input type="hidden" id="id2" name="id">
                            <input type="hidden" id="pago-id" name="pago-id">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                            <div id="msj-pago-add"></div>
                            @include('pago.formulario.datos_pago')
                            
                            
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="btn-editar-pago"
                                onclick="actualizar_pago()">Actualizar pago</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js') !!}
    {!! Html::script('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('js/jskevin/tiposmensajes.js') !!}
    {!! Html::script('js/jskevin/kavvdt.js') !!}
    {!! Html::script('js/gijgo2/js/gijgo.js') !!}

    <script>
        var table_payments;
        var fila_pago;
        var abono_total = 0;
        var delete_row;

        $(document).ready(function() {
            table_payments = createdt($('#tabla-pagos'), {
                cant: [10, 20, -1],
                cantT: [10, 20, "todo"]
            });
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index', 1060);
        });

        function actualizar_lista()
        {
            var route = '/pago/list';
            var token = $("#token").val();
            $("#loading").css('display','block');
            $("#msg_dates").empty();
            return $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'GET',
                dataType: 'json',
                data: {'begin': $("#begin_date").val() ,'end': $("#end_date").val()},
                success: function(res) {
                    var payments = null;
                    var state = false;
                    if(typeof res.data !== 'undefined')
                    {
                        payments = res.data;
                        state = true;
                    }
                    if(typeof res.code !== 'undefined')
                    {
                        message(res.msg, {objeto:$("#msg_dates"), manual:true, tipo:"danger"});
                    }
                    cargar_tabla_pagos(payments, state);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#msg_dates"), tipo:"danger"});
                $("#loading").css('display','none');
            });
        }


        function editar_pago(pago, element) {
            fila_pago = $(element).parents('tr');
            let route = '/pago/show/'+pago;
            $("#loading").css('display','block');
            limpiar();
            $.get(route, function(res){
                if(res.code == 1)
                {
                    let data = res.data;
                    if (data.lesson_id != null) {
                        if(data.service_id != null)
                            select_service = data.service_id;
                        $("#leccion").val(data.lesson_id);
                    } 
                    else 
                        $("#leccion").val("");

                    $("#leccion").change();
                    $("#monto").val(data.amount);
                    $("#recibo").val(data.number);
                    $("#fecha_pago").val(data.date);
                    $("#inicio").val(data.start);
                    $("#corte").val(data.end);
                    $("#pago-id").val(data.id);

                }
                else
                {
                    message(res.msg,{objeto:$("#msj-pago-table"), manual:true,tipo:"danger"});
                    $("#Edit").modal('hide');
                }
                $("#loading").css('display','none');
            });
        }

        function actualizar_pago()
        {
            var route = "/pago/"+$("#pago-id").val();
            var token = $("#token").val();
            $("#btn-editar-pago").attr("disabled", true);
            $("#loading").css('display', 'block');
            return $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data: $("#data-pago").serialize(),
                success: function(res){
                    if(res.code === 1)
                    {
                        message(["¡Se actualizo el pago correctamente!"],{objeto:$("#msj-pago-table"), manual:true});
                        var text_service = $("#servicios option:selected").data().name;
                        table_payments.cell(fila_pago.children('td')[0]).data( $("#fecha_pago").val());
                        table_payments.cell(fila_pago.children('td')[2]).data( $("#leccion option:selected").text());
                        table_payments.cell(fila_pago.children('td')[3]).data( text_service);
                        table_payments.cell(fila_pago.children('td')[4]).data( $("#monto").val());
                        table_payments.cell(fila_pago.children('td')[5]).data( res.receipt);
                        table_payments.cell(fila_pago.children('td')[6]).data( $("#inicio").val());
                        table_payments.cell(fila_pago.children('td')[7]).data( $("#corte").val());
                    
                        abono_total -= parseInt(res.diff);
                        $("#total").text("Monto total: "+ abono_total);

                        $("#btn-editar-pago").attr("disabled", false);
                        $("#Edit").modal('hide');
                        $("#loading").css('display','none');
                    }
                    else
                    {
                        message(res.msg,{objeto:$("#msj-pago-add"), manual:true,tipo:"danger"});
                        $("#btn-editar-pago").attr("disabled", false);
                        $("#loading").css('display','none');
                    }
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#msj-pago-add"), tipo:"danger"});
                $("#btn-editar-pago").attr("disabled", false);
                $("#loading").css('display','none');
            });
        }

        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#pago-id").val(value);
        
            {{-- Mantenemos la fila del elemento seleccionado --}}
            delete_row = button.parents('tr');
        });
    
        $('#delete').on('click', function () {
            var route = "/pago/"+$("#pago-id").val();
            var token = $("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    if(res.code == 1)
                    {
                        message(['Se elimino el pago correctamente'],{objeto:$("#msj-pago-table"), manual:true});
                        table_payments.row(delete_row).remove().draw( false );
    
                        abono_total -= parseInt(res.amount);
                        $("#total").text("Monto total: "+ abono_total);
    
                    }
                    
                    if(res.code == 2)
                    {
                        message(res.msg,{objeto:$("#msj-pago-table"), manual:true,tipo:"danger"});
                    }
                    
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $("#Edit").css('overflow-y', 'auto');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#msj-pago-table"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });

        
        function cargar_tabla_pagos(payments, empty) 
        {
            if (table_payments != null) {
                table_payments.destroy();
                $('#tb-pagos').empty();
            }
            abono_total = 0;
            if(empty)
            {
                // Agregamos a la tabla cada uno de los pagos efectuados por el cliente
                payments.forEach(function(payment) {
                    add_payment(payment);

                    abono_total += parseInt(payment.amount);
                });
            }
            $("#total").text("Monto total: "+ abono_total);
            table_payments = createdt($('#tabla-pagos'), {
                col: 0,
                cant: [25, 50, 100, -1],
                cantT: [25, 50, 100, "todo"],
            });
        }

        function add_payment(p) 
        {
            var html = "" +
                "<tr>" +
                "<td>" + p.date + "</td>" +
                "<td>" + p.dni + "</td>" +
                "<td>" + p.lesson + "</td>" +
                "<td>" + p.service + "</td>" +
                "<td>" + p.amount + "</td>" +
                "<td>" + p.number + "</td>" +
                "<td>" + p.start + "</td>" +
                "<td>" + p.end + "</td>" +
                "<td>" +
                "<button type='button' data-toggle='modal' data-target='#Edit' class='btn btn-warning edit fa fa-pencil' onclick='editar_pago(" + p.id + ", this)'></button>" +
                "<button type='button' data-toggle='modal' data-target='#deleteModal'  class='btn btn-danger fa fa-trash ' data-value='" +p.id + "'></button>" +
                "</td>" +
                "</tr>";
            $("#tb-pagos").append(html);
        }
        
        
        function limpiar() {
            $("#monto").val("");
            $("#recibo").val("");
            $("#leccion").val("");
            $("#servicios").val("");
            $("fecha_pago").val("");
        }
        
        $('#begin_date').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        
        $('#end_date').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });

        

        $('#fecha_pago').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        $('#inicio').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        $('#corte').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        
        $("#leccion").change(function(){

            console.log('entra');
            var leccion = $(this).val();
            var route = '/leccion/servicios/'+ leccion;
            $("#loading").css('display','block');
            $("#mensaje").empty();
            $("monto").val("");
            $("#servicios").empty();
            $.get(route, function(res){
                $("#servicios").append(res);
                
                if(select_service != 0)
                {
                    $("#servicios").val(select_service);
                    select_service = 0;
                }
                
                $("#loading").css('display','none');
            }).fail(function() {
                message(['Ocurrio un error al cargar los servicios'],{tipo:"danger", manual:true});
                $("#loading").css('display','none');
            });
        });

        $("#servicios").change(function(){
            var monto = $("#servicios option:selected").data().amount;
            $("#monto").val(monto);
        });
    </script>
@stop
