@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    {!!Html::style("css/gijgo2/css/gijgo.css")!!}
    <div class="d-block bg bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">

        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead>
                <th class="text-center" style="max-width:20%">Cedula</th>
                <th class="text-center">Nombre</th>
                <th class="text-center">Apellido</th>
                <th class="text-center">Edad</th>
                <th class="text-center" style="max-width:20%">Direccion</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('personal.recargable.listapersonal')
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="data" autocomplete = off>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->
                        <div id="modalMessage"></div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group text-center">
                                    {!!Form::label('Cedula','Cedula:')!!}
                                    <div  class="input-group ">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <input type="checkbox" id="nacional" checked>
                                                <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                                                    Nica
                                                </label>
                                            </div>
                                        </div>
                                        {!!Form::text('Cedula_Personal',null,['class'=>'form-control border border-warning',
                                        'placeholder'=>'xxx-xxxxxx-xxxxx', 'autocomplete'=>'off', 'onkeypress'=>'return CharCedula(event,this,"#nacional");', 
                                        'onkeyup'=>'formatonica(this,"#nacional")','id'=>'cedu'])!!}  
                                        <div class="input-group-append">
                                            <button class="btn btn-primary fa fa-search" onclick="buscar();"  type="button"></button>
                                            <button class="btn btn-primary fa fa-eraser" onclick="limpiar();"  type="button"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('Contraseña:') !!}
                                    <div class="input-group ">
                                        <input autocomplete="new-password" id="password" type="password" name="password" class="form-control " placeholder="Defina una contraseña">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary fa fa-eye" onclick="ShowPass(this);" type="button"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Nombre:')!!}
                                    {!!Form::text('Nombre',null,['id'=>'nombre','class'=>'form-control border border-warning','placeholder'=>'Nombre del Personal'])!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Apellido:')!!}
                                    {!!Form::text('Apellido',null,['id'=>'apellido','class'=>'form-control border border-warning','placeholder'=>'Apellido del Personal'])!!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Direccion:')!!}
                                    {!!Form::text('Direccion',null,['id'=>'direccion','class'=>'form-control','placeholder'=>'Direccion del personal'])!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Edad:')!!}
                                    <div  class="input-group ">
                                        {!!Form::text('Edad',null,['id'=>'edad','class'=>'form-control','placeholder'=>'Edad del Cliente'])!!}
                                        <div class="input-group-append">
                                            <button class="btn btn-primary fa fa-eye" onclick="mostraredad();"  type="button"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div id="telefonos">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!!Form::label('Numero de teléfono:')!!}
                                        <div  class="input-group ">
                                            {!!Form::tel('telefono[]',null,['id'=>'telefono','class'=>'form-control','placeholder'=>'Ingrese el numero'])!!}
                                            <div class="input-group-append">
                                                <button class="btn btn-primary fa fa-plus" onclick="addPhone();"  type="button"></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @can('cargo.index')
                        <div class="row" >
                            <div class="col-md-12" id="lc">
                                <div class="list-group " id="list-tab" role="tablist" style="height:15em;width:auto; overflow:scroll;">
                                    <table class="table table-hover table-dark" cellspacing="0" id="cargos" style="width:100%;">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Cargo</th>
                                                <th data-orderable="false"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($Cargos as $cargo)
                                                <tr>
                                                    <td >{!!$cargo->name!!}</td>
                                                    <td class="btn-primary text-center">
                                                        <input name="cargos[]" id="check{!!$cargo->id!!}" value="{!!$cargo->id!!}" type="checkbox" OnClick='addcargo(this,{!!$cargo->id!!})'>
                                                    </td>
                                                </tr>  
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endcan

                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        @can('personal.edit')
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar();" id="actualizar" >Actualizar</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!}
    {!!Html::script("js/gijgo2/js/gijgo.js")!!}
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!}  
    {!!Html::script("js/jskevin/kavvdt.js")!!}  
    <script>
        var table;
        var fila;
        //le pica el culito pero funciona no borrar
        var cargo = {!! json_encode($Cargos->toArray()) !!};
        var cont = cargo.length;
        //no borrar lo de arriba

        $(document).ready( function () {
            table= createdt($("#Datos"),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"todo"] });
            createdt($("#cargos"),{col:0,dom:""});
            $("#main").css("visibility", "visible"); 
            $("#loading").css('z-index',1060);
            add_select();
        });
        let drop = null;
        function add_select(){
            let divi = $('#Datos_length');
            drop = document.createElement("select");
            //base
            let opt = new Option();
            opt.value = 0;
            opt.text = 'Cargos';
            drop.options.add(opt);
            //datos agregados segun los cargos existentes
            drop.setAttribute("id", "dropdown-cargos");
            drop.setAttribute("class", "form-control pull-right drop-cargo");

            /* Todos */
            opt = new Option();
            opt.value = -1;
            opt.text = 'Todos';
            drop.options.add(opt);
            
            /* Sin cargos asignados */
            opt = new Option();
            opt.value = -2;
            opt.text = 'Sin cargos';
            drop.options.add(opt);


            //dependiendo de los cargos se realizara la iteracion
            for(let i = 0; i < cont; i++){
                //creacion de opciones para el select
                opt = new Option();
                //asignacion de valores dependientes los cargos
                opt.value = cargo[i].id;
                opt.text = cargo[i].name;
                //agregar a el html tag options
                drop.options.add(opt);
            }
            //se agrega a la division perteneciente
            divi.append(drop);
            //como no existe el evento porque se agrego desde el javascript
            if(drop.addEventListener){
                //se crea el evento change
                drop.addEventListener('change', midrop, false);
            } else{
                drop.attachEvent('change', midrop);
            }
        }

        function midrop(){
            let value = $(this).val();
            // console.log(value);
            let ruta="/personal/cargo/"+value;
            $.get(ruta,function (res) {
                // console.log(res);
                table.destroy();
                $('#lista').empty();
                $('#lista').append(res);
                table = createdt($("#Datos"),{col:1,cant:[10,20,-1],cantT:[10,20,"todo"] });
                $('#Datos_length').append(drop);
                //add_select();
            });
        }

        function detalles(input) {

            limpiar();
            fila=$(input).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            var row=table.row(fila).data();//Tomamos el contenido de la fila 
            //Sobreescribimos en los campos editables los datos del cliente
            $("#cedu").val(row[0]);
            $("#nombre").val(row[1]);
            $("#apellido").val(row[2]);
            $("#edad").val(row[3]);
            $("#direccion").val(row[4]);
            $("#id").val($(input).val());//Campo que almacena la cedula inicial
            
            //Vaciamos el campo de telefono y Removemos todos los input que sean clone
            $('#telefono').val("");
            $('.telefono-clone').remove();

            $("#loading").css('display','block');
            $("#actualizar").attr('disabled',true);

            @can('cargo.index')
            $('input[type="checkbox"]').prop('checked',false);
            $("#nacional").prop('checked',true);
            var ruta="/personal/cargopersonal/"+$(input).val();

            $.get(ruta,function(res){
                listacargo=new Object();
                for(i=0; i<res.length; i++)
                {
                    $("#check"+res[i].id).prop("checked",true);
                    listacargo[res[i].id]=res[i].id;
                }
            });
            @endcan

            
            var route = "/personal/telefonos/"+$(input).val();
            
            //Solicitamos los numeros de telefono del cliente
            $.get(route, function(res){
                for(var i=0; i<res.length; i++)
                {
                    //enviamos el numero y especificamos que la agregacion sera manual(true)
                    addPhone(res[i].number,true);
                }
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
            });
        }
        @can('personal.edit')
        function actualizar()
        {
            route="/personal/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    if(res==1)
                    {
                        message(['Personal editado correctamente'],{manual:true});
                        table.cell(fila.children('td')[0]).data( $("#cedu").val());
                        table.cell(fila.children('td')[1]).data( $("#nombre").val());
                        table.cell(fila.children('td')[2]).data( $("#apellido").val());
                        table.cell(fila.children('td')[3]).data( $("#edad").val());
                        table.cell(fila.children('td')[4]).data( $("#direccion").val());
                        /* fila.children('td.botones').children('input.delete').removeAttr( "onclick");//Eliminamos la funcion onclick del boton eliminar
                        fila.children('td.botones').children('input.delete').attr( "onclick",'erase(\''+$("#cedu").val()+'\');');//Agregamos la funcion onclick con el nuevo parametro */
                        table=$("#Datos").DataTable().draw();
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        message(res,{objeto:$("#modalMessage"), manual:true,tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
                $('body').animate({scrollTop:0}, 'fast');
            });
        }
        @endcan
        @can('personal.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            var route="/personal/" + $("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){
                    message(['Se elimino al personal correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
                $('body').animate({scrollTop:0}, 'fast');
            });
        });
        @endcan
        var listacargo=new Object();
        @can('cargo.index')
        function addcargo(elemento,key)
        {
            if(elemento.checked==true)
            {
                listacargo[key]=key;
            }
            else
            {
                delete listacargo[key];
            }
        }

        var campocedula = $("#cedu");
        function buscar()
        {
            cedula=campocedula.val();
            if(cedula.length>0)
            {
                //Si la cedula es la misma de que se encuentra en la dt entonces no hace la consulta
                if(table.row(fila).data()[0] != $("#cedu").val())
                {
                    var ruta="/personal/show/"+cedula;
                    $("#loading").css('display','block');
                    $.get(ruta,function(res){
                        $("#loading").css('display','none');
                        if(res.data)
                            message(['La cedula esta en uso por otro personal!'],{objeto:$("#modalMessage"),tipo:"danger",manual:true});
                        else
                            message(['La cedula es valida, es posible editar!'],{objeto:$("#modalMessage"),tipo:"success",manual:true})
                    });
                }
                else
                    message(['La cedula es valida, es posible editar!'],{objeto:$("#modalMessage"),tipo:"success",manual:true})
            }
        }
        function mostraredad()
        {
            if(year>0 && month>0 && day>0)
            {
                $("#edad").val(edad);
            }
        }
        
        $("#nacional").on("change",function()
        {
            if($("#nacional").prop('checked')==true)//Si es idenficacion Nica
            {
                $("#cedu").val("");
            }
        });
        @endcan
        
        function addPhone(telefono=null, manual=false)
        {
            if(telefono == null)
                telefono = $("#telefono").val();
            if(telefono != "")
            {
                html= '<div class="row telefono-clone">'+
                        '<div class="col-md-2"></div>'+
                        '<div class="col-md-12">'+
                            '<div class="form-group">'+
                            '<div class="input-group ">'+
                                '<input value="'+ telefono +'" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel">'+
                                '<div class="input-group-append">'+
                                '<button class="btn btn-danger fa fa-trash" onclick="removePhone(this);" type="button"></button>'+
                                '</div>'+
                            '</div>'+
                            '</div>'+
                        '</div>'+
                        '</div>';

                $("#telefono").val("");
                $("#telefonos").append(html);
                if(manual == false)
                    $("#telefono").focus()
            }
        }
        
        function removePhone(btn)
        {
            $(btn).parents('.telefono-clone').remove();
        }

        function limpiar()
        {
            $("#cedu").val("");
            $("#Nombre").val("");
            $("#Apellido").val("");
            $("#Direccion").val("");
            $("#Edad").val("");
            for(var elemento in listacargo)
            {
                $("#"+elemento).prop('checked',false);
            }
            listacargo=new Object();
            
            $("#telefono").val("");
            $(".telefono-clone").remove();
            $("#password").val("");
        }
    </script>
@stop