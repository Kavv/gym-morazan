@extends('layouts.dashboard')
@section('content')
  {!!Html::style("css/gijgo2/css/gijgo.css")!!}
  <div class="card">
    <h4 class="card-header">Agregar Personal</h4>
    <div class="card-body">
    <div id="mensaje"></div>
      <form id="data" autocomplete="off">

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <div class="row ">
          <div class="col-md-2 "></div>
          <div class="col-md-4 ">
            <div class="form-group text-center">
                {!!Form::label('Cedula','Cedula:')!!}
                <div  class="input-group ">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="nacional" checked>
                      <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                          Nica
                      </label>
                    </div>
                  </div>
                  {!!Form::text('Cedula_Personal',null,['class'=>'form-control border border-warning','placeholder'=>'xxx-xxxxxx-xxxxx', 
                  'autocomplete'=>'off', 'onkeypress'=>'return CharCedula(event,this,"#nacional");', 'onkeyup'=>'formatonica(this,"#nacional")','id'=>'cedu'])!!}  
                  <div class="input-group-append">
                    <button class="btn btn-primary fa fa-search" onclick="buscar();"  type="button"></button>
                    <button class="btn btn-primary fa fa-eraser" onclick="limpiar();"  type="button"></button>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  {!! Form::label('Contraseña:') !!}
                  <div class="input-group ">
                      <input autocomplete="new-password" id="password" type="password" name="password" class="form-control " placeholder="Defina una contraseña">
                      <div class="input-group-append">
                          <button class="btn btn-primary fa fa-eye" onclick="ShowPass(this);" type="button"></button>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        @include('personal.formulario.datos')
        <!--Tabla de cargos-->
        @can('cargo.index')
        <div class="row" >
          <div class="col-md-2"></div>
          <div class="col-md-8" id="lc">
            <div class="list-group " id="list-tab" role="tablist" style="height:15em;width:auto;">
              <table class="table table-hover table-dark" cellspacing="0" id="cargos" style="width:100%;">
                <thead>
                  <tr>
                    <th class="text-center">Cargo</th>
                    <th data-orderable="false"></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($Cargos as $cargo)
                    <tr>
                      <td >{!!$cargo->name!!}</td>
                      <td class="btn-primary text-center">
                        <input name="cargos[]" value="{!!$cargo->id!!}" id="{!!$cargo->id!!}" type="checkbox" OnClick='addcargo(this,"{!!$cargo->name!!}","{!!$cargo->id!!}")'>
                      </td>
                    </tr>  
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        @endcan
        <div class="text-center">
          <input class="btnSent btn btn-primary" type="button" onclick="guardar('guardar');" value="Guardar">
          <input class="btnSent btn btn-primary" type="button" onclick="guardar('guardarv');" value="Guardar y ver">
        </div>
      </form>
    </div>
  </div>
  {{-- Modal de cuando busca un cliente y este ya existe --}}
  @include('personal.formulario.modalBusqueda')
  @include('layouts.cargando.cargando')

@stop
<!--Ese escript no siempre se utiliza por lo que hacemos uso de la seccion script-->
@section("script")
  {!!Html::script("js/jskevin/cedulanica.js")!!} 
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  {!!Html::script("js/gijgo2/js/gijgo.js")!!}
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
  {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
  {!!Html::script("js/jskevin/kavvdt.js")!!}  
  <script>
    var listacargo=new Object();
    campocedula=$("#cedu");
    @can('cargo.index')
    $(document).ready(function(){
      createdt($("#cargos"),{col:0,dom:""});
    });

    //agregamos o eliminamos en la lista cargo
    function addcargo(elemento,cargo,key)//Comentar esta funcion
    {
      if(elemento.checked==true)
      {
        listacargo[key]=[cargo,key];
      }
      else
      {
        delete listacargo[key];
      }
    }
    @endcan
    function buscar()
    {
      cedula=campocedula.val();
      if(cedula.length>0)
      {
        var ruta="/personal/show/"+cedula;
        $("#loading").css('display','block');
        $.get(ruta,function(res){
          if(res.data)
          {
            // $("#Nombre").val(res.data.name);
            // $("#Apellido").val(res.data.last_name);
            // $("#Direccion").val(res.data.address);
            // $("#Edad").val(res.data.age);
            message(res.message, {objeto:$("#mensaje"),tipo:"danger",manual:true});
            // console.log(1);
            $('#modalBusquedaPersonal').modal('show');
            $("#mpNombre").val(res.data.name);
            $("#mpApellido").val(res.data.last_name);
            $("#mpDireccion").val(res.data.address);
            $("#mpEdad").val(res.data.age);
          }
          else
          {
            message(['Cedula no registrada, se puede registrar!'],{objeto:$("#mensaje"),tipo:"success",manual:true});
          }
          $("#loading").css('display','none');
        });
      }
    }

    function guardar(decision)
    {
      var ruta = "/personal";
      var token = $("#token").val();
      $(".btnSent").attr("disabled",true);
      $("#loading").css('display','block');
      //Consulta para añadir el nuevo personal
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data:$("#data").serialize(),
        success: function(res){
          if(res === 1)
          {
            if(decision=='guardarv')//si desea guardar y ver el personal recien agregado
              location.href="/personal/personalv/"+"1"+"/"+$("#cedu").val();
            if(decision=='guardar')//Si desea guardar e ingresar uno mas
            {
              message(["Se agrego el personal correctamente "],{manual:true})
              limpiar();
              $(".btnSent").attr("disabled",false);
              $("#loading").css('display','none');
              $('body').animate({scrollTop:0}, 'fast');
            }
          }
          else
          {
            message(res,{manual:true,tipo:"danger"});
            $(".btnSent").attr("disabled",false);
            $("#loading").css('display','none');
            $('body').animate({scrollTop:0}, 'fast');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{tipo:"danger"});
        $(".btnSent").attr("disabled",false);
        $("#loading").css('display','none');
        $('body').animate({scrollTop:0}, 'fast');
      });
    }
    function limpiar()
    {
      $("#cedu").val("");
      $("#Nombre").val("");
      $("#Apellido").val("");
      $("#Direccion").val("");
      $("#Edad").val("");
      for(var elemento in listacargo)
      {
        $("#"+elemento).prop('checked',false);
      }
      listacargo=new Object();
      
      $("#telefono").val("");
      $(".telefono-clone").remove();
      $("#password").val("");
    }
    function mostraredad()
    {
      if(year>0 && month>0 && day>0)
      {
        $("#Edad").val(edad);
      }
    }
    $("#nacional").on("change",function()
    {
      if($("#nacional").prop('checked')==true)//Si es idenficacion Nica
      {
        campocedula.val("");
      }
    });
        
    function addPhone()
    {
      telefono = $("#telefono");
      if(telefono.val() != "")
      {
        html= '<div class="row telefono-clone">'+
                  '<div class="col-md-2"></div>'+
                  '<div class="col-md-8">'+
                    '<div class="form-group">'+
                      '<div class="input-group ">'+
                        '<input value="'+ telefono.val() +'" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel">'+
                        '<div class="input-group-append">'+
                          '<button class="btn btn-danger fa fa-trash" onclick="removePhone(this);" type="button"></button>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>';

        telefono.val("");
        $("#telefonos").append(html);
        $("#telefono").focus()
      }
    }
    
    function removePhone(btn)
    {
      $(btn).parents('.telefono-clone').remove();
    }
  </script>  
@stop