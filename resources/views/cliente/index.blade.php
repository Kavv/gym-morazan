@extends('layouts.dashboard')
@section('css')
  {!!Html::style("css/gijgo2/css/gijgo.css")!!}
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')

<!--Arreglar toda la mierda que hiciste-->
    <div class="d-block bg bg-dark" style="padding-top: 10px; color:white;visibility:hidden;" id="main">
        @include('alert.mensaje')
        <div id="mensaje"></div>
        <!--class="table table-striped table-bordered"-->
        <table class="table table-bordered table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;" >
            <thead>
                <tr>
                    <th class="text-center" >Estado</th>
                    <th class="text-center" >Lección</th>
                    <th class="text-center" style="width:20%;">Cedula</th>
                    <th class="text-center" >Nombre</th>
                    <th class="text-center" >Apellido</th>
                    <th class="text-center" >Edad</th>
                    <th class="text-center" >Sexo</th>
                    <th class="text-center" data-orderable="false" style="width:15%;"></th>
                </tr>
            </thead >
            <tbody class="text-center" id="lista" >
                @include('cliente.recargable.listaclientes')
            </tbody>
        </table>
    </div>
{{--    --}}{{-- Modal de cuando busca un cliente y este ya existe --}}
{{--    @include('cliente.formulario.modalBusqueda')--}}

    @can('cliente.edit')

    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden" id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <div id="msgmodal"></div>
                        <?php $bt_columns = 6;
                        $edit = true; ?>
                        @include('cliente.formulario.datos')
                        
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        @can('cliente.edit')
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar();" id="actualizar" >Actualizar</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan
    
    @can('cliente.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section("script")
    {!!Html::script("js/jskevin/cedulanica.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!} 
    {!!Html::script("js/gijgo2/js/gijgo.js")!!}




    <script>

        var table;
        var fila;
    
        $('#datepicker').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });

        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"todo"]})
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        @can('cliente.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);

            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on('click', function () {
            var route="/cliente/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(res){
                    message(['Se elimino el cliente correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan
        let prueba;
        $('.edit').on( 'click', function () {
            
            limpiar();
            fila = $(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            route = "/cliente/detail/"+$(this).val();
            $.get(route, function(res){
                phones = res.phones;
                prueba = phones;
                data = res.data;
                servicios = res.services;
                
                console.log(phones)
                $("#CC").val(data.dni);
                $("#estado").val(data.status_id);
                $("#Nombre").val(data.name);
                $("#Apellido").val(data.last_name);
                $("#Edad").val(data.age);
                $("#Sexo").val(data.gender);
                $("#Direccion").val(data.address);
                $("#id").val(data.id);
                
                //Limpiamos la casilla del telefono
                $("#telefono").val("");
                // Limpiamos todos los telefonos que se estubiesen mostrando
                $("#telefonos").empty();
                for(var i=0; i < phones.length; i++)
                {
                    //enviamos el numero y especificamos que la agregacion sera manual(true)
                    addPhone(phones[i].number,true);
                }
                if(servicios.length > 0)
                    $("#servicios").val(servicios[0].id);
                else
                    $("#servicios").val("");

                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });

        @can('cliente.edit')
        function actualizar()
        {
            route="/cliente/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    if(res==1)
                    {
                        message(['Cliente editado correctamente'],{manual:true});
                        table.cell(fila.children('td')[0]).data( $("#CC").val());
                        table.cell(fila.children('td')[0]).data( $("#estado").val());
                        table.cell(fila.children('td')[1]).data( $("#Nombre").val());
                        table.cell(fila.children('td')[2]).data( $("#Apellido").val());
                        table.cell(fila.children('td')[3]).data( $("#Edad").val());
                        table.cell(fila.children('td')[4]).data( $("#Sexo").val());
                        table.cell(fila.children('td')[5]).data( $("#Direccion").val());
                        /* fila.children('td.botones').children('input.delete').removeAttr( "onclick");//Eliminamos la funcion onclick del boton eliminar
                        fila.children('td.botones').children('input.delete').attr( "onclick",'erase(\''+$("#cedula").val()+'\');');//Agregamos la funcion onclick con el nuevo parametro */
                        table=$("#Datos").DataTable().draw();
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        message(['No se actualizo: La cedula ingresada ya esta en uso'],{objeto:$("#modalMessage"), manual:true,tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            });
        }

        campocedula=$("#cedula");
        function buscar()
        {
            cedula=campocedula.val();
            if(cedula.length>0)
            {
                if(table.row(fila).data()[0] != $("#cedula").val())
                {
                    var ruta="/cliente/show/"+cedula;
                    $("#loading").css('display','block');
                    $.get(ruta,function(res){
                        $("#loading").css('display','none');
                        if(res.data)
                            message(['La cedula ya esta en uso por otro cliente!'],{objeto:$("#msgmodal"),tipo:"danger",manual:true});
                        else
                            message(['La cedula es valida, es posible editar!'],{objeto:$("#msgmodal"),tipo:"success",manual:true})
                    });
                }
                else
                    message(['La cedula es valida, es posible editar!'],{objeto:$("#msgmodal"),tipo:"success",manual:true})
            }
        }
        function limpiar()
        {
            $("#cedula").val("");
            $("#nombre").val("");
            $("#apellido").val("");
            $("#edad").val("");
            $("#sexo").val("");
            $("#direccion").val("");
            $("#ce").val("");
            
            $("#telefono").val("");
            $("#password").val("");
            $(".telefono-clone").remove();
        }
        $("#nacional").on("change",function()
        {
            if($("#nacional").prop('checked')==true)//Si es idenficacion Nica
            {
                campocedula.val("");
            }
        });
        function mostraredad()
        {
            if(edad!=0 && !isNaN(edad))
                $("#edad").val(edad);
        }
        @endcan
        
        function addPhone(telefono=null, manual=false)
        {
            if(telefono == null)
                telefono = $("#telefono").val();
            if(telefono != "")
            {
                html=   '<div class="telefono-clone col-md-3">'+
                            '<div class="form-group">'+
                                '<div class="input-group ">'+
                                    '<input value="'+ telefono +'" class="form-control telefono" placeholder="Ingrese el numero" name="telefono[]" type="tel">'+
                                    '<div class="input-group-append">'+
                                    '<button class="btn btn-danger fa fa-trash" onclick="removePhone(this);" type="button"></button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                $("#telefono").val("");
                $("#telefonos").append(html);
                if(manual == false)
                    $("#telefono").focus()
            }
        }
        
        function removePhone(btn)
        {
            $(btn).parents('.telefono-clone').remove();
        }

    </script>
@stop