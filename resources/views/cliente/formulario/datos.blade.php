        <div class="row ">
            @if($edit)
              <div class="col-md-{{ $bt_columns }}">
                  <div class="form-group">
                      {!!Form::label('Estado:')!!}
                      <select name="estado" id="estado" class="form-control border-warning">
                          @foreach($status as $stade)
                              <option value="{{$stade->id}}">{{$stade->name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
            @endif
            <div class="col-md-{{ $bt_columns }} ">
                <div class="form-group text-center">
                    {!! Form::label('Cedula', 'Cedula:') !!}
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" id="nacional" checked>
                                <label class="form-check-label badge badge-pill badge-info" for="defaultCheck1">
                                    Nica
                                </label>
                            </div>
                        </div>
                        {!! Form::text('Cedula_Cliente', null, ['class' => 'form-control border border-warning', 'placeholder' => 'xxx-xxxxxx-xxxxx', 'autocomplete' => 'off', 'onkeypress' => 'return CharCedula(event,this,"#nacional");', 'onkeyup' => 'formatonica(this,"#nacional")', 'id' => 'CC']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-primary fa fa-search" onclick="buscar();" type="button"></button>
                            <button class="btn btn-primary fa fa-eraser" onclick="limpiar();" type="button"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-{{ $bt_columns }}">
                <div class="form-group">
                    {!! Form::label('Nombre:') !!}
                    {!! Form::text('Nombre', null, ['id' => 'Nombre', 'class' => 'form-control border border-warning', 'placeholder' => 'Nombre del Cliente']) !!}
                </div>
            </div>
            <div class="col-md-{{ $bt_columns }}">
                <div class="form-group">
                    {!! Form::label('Apellido:') !!}
                    {!! Form::text('Apellido', null, ['id' => 'Apellido', 'class' => 'form-control border border-warning', 'placeholder' => 'Apellido del Cliente']) !!}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('Contraseña:') !!}
                    <div class="input-group ">
                        <input autocomplete="new-password" id="password" type="password" name="password" class="form-control border @if(!$edit) border-warning @endif " placeholder="Defina una contraseña">
                        <div class="input-group-append">
                            <button class="btn btn-primary fa fa-eye" onclick="ShowPass(this);" type="button"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('Edad:') !!}
                    <div class="input-group ">
                        {!! Form::text('Edad', null, ['id' => 'Edad', 'class' => 'form-control', 'placeholder' => 'Edad del Cliente']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-primary fa fa-calculator" onclick="mostraredad();" type="button"></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('Genero:') !!}
                    {!! Form::select('Sexo', ['Masculino' => 'Masculino', 'Femenino' => 'Femenino', 'No Definir' => 'No Definir  '], null, ['placeholder' => 'Seleeciona un genero', 'class' => 'form-control', 'id' => 'Sexo']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('Numero de teléfono:') !!}
                    <div class="input-group ">
                        {!! Form::tel('telefono[]', null, ['id' => 'telefono', 'class' => 'form-control', 'placeholder' => 'Ingrese el numero']) !!}
                        <div class="input-group-append">
                            <button class="btn btn-primary fa fa-plus" onclick="addPhone();" type="button"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="telefonos" class="row">
        </div>

        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('Direccion:') !!}
                    {!! Form::text('Direccion', null, ['id' => 'Direccion', 'class' => 'form-control', 'placeholder' => 'Direccion del Cliente']) !!}
                </div>
            </div>
        </div>
