<!-- Modal -->
<div class="modal fade" id="modalBusquedaCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"> Cliente encontrado </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="data-found">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="Nombre">Nombre:</label>
                            <input id="mcNombre" class="form-control input-modal" placeholder="N\A" type="text" disabled>
                        </div>
                        <div class="col-md-6">
                            <label for="Apellido">Apellido:</label>
                            <input id="mcApellido" class="form-control" placeholder="N\A" type="text" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="Edad">Edad:</label>
                            <input id="mcEdad" class="form-control" placeholder="N\A" type="text" disabled>
                        </div>
                        <div class="col-md-6">
                            <label for="Sexo">Sexo:</label>
                            <input id="mcSexo" class="form-control" placeholder="N\A" type="text" disabled>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="Direccion:">Direccion:</label>
                            <input id="mcDireccion" class="form-control" placeholder="N\A" type="text" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>