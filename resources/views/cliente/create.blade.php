@extends('layouts.dashboard')
@section('css')
{!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css') !!}
{!! Html::style("css/gijgo2/css/gijgo.css")!!}
@stop
@section('content')
  <div class="card">
    <h4 class="card-header">Agregar Cliente</h4>
    <div class="card-body">
      <div id="mensaje"></div>
      <form  id="data" autocomplete="off">

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        <?php $bt_columns = 4;
        $edit = false; ?>
        @include('cliente.formulario.datos')
        
        <div class="row py-2" style="background: #f0d467;">
          <div class="col-md-12 text-center">
            <h5 for="">Aplicar primer pago<span id="total"></span></h5>
          </div>
        </div>

        @include('pago.formulario.datos_pago')
          
        <div class="text-center">
          <input href="#" class="btnSent btn btn-primary col-md-2" onclick="customer_save('guardar');" value="Guardar">
        </div >
        
      </form>
    </div>
  </div>
  {{-- Modal de cuando busca un cliente y este ya existe --}}
  @include('cliente.formulario.modalBusqueda')
  @include('layouts.cargando.cargando')
@stop

@section("script")
  {!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js') !!}
  {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
  {!!Html::script("js/jskevin/cedulanica.js")!!} 
  {!!Html::script("js/jskevin/customer.js")!!} 
  {!!Html::script("js/gijgo2/js/gijgo.js")!!}
  <script>
    
    $('#fecha_pago').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });
    $('#inicio').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });
    $('#corte').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });
    function buscar()
    {
      cedula='';
      if($("#CC").length>0)
      {
        cedula=campocedula.val();
        if(cedula.length > 0)
        {
          var ruta="/cliente/show/"+cedula;
          $("#loading").css('display','block');
          $.get(ruta,function(res){
            if(res.data)
            {
              message(res.message, {objeto:$customer_msg,tipo:"danger",manual:true});
              $('#modalBusquedaCliente').modal('show');
              $("#mcNombre").val(res.data.name);
              $("#mcApellido").val(res.data.last_name);
              $("#mcEdad").val(res.data.age);
              $("#mcSexo").val(res.data.gender);
              $("#mcDireccion").val(res.data.address);
            }
            else
            {
              message(['Cedula no registrada, se puede registrar!'],{objeto:$customer_msg,tipo:"success",manual:true});
            }
            $("#loading").css('display','none');
          });
        }
      }
    }
    $("#leccion").change(function(){

      var leccion = $(this).val();
      var route = '/leccion/servicios/'+ leccion;
      $("#loading").css('display','block');
      $("#mensaje").empty();
      $("monto").val("");
      $("#servicios").empty();
      $.get(route, function(res){
        $("#servicios").append(res);
        $("#loading").css('display','none');

      }).fail(function() {
        message(['Ocurrio un error al cargar los servicios'],{tipo:"danger", manual:true});
        $("#loading").css('display','none');
      });
    });

    $("#servicios").change(function(){
      var monto = $("#servicios option:selected").data().amount;
      $("#monto").val(monto);
    });
    
    var showPass = 0;
    function ShowPass(element)
    {
        var btn = $(element);
        var input = $("#password");
        if(input.attr("type") == "password" )
            input.attr("type","text")
        else
            input.attr('type','password');
    }
  </script>
@stop