
  @foreach($clientes as $cliente)
    <tr @if($cliente->vetoed_id != null) class = "text-danger" @endif>
      <td>{{$cliente->state}}</td> 
      <td>{{$cliente->code}}</td> 
      <td>{{$cliente->dni}}</td> 
      <td >{{$cliente->name}}</td>    
      <td >{{$cliente->last_name}}</td>  
      <td >{{$cliente->age}}</td>  
      <td >{{$cliente->gender}}</td>  
      <td class="botones" >
        <button type="button" data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$cliente->id}}">Detalle</button>
        @can('cliente.destroy') 
        <button type="button" data-toggle="modal" data-target="#deleteModal"  class="btn btn-danger fa fa-trash " data-value="{{$cliente->id}}"></button>
        @endcan
      </td>
    </tr>
  @endforeach