
        <div id="mensajemenu"></div>
        <table class="table table-hover table-dark" id="tablamenu" cellspacing="0" style="width:100%;">
            <thead>
                <th class="text-center" data-orderable="false">Imagen</th>
                <th class="text-center">Descripcion</th>
                <th class="text-center">Costo</th>
                <th class="text-center" >Cantidad</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" style="overflow-y:scroll;"> 
                @foreach($menu as $comida)
                    <tr style="height:5em;" id="menucant{{$comida->id}}">
                        @if(strcmp($comida->image_name,"vacio.jpg")!=0)                        
                        <td><img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/{{$carpeta}}/{{$comida->image_name}}"  style="height:10em;border-radius: 40px;" value="{{$comida->path}}"></td> 
                        @else
                        <td><img src="https://fatsaas-2021.s3.us-east-2.amazonaws.com/{{$comida->image_name}}"  style="height:10em;border-radius: 40px;" value="{{$comida->path}}"></td> 
                        @endif
                        <td>{{$comida->description}}</td>    
                        <td>{{$comida->price}}</td>  
                        <td><input type="text" onkeypress="return valida(event);" class="form-control" style="width:100%" id="c_menucant{{$comida->id}}" placeholder="Cantidad"></td> 
                        <td>
                            <button class="btn btn-success" onclick="AddToListMenu(this)" value="{{$comida->id}}">Añadir</button>
                        </td>
                    </tr>  
                @endforeach
            </tbody>
        </table>
