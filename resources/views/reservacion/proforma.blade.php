<html>
    <head>
        <title>
            Proforma
        </title>
        <link href="/img/logosantana.jpg" type="image/x-icon" rel="shortcut icon" />
        <link rel="stylesheet" href="css/invoice/invoice.css">
    </head>

    <body>
    <br>
    <!-- Datos superiores, Encabezado -->
    <div class="top-data">
        <div>
            <img src="https://res.cloudinary.com/fguzman/image/upload/v1562814371/LOGO-Santana/logo-negro-2.png" style="position: absolute; width: 15%;">
        </div>

        <div id="datos_fac" style="text-align:center;">
            <h2  style="margin: 0; "> ALQUILER </h2>
            <h2  style="margin: 0; "> SANTANA </h2>
            <p  style="margin: 0; "><b><i> Para todo tipo de eventos sociales </i></b></p>
            <p style="margin: 0; "><b><i>
                        Alquiler de sillas, mesas, manteles, Pozo para regalo, Trajes con cintas para sillas, <br>
                        Servicio y Alquiler de Buffete, Toldos y Decoraciones en general </i></b></p>
            <p style="margin: 0; "><i>
                    Dir: Bo, La Fuente, frente al portón principal de la Escuela Normal Casa #26 <br>
                    Tels: 2289-4289, M = 8957-8863, 8888-6113, C = 5801-9277, <br>
                    Managua, Nicaragua. Email: yenssysantana17@gmail.com </i></p>
            <p style="margin: 0; "><b> RUC Nº 0012312770036R </b></p>
            <!-- Datos del cliente -->
        </div>
    </div>

    <br>

    <div>
        <h3 style="text-align: left">Cliente: {{$customer->name." ".$customer->lastname}} / {{$customer->dni}}</h3>
        <h3 style="text-align: left">Direccion: {{$reservation->event_address}}</h3>
        <!-- fecha -->
        <table>
            <thead>
            <tr style="text-align: center">
                <th> DIA </th>
                <th> MES </th>
                <th> AÑO </th>
            </tr>
            </thead>
            <tbody style="text-align: center;">
                <td> <?php echo date("d") ?> </td>
                <td> <?php echo date("m") ?> </td>
                <td> <?php echo date("y") ?> </td>
            </tbody>
        </table>
        <!--fin fecha -->
    </div>

    <br>

    <div style="width:100%">
        <table style="width:100%; text-align:center;">
            <thead>
                <tr>
                    <th style="width:10%">Cant.</th>
                    <th style="width:70%; text-align: left;">Descripcion</th>
                    <th style="width:10%">P/Unitario</th>
                    <th style="width:10%">TOTAL</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    //Filas por factura
                    $r_invoice = $reservation->invoice_row;
                    //La cantidad de filas multiplicado por la pagina de factura actual
                    $start = ($r_invoice * $index_invoice);
                    //Cantidad total de articulos reservados
                    $all_items = count($items) + count($menu);

                    $pago = 0;
                    //index_i = indice para los items, index_m = indice para los elementos del menu
                    $index_i = 0; $index_m = 0;

                    //Se repetira hasta que sea igual a la cantidad de filas establecidas en la reservación
                    for($i = 1 ; $i <= $all_items; $i++)
                    {
                        //Mientras la fila actual sea inferior a la maxima cantidad de filas que deberia tener la proforma
                        if($i < $all_items)
                        {
                            //Si existe el indice
                            if(isset($items[$index_i]))
                            {
                                //Verifica si el indice de fila establecido en el registro es igual al actual indice
                                if($items[$index_i]->index == $start + 1)
                                {
                                    $temp_pago = $items[$index_i]->qu * $items[$index_i]->up * $items[$index_i]->day;
                                    echo
                                    '<tr>'.
                                        '<td>'.$items[$index_i]->qu.'</td>'.
                                        '<td style="text-align: left;">'.$items[$index_i]->name.' - dias('.$items[$index_i]->day.')</td>'.
                                        '<td>'.$items[$index_i]->up.'</td>'.
                                        '<td>'.number_format($temp_pago, 2, '.', ',').'</td>'.
                                    '</tr>';
                                    $pago += $temp_pago;
                                    $index_i++;
                                    $start++;
                                }
                                else if($items[$index_i]->index < $start +1)
                                    $index_i++;

                            }

                            if(isset($menu[$index_m]))
                            {
                                if($menu[$index_m]->index == $start + 1)
                                {
                                    $temp_pago = $menu[$index_m]->qu * $menu[$index_m]->up * $menu[$index_m]->day;
                                    echo
                                    '<tr>'.
                                        '<td>'.$menu[$index_m]->qu.'</td>'.
                                        '<td style="text-align: left;">'.$menu[$index_m]->description.' - dias('.$menu[$index_m]->day.')</td>'.
                                        '<td>'.$menu[$index_m]->up.'</td>'.
                                        '<td>'.number_format($temp_pago, 2, '.', ',').'</td>'.
                                    '</tr>';
                                    $pago += $temp_pago;
                                    $index_m++;
                                    $start++;
                                }
                                else if($menu[$index_m]->index < $start +1)
                                    $index_m++;
                            }
                        
                        }



                        $last_item = $index_i + $index_m;
                        if($last_item == $r_invoice)
                            break;
                    }

                    for(; $last_item < $r_invoice; $last_item++)
                    {
                        echo '<tr ><td> - </td><td></td><td></td><td></td></tr>';
                    }
                ?>

            </tbody>
            <tfoot>
                <?php

                    $iva = $reservation->tax;
                    $descuento = $reservation->discount;
                    $r_add = 0;

                    if($iva == 0 && $descuento == 0)
                    {
                        $r_add = 1;
                        echo
                            '<tr>
                                <td colspan="3" style="text-align: right"> <b> Gran Total </b></td>
                                <td style="text-align: center">'.number_format($pago, 2, '.', ',').'</td>
                            </tr>';
                    }

                    if($iva != 0 && $descuento == 0)
                    {
                        $temp_iva = $pago*$iva;
                        $total = $pago + $temp_iva;
                        $r_add = 3;
                        echo
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                '<td style="text-align: center">'.number_format($pago, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                                '<td style="text-align: center">'.number_format($temp_iva, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                '<td style="text-align: center">'.number_format($total, 2, '.', ',').'</td>'.
                            '</tr>';
                    }

                    if($iva == 0 && $descuento != 0)
                    {

                        $temp_descuento = $pago*$descuento;
                        $total = $pago - $temp_descuento;
                        $r_add = 3;
                        echo
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                '<td style="text-align: center">'.number_format($pago, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>Descuento('.($descuento*100).'%)</b></td>'.
                                '<td style="text-align: center">-'.number_format($temp_descuento, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                '<td style="text-align: center">'.number_format($total, 2, '.', ',').'</td>'.
                            '</tr>';
                    }

                    if($iva != 0 && $descuento != 0)
                    {
                        $temp_iva = $pago*$iva;
                        $temp_descuento = $pago*$descuento;
                        $total = $pago-$temp_descuento+$temp_iva;
                        $r_add = 4;
                        echo
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>Sub Total</b></td>'.
                                '<td style="text-align: center">'.number_format($pago, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>Descuento('.($descuento*100).'%)</b></td>'.
                                '<td style="text-align: center">-'.number_format($temp_descuento, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b>IVA</b></td>'.
                                '<td style="text-align: center">'.number_format($temp_iva, 2, '.', ',').'</td>'.
                            '</tr>'.
                            '<tr>'.
                                '<td colspan="3" style="text-align: right"> <b> Gran Total </b></td>'.
                                '<td style="text-align: center">'.number_format($total, 2, '.', ',').'</td>'.
                            '</tr>';
                    }

                    ?>


            </tfoot>
        </table>

        <!-- La suma de las filas de la factura + las agregadas del detalle final -->
        @if($r_add + $r_invoice >= 5)
        <div class="page-break"></div>
        @endif

        <section id="clausulas">
            <p>
                CLAUSULA I. El cliente se compromete a pagar la cantidad de ______________ al momento de recibir los artículos y dejará en concepto de garantía su cédula de identidad (en buen estado), la que será devuelta al cliente siempre y cuando garantice el cuido de los mismos.
            </p>
            <p>
                CLAUSULA II. En caso de que el cliente cause daño a un artículo determinado de los que ofrece Alquiler Santana, este se compromete a pagar el daño causado, es decir: Si quiebra, rompe, quema o daña mesas, sillas, manteles, cristalería u otros artículos deberán pagar el valor del mismo según lo establecido por la propietaria.
            </p>
            <p>
                CLAUSULA III. Alquiler Santana ofrece el servicio de 24 horas de alquiler, si el cliente sobrepasa la hora establecida de entrega en el contrato de alquiler, el cliente pagará la diferencia como si solicito el servicio nuevamente.
            </p>
            <p>
                CLAUSULA IV. Alquiler Santana ofrece esta proforma valida por 31 dias, si se excediera el tiempo de validez, se debe solicitar una nueva por causa de variaciones en el precio.
            </p>
            <p>
                CLAUSULA V. Estableciendo formal acuerdo entre las partes, el cliente acepta cada una de las cláusulas del presente
                contrato, una vez firmado el mismo <b>no hay devolución del dinero</b>. Dado en la ciudad de Managua a los ___ días del mes de _________ del año _____, firmamos común acuerdo.
            </p>
            <p><b>
                    NOTA. La reserva se hace efectiva con la cancelación del 50% del costo en base al precio total sobre el pedido solicitado, cuando el contrato este aceptado y firmado por ambas partes, Alquiler Santana no admitira la devolucion de dinero.
            </b></p>
        </section>

        <section id="firmas">
            <div class="right" style="width:50%; text-align:center">
                <span>
                    __________________________________
                    <br>
                    Cliente<br>
                    {{$customer->name." ".$customer->lastname}}</span>
                </span>
            </div>
            <div class="left" style="width:50%; text-align:center">
                <span>
                    __________________________________
                    <br>
                    Propietaria<br> 
                    Yenssy Smyrna Santana
                </span>
            </div>
        </section>

        </div>
    </body>

</html>