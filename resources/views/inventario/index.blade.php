@extends('layouts.dashboard')
@section('css')
    <style>
        .content-wrapper 
        {
            background: #343a40 !important;
        }
    </style>
@stop
@section('content')
    <div class="d-block bg-dark" style="color:white; padding-top: 10px;visibility:hidden;" id="main">

        @include('alert.mensaje')
        <div id="mensaje"></div>
        <table class="table table-hover table-dark" cellspacing="0" id="Datos" style="width:100%;">
            <thead >

                <th class="text-center" style=" max-width:20%;">Servicio</th>
                <th class="text-center" style=" max-width:30%;">Nombre</th>
                <th>Cantidad</th>
                <th>Costo Alquiler</th>
                <th>Costo Objeto</th>
                <th class="text-center" data-orderable="false" style="width:20%"></th>
            </thead>
            <tbody class="text-center" id="lista"> 
                @include('inventario.recargable.listainventario')
            </tbody>
        </table>
        
    </div>
    @can('inventario.edit')
    <!-- Modal -->
    <div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Editar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalMessage"></div>
                    <form id="data">
                        <input type="hidden"  id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group text-center">
                                    {!!Form::label('Servicio:')!!}
                                    <div  class="input-group ">

                                        <select class="form-control border border-warning" name="ID_Servicio" id="servicios">
                                            <option ></option>
                                            @foreach($servicios as $servicio)
                                            <option class="option{!!$servicio->id!!}" value='{!!$servicio->id!!}'>{!!$servicio->name!!}</option>
                                            @endforeach
                                        </select>

                                        <div class="input-group-append">
                                            @can('servicio.create')
                                            <button class="btn btn-primary" data-toggle="modal" data-target="#addService" type="button" id="btnAddService">Nuevo Servicio</button>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Nombre:')!!}
                                    {!!Form::text('Nombre',null,['id'=>'nombre', 'class'=>'form-control border border-warning','placeholder'=>'Nombre del articulo'])!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Cantidad:')!!}
                                    {!!Form::text('Cantidad',null,['id'=>'cantidad','class'=>'form-control','placeholder'=>'Cantidad'])!!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Costo de Alquiler:')!!}
                                    {!!Form::text('Costo_Alquiler',null,['id'=>'CA','class'=>'form-control','placeholder'=>'Costo de Alquiler'])!!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!!Form::label('Costo de Objeto:')!!}
                                    {!!Form::text('Costo_Objeto',null,['id'=>'CO','class'=>'form-control','placeholder'=>' Costo de Objeto'])!!}
                                </div>
                            </div>
                        </div> 

                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar();" id="actualizar">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan

    @can('servicio.create')
    <!-- MODAL PARA AGREGAR SERVICIO -->
    <div class="modal fade" id="addService" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar Nuevo Servicio</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <div id="modalMessageService"></div>
            <div class="row ">
                <div class="col-md-12 text-center">
                <div class="form-group">
                    {!!Form::label('Nombre')!!}
                    {!!Form::text('serviceName',null,['id'=>'serviceName','class'=>'form-control border border-warning','placeholder'=>'Nombre del servicio'])!!}
                </div>
                </div>
            </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" onclick="serviceSave();">Guardar</button>
            </div>
        </div>
        </div>
    </div>
    @endcan



    @can('personal.destroy')
        @include('layouts.modal.deleteModal')
    @endcan

    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!}   
    <script>
        var servi=0;
        var item=0;
        var table;
        var fila;
        $(document).ready( function () {
            table=createdt($('#Datos'),{buscar:'{!!session("valor")!!}',col:1,cant:[10,20,-1],cantT:[10,20,"Todo"]});
            $("#main").css("visibility", "visible");
            $("#loading").css('z-index',1060);
        });
        
        @can('inventario.edit')
        $('.edit').on( 'click', function () {
            fila=$(this).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos editar
            var row=table.row(fila).data();//Tomamos el contenido de la fila s
            $("#nombre").val(row[1]);
            $("#cantidad").val(row[2]);
            $("#CA").val(row[3]);
            $("#CO").val(row[4]);
            $("#id").val($(this).val());
            //Si el servicio es eliminado previamente, la siguiente linea genera error
            //$("#servicios option[class=option"+ $(this).val() +"]").prop("selected",true);
            $("#servicios").val($(this).data('servicio'));
        });
        function actualizar()
        {
            route="/inventario/"+$("#id").val();
            var token=$("#token").val();
            $("#actualizar").attr('disabled',true);
            $("#loading").css('display','block');
            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'PUT',
                dataType: 'json',
                data:$("#data").serialize(),
                success: function(res){
                    console.log(res);
                    if(res==1)
                    {
                        message(['Articulo editado correctamente'],{manual:true});
                        console.log($("#servicios option[selected]").text());
                        table.cell(fila.children('td')[0]).data( $("#servicios option[value="+ $("#servicios").val() +"]").text());
                        table.cell(fila.children('td')[1]).data( $("#nombre").val());
                        table.cell(fila.children('td')[2]).data( $("#cantidad").val());
                        table.cell(fila.children('td')[3]).data( $("#CA").val());
                        table.cell(fila.children('td')[4]).data( $("#CO").val());
                        table=$("#Datos").DataTable().draw(false);
                        $("#Edit").modal('toggle');
                        $('body').animate({scrollTop:0}, 'fast');
                    }
                    else
                    {
                        message(res,{objeto:$("#modalMessage"),manual:true,tipo:"danger"});
                    }
                    $("#actualizar").attr('disabled',false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#actualizar").attr('disabled',false);
                $("#loading").css('display','none');
                message(jqXHR,{objeto:$("#modalMessage"), tipo:"danger"});
            });
        }
        @endcan
        
        @can('inventario.destroy')
        var row;
        $('#deleteModal').on('show.bs.modal', function (event) {
            {{-- Mantenemos el id del elemento seleccionado --}}        
            var button = $(event.relatedTarget); // Button that triggered the modal
            var value = button.data('value'); // Extract info from data-* attributes
            $("#id").val(value);


            {{-- Mantenemos la fila del elemento seleccionado --}}
            row = button.parents('tr');
        });

        $('#delete').on( 'click', function () {
            var route="/inventario/"+$("#id").val();
            var token=$("#token").val();
            $("#delete").attr('disabled',true);
            $("#loading").css('display','block');

            $.ajax({
                url: route,
                headers:{'X-CSRF-TOKEN': token},
                type: 'DELETE',
                dataType: 'json',
                success: function(){
                    message(['Se elimino el articulo correctamente'],{manual:true});
                    table.row(row).remove().draw( false );
                    $("#delete").attr('disabled',false);
                    $("#loading").css('display','none');
                    $("#deleteModal").modal('toggle');
                    $('body').animate({scrollTop:0}, 'fast');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{objeto:$("#deleteModalMessage"), tipo:"danger"});
                $("#delete").attr('disabled',false);
                $("#loading").css('display','none');
            });
        });
        @endcan
           
    @can('servicio.create')
    function serviceSave()
    {
      var ruta="/servicio";
      var token=$("#token").val();
      $("#btnAddService").attr("disabled",true);
      $("#loading").css('display','block');
      $.ajax({
        url: ruta,
        headers: {'X-CSRF-TOKEN': token},
        type: 'POST',
        dataType: 'json',
        data: {"Nombre":$('#serviceName').val()},
        success: function(result){
          if(result === 1)
          {
            $.get("{{route('inventario.servicelist')}}", function(res){

              htmlCode = "<option ></option>";
              focus = $("#serviceName").val();
              for(i=0; i< res.length; i++)
              {
                if(focus != res[i].name)
                  htmlCode += "<option value='" + res[i].id + "'>" + res[i].name + "</option>";
                else
                  htmlCode += "<option selected value='" + res[i].id + "'>" + res[i].name + "</option>";

              }

              $("#servicios").empty();
              $("#servicios").append(htmlCode);
              $("#addService").modal("toggle");
              $("#serviceName").val("");

              $("#btnAddService").attr("disabled",false);
              $("#loading").css('display','none');
              message(["Servicio agregado exitosamente!"],{objeto:$("#modalMessage"),manual:true})
              $('body').animate({scrollTop:0}, 'fast');
            });
          } else {
            message(result,{objeto:$("#modalMessageService"), manual:true, tipo:'danger'});
            $("#btnAddService").attr("disabled",false);
            $("#loading").css('display','none');
          }
        }
      }).fail( function( jqXHR, textStatus, errorThrown ) {
        message(jqXHR,{objeto:$("#modalMessageService"), tipo:"danger"});
        $("#btnAddService").attr("disabled",false);
        $("#loading").css('display','none');
      });
    }
    @endcan
    </script>
@stop