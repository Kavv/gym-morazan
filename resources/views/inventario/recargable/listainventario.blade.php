                @foreach($inventario as $item)
                    <tr>
                        <td>{{$item->servicio['name']}}</td> 
                        <td>{{$item->name}}</td>    
                        <td>{{$item->quantity}}</td>  
                        <td>{{$item->rental_price}}</td>  
                        <td>{{$item->buy_price}}</td>  
                        <td class="botones">
                            @can('inventario.edit')
                            <button data-toggle="modal" data-target="#Edit" class="btn btn-primary edit" value="{{$item->id}}" data-servicio="{{$item->servicio['id']}}">Editar</button>
                            @endcan
                            @can('inventario.destroy')
                            <button data-toggle="modal" data-target="#deleteModal" class="btn btn-danger fa fa-trash" data-value="{{$item->id}}"></button>
                            @endcan
                        </td>
                    </tr>
                @endforeach

