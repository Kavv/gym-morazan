        

          <div class="row ">
            <div class="col-md-3"></div>
            
            <div class="col-md-6 ">
              <div class="form-group text-center">
                {!!Form::label('Servicio:')!!}
                <div  class="input-group ">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="checkServicio" checked>
                      <span class='fa fa-eraser '></span>
                    </div>
                  </div>

                  <select class="form-control border border-warning" name="ID_Servicio" id="servicios">
                    <option ></option>
                    @foreach($servicios as $servicio)
                    <option value='{!!$servicio->id!!}'>{!!$servicio->name!!}</option>
                    @endforeach
                  </select>

                  <div class="input-group-append">
                    @can('servicio.create')
                    <button class="btn btn-primary" data-toggle="modal" data-target="#addService" type="button" id="btnAddService">Nuevo Servicio</button>
                    @endcan
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row ">
            <div class="col-md-2"></div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Nombre:')!!}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="checkNombre" checked>
                      <span class='fa fa-eraser'></span>
                    </div>
                  </div>
                  {!!Form::text('Nombre',null,['id'=>'nombre', 'class'=>'form-control border border-warning','placeholder'=>'Nombre del articulo'])!!}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Cantidad:')!!}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="checkCantidad" checked>
                      <span class='fa fa-eraser'></span>
                    </div>
                  </div>
                  {!!Form::text('Cantidad',null,['id'=>'cantidad','class'=>'form-control','placeholder'=>'Cantidad'])!!}
                </div>
              </div>
            </div>
          </div>

           <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Costo de Alquiler:')!!}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="checkCA" checked>
                      <span class='fa fa-eraser'></span>
                    </div>
                  </div>
                  {!!Form::text('Costo_Alquiler',null,['id'=>'CA','class'=>'form-control','placeholder'=>'Costo de Alquiler'])!!}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                {!!Form::label('Costo de Objeto:')!!}
                <div class="input-group">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <input type="checkbox" id="checkCO" checked>
                      <span class='fa fa-eraser'></span>
                    </div>
                  </div>
                  {!!Form::text('Costo_Objeto',null,['id'=>'CO','class'=>'form-control','placeholder'=>' Costo de Objeto'])!!}
                </div>
              </div>
            </div>
          </div> 


          