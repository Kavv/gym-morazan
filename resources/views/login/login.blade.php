<!DOCTYPE html>
<html lang="en">
<head>
	<title>GIMNASIO MORAZÁN</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    {!!Html::style("css/cssF/vendor/bootstrap/css/bootstrap.min.css")!!}
    
    {!!Html::style("css/login-kavv/fonts/font-awesome-4.7.0/css/font-awesome.min.css")!!}
    {!!Html::style("css/login-kavv/fonts/Linearicons-Free-v1.0.0/icon-font.min.css")!!}
    {!!Html::style("css/login-kavv/vendor/animate/animate.css")!!}
    {!!Html::style("css/login-kavv/vendor/css-hamburgers/hamburgers.min.css")!!}
    {!!Html::style("css/login-kavv/vendor/animsition/css/animsition.min.css")!!}
    {!!Html::style("css/login-kavv/vendor/select2/select2.min.css")!!}
    {!!Html::style("css/login-kavv/vendor/daterangepicker/daterangepicker.css")!!}
    {!!Html::style("css/login-kavv/css/util.css")!!}
    {!!Html::style("css/login-kavv/css/main.css")!!}
    <style>
        .show-pass{
            display: flex; 
            align-items: center; 
            font-size: 120%; 
            margin-right: 1px; 
            text-align: center; 
            cursor: pointer; 
            width: 12%!important;
        }
        .password{
            width: 88%!important;
        }
        .nav-link:not(.active):hover{
            color: #000!important;
            background: rgb(204, 204, 204);
        }
        .wrap-login100 {
            background: #fff;
        }
        .login100-form-title{
            color:#000;
        }
    </style>
    
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100" style="background-image: url('img/fondo.jpg');">
			<div class="wrap-login100 p-t-30 p-b-50">
				<span class="login100-form-title p-b-41">
					Iniciar Sessión
				</span>
                <ul class="nav nav-pills mb-3 d-flex justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Atleta</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-trainer-tab" data-toggle="pill" href="#pills-trainer" role="tab" aria-controls="pills-trainer" aria-selected="false">Entrenador</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Gerencia</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <form id="data_atleta" class="login100-form validate-form p-b-33 p-t-5">
        
                            <div class="wrap-input100 validate-input" data-validate = "">
                                <input class="input100" type="text" name="username" placeholder="Identificación" onkeypress="BlockEnter(event, 1 )">
                                <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                            </div>
                            
                            <div class="wrap-input100 validate-input input-group" data-validate="">
                                <input class="input100 password" type="password" name="pass" placeholder="Contraseña" onkeypress="BlockEnter(event, 1)">
                                <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                                <div class="btn btn-info input-group-append show-pass eye" onclick="ShowPass(this);">
                                    <span class="lnr lnr-eye"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 text-center" style="color:#de237c;"> <span id="message">  </span> </div>
        
                            <div class="container-login100-form-btn m-t-32">
                                <button id="btn-login" type="button" class="login100-form-btn" onclick="login_atleta();">
                                    Iniciar
                                </button>
                            </div>
        
                        </form>
                        
                    </div>
                    <div class="tab-pane fade" id="pills-trainer" role="tabpanel" aria-labelledby="pills-trainer-tab">
                        <form id="data_trainer" class="login100-form validate-form p-b-33 p-t-5">

                            <div class="wrap-input100 validate-input3" data-validate = "">
                                <input class="input100" type="text" name="username" placeholder="Usuario" onkeypress="BlockEnter(event, 3)">
                                <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                            </div>
                            
                            <div class="wrap-input100 validate-input3 input-group" data-validate="">
                                <input class="input100 password" type="password" name="pass" placeholder="Contraseña" onkeypress="BlockEnter(event, 3)">
                                <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                                <div class="btn btn-info input-group-append show-pass eye" onclick="ShowPass(this);">
                                    <span class="lnr lnr-eye"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 text-center" style="color:#de237c;"> <span id="message3">  </span> </div>

                            <div class="container-login100-form-btn m-t-32">
                                <button id="btn-login3" type="button" class="login100-form-btn" onclick="login_trainer();">
                                    Iniciar
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <form id="data_user" class="login100-form validate-form p-b-33 p-t-5">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

                            <div class="wrap-input100 validate-input2" data-validate = "">
                                <input class="input100" type="text" name="username" placeholder="Usuario" onkeypress="BlockEnter(event, 2)">
                                <span class="focus-input100" data-placeholder="&#xe82a;"></span>
                            </div>
                            
                            <div class="wrap-input100 validate-input2 input-group" data-validate="">
                                <input class="input100 password" type="password" name="pass" placeholder="Contraseña" onkeypress="BlockEnter(event, 2)">
                                <span class="focus-input100" data-placeholder="&#xe80f;"></span>
                                <div class="btn btn-info input-group-append show-pass eye" onclick="ShowPass(this);">
                                    <span class="lnr lnr-eye"></span>
                                </div>
                            </div>
                            
                            <div class="col-md-12 text-center" style="color:#de237c;"> <span id="message2">  </span> </div>

                            <div class="container-login100-form-btn m-t-32">
                                <button id="btn-login2" type="button" class="login100-form-btn" onclick="login_user();">
                                    Iniciar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
            @include('layouts.cargando.hexagonos')
		</div>
	</div>
	


	<div id="dropDownSelect1"></div>
	
    {!!Html::script("js/jsF/vendor/jquery/jquery.min.js")!!}
    {!!Html::script("js/jsF/vendor/popper/popper.min.js")!!}
    {!!Html::script("js/jsF/vendor/bootstrap/js/bootstrap.min.js")!!}
    
    {!!Html::script("js/login-kavv/vendor/animsition/js/animsition.min.js")!!}
    {!!Html::script("js/login-kavv/vendor/select2/select2.min.js")!!}
    {!!Html::script("js/login-kavv/vendor/daterangepicker/moment.min.js")!!}
    {!!Html::script("js/login-kavv/vendor/daterangepicker/daterangepicker.js")!!}
    {!!Html::script("js/login-kavv/vendor/countdowntime/countdowntime.js")!!}
    {!!Html::script("js/login-kavv/js/main.js")!!}
    

</body>

<script>
    function login_user()
    {
        if(Validation(2))
        {
            $(".input100").prop("readonly",true);
            $("#carga").css("display","block");
            $("#btn-login2").attr("disabled",true);

            route = "{{route('login.in')}}";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:$("#data_user").serialize(),
                success: function(res){
                    if(res.action === 1)
                        location.href = "{{route('principal.index')}}";
                    else
                    {
                        $("#message2").empty();
                        $("#message2").append(res.message);
                        $("#carga").css("display", "none");
                        $("#btn-login2").attr('disabled',false);
                        $(".input100").prop("readonly",false);
                    }
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                
                $("#message2").empty();
                $("#message2").append("Upsss! Ocurrio un error");
                $("#carga").css("display", "none");
                $("#btn-login2").attr('disabled',false);
                $(".input100").prop("readonly",false);
                
            });
        }
    }
    function login_atleta()
    {
        if(Validation())
        {
            $(".input100").prop("readonly",true);
            $("#carga").css("display","block");
            $("#btn-login").attr("disabled",true);

            route = "{{route('login.login_atleta')}}";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:$("#data_atleta").serialize(),
                success: function(res){
                    if(res.code === 1)
                    {
                       location.href = "/mi_calendario";
                    }
                    else
                    {
                        $("#message").empty();
                        $("#message").append(res.msg);
                        $("#carga").css("display", "none");
                        $("#btn-login").attr('disabled',false);
                        $(".input100").prop("readonly",false);
                    }
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                
                $("#message").empty();
                $("#message").append("Upsss! Ocurrio un error");
                $("#carga").css("display", "none");
                $("#btn-login").attr('disabled',false);
                $(".input100").prop("readonly",false);
                
            });
        }
    }
    function login_trainer()
    {
        if(Validation(3))
        {
            $(".input100").prop("readonly",true);
            $("#carga").css("display","block");
            $("#btn-login3").attr("disabled",true);

            route = "{{route('login.login_trainer')}}";
            var token = $("#token").val();
            $.ajax({
                url: route,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data:$("#data_trainer").serialize(),
                success: function(res){
                    if(res.code === 1)
                    {
                       location.href = "/mi_calendario_de_trabajo";
                    }
                    else
                    {
                        $("#message3").empty();
                        $("#message3").append(res.msg);
                        $("#carga").css("display", "none");
                        $("#btn-login3").attr('disabled',false);
                        $(".input100").prop("readonly",false);
                    }
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                $("#message3").empty();
                $("#message3").append("Upsss! Ocurrio un error");
                $("#carga").css("display", "none");
                $("#btn-login3").attr('disabled',false);
                $(".input100").prop("readonly",false);
                
            });
        }
    }
    function BlockEnter(e, option)
    {
        tecla = (document.all) ? e.keyCode :e.which; 
        if(tecla == 13)
        {
            if(option == 1)
                login_atleta();
            if(option == 2)
                login_user();
            if(option == 3)
                login_trainer();
        } 
    }
</script>
</html>