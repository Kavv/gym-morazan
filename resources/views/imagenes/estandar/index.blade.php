@extends('layouts.dashboard')
@section('css')
    <style>
        .full-center{
            align-items:center;
            display:flex;
        }
    </style>
@stop

@section('content')
    {{--video--}}
    <div class="row" style="margin-left: 0px; background: #343a40">
        <div id="vid" class="video-container img-fluid black col-md-12" id="header">
            @if(in_array($old_type,array('webm', 'mp4')))
            <video autoplay  loop  controls height="720" class="col-md-12">
                <!-- Recorre todos los videos y crea una source para cada video pero supongo q solo hay uno -->
                <source id="video{{ $video->id }}" src="{{ $video->image_url }}"  type="video/mp4">
            </video>
            @else
            <img id="video{{ $video->id }}" src="{{ $video->image_url  }}"height="720" class="w-100">
            @endif
            @can('imagenes.create')
            <button id="boton_video" type="button" class="btn btn-primary tu" data-toggle="modal" data-target="#videoSaveModal" >Cambiar</button>
            @endcan
            @can('imagenes.destroy')
            <button id="botonEVid" type="button" class="btn btn-danger tu" data-toggle="modal" data-target="#videoDeleteModal" >Borrar</button>
            @endcan
        </div>
    </div>

    <!-- Agrega todas las categorias en un arreglo -->
    {{--galeria--}}
    @php $n_cat = array();
     foreach($categories as $category)
        $n_cat[]= $category->title;
    @endphp

    <div class="row" style="margin-left: 0">
        @foreach( $images as $indexKey=>$image)
            <div id="category{{$image['categories_id']}}" class="col-12 col-md-3 col-lg-3 services border border-secondary"
                onclick="ShowModalPart2(this);" style="padding:0;background-image: url({{ $image['image_url'] }}) ">
                <div class="screen full-center" style="cursor: hand">
                    <div class="row full-height t-div">
                        <div class="col-12 white-text text-center t-child">
                            <h3 id="Slide{{ $indexKey+1 }}" class="satisfy subtitle">{{ $n_cat[$indexKey+1] }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {{--BORRAR TODO ESTO--}}
    {{--carusel--}}
    {{--<div class="row" style="margin-left: 0em; background: #343a40">--}}
        {{--<div id="gallery" class="carousel slide border border-secondary col-md-12" data-ride="carousel" data-interval="false" style="padding:0px;cursor: hand">--}}
            {{--inicio de indicadores "responsive" conforme las imagenes agregadas--}}
            {{--<ol class="carousel-indicators">--}}
                {{--@foreach( $carus as $caru )--}}
                    {{--<li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>--}}
                {{--@endforeach--}}
            {{--</ol>--}}
            {{--Fin de indicadores--}}

            {{--<div class="carousel-inner" >--}}
                {{--@foreach( $carus as $indexKey=>$caru )--}}
                    {{--<div class="cb carousel-item {{$loop->first ? 'active' : ''}}" style=" width:100%; height: 800px">--}}
                        {{--<img id="image{{ $caru->id }}" class="w-100 d-block" style="height: 800px" src="{{ $caru->image_url }}">--}}
                        {{--boton para activacion del modal--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}

            {{--<a class="carousel-control-prev" href="#gallery" role="button" data-slide="prev">--}}
                {{--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
                {{--<span class="sr-only">Anterior</span>--}}
            {{--</a>--}}

            {{--<a class="carousel-control-next" href="#gallery" role="button" data-slide="next">--}}
                {{--<span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
                {{--<span class="sr-only">Siguiente</span>--}}
            {{--</a>--}}

            {{--@can('imagenes.create')--}}
                {{--<button id="boton_down2" type="button" class="btn btn-primary" onclick="SelectPart(3);"--}}
                    {{--data-toggle="modal" data-target="#saveModal" >Cambiar</button>--}}
            {{--@endcan--}}

            {{--@can('imagenes.destroy')--}}
                {{--<button id="botonEImg" type="button" class="btn btn-danger" data-toggle="modal"--}}
                    {{--onclick="SelectPart(3);" data-target="#imageDeleteModal" >Eliminar</button>--}}
            {{--@endcan--}}

        {{--</div>--}}
    {{--</div>--}}

    {{--inicio del modal 1 upload--}}
    <div class="modal fade" id="videoSaveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div id="mContent" class="modal-content">
                {{--cabezera del modal--}}
                <div id="mHead" class="modal-header">
                    <h5 class="modal-title mx-auto" >Subir Video o Imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{--Cuerpo del modal--}}
                <div id="mBody" class="modal-body">
                    <div class="mx-auto">
                        <form class="mx-auto" role="form" enctype="multipart/form-data" method="post" action="">
                            <div class="" id="mensaje">

                            </div>

                            <div class="col-md-12 text-center" id="video" >

                            </div>

                            <input type="hidden" id="token" name="csrf-token" value="{{ csrf_token() }}"> 

                            <div class=" form-group mx-auto m-4 {{$errors->has('name') ? 'has-error' : ''}}">
                                <input type="file" name="image_name2" class="form-control btn btn-secondary" id="videoFileInput">
                            </div>
                            @can('imagenes.create')
                            <div class="form-group text-center">
                                <button id="boton_submit" onclick="ChangeVideo();" type="button" class="btn btn-primary"> Subir </button>
                            </div>
                            @endcan
                        </form>
                    </div>
                </div>
                {{--pie del modal--}}
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    {{--Fin del modal--}}

    {{--inicio del modal 1 borrar video--}}
    <div class="modal fade" id="videoDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div id="mContent" class="modal-content">
                {{--cabezera del modal--}}
                <div id="mHead" class="modal-header">
                    <h5 class="modal-title mx-auto"> Borrar Contenido </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{--Cuerpo del modal--}}
                <div id="mBody" class="modal-body">
                    <div class="" id="mensajeB">

                    </div>
                    <p class="text-center"> <b> ¿Esta seguro/a que desea borrar el contenido? </b> </p>
                </div>
                {{--pie del modal--}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    @can('imagenes.destroy')
                        <button id="borrarVideo" type="button" class="btn btn-danger"> Borrar </button>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    {{--Fin del modal--}}

    {{--Modal para activar el carusel dentro de cada categoria--}}
    <div class="modal fade bd-example-modal-lg" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div id="mContent" class="modal-content" style="width: 800px">
            
                <div class="modal-body">
                    {{--Cuerpo del modal--}}
                    <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel" data-interval="false" >
                        <div id="interno" class="carousel-inner">

                        </div>
                        <!-- Boton izquierda -->
                        <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <!-- Boton derecha -->
                        <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    @can('imagenes.create')
                        <button id='boton_cat' type='button' class='btn btn-primary' data-toggle='modal'
                            onclick="SelectPart(2);" data-target='#saveModal' >Cambiar</button>
                    @endcan
                    @can('imagenes.destroy')
                        <button id='botonBCat' type='button' class='btn btn-danger' data-toggle='modal'
                            onclick="SelectPart(2);" data-target='#imageDeleteModal'>Eliminar</button>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    {{--Fin del modal--}}

    {{--BORRAR TODO ESTO--}}
    {{--Modal para disparar el cambio de imagen--}}
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div id="mContent" class="modal-content">
{{--                cabezera del modal--}}
                <div id="mHead" class="modal-header">
                    <h5 class="modal-title mx-auto" >Subir Fotos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
{{--                Cuerpo del modal--}}
                <div id="mBody4" class="modal-body">
                    <div class="mx-auto">
                        <form class="mx-auto" role="form" enctype="multipart/form-data" method="post" action="">
                            <div class="" id="mensaje2"></div>

                            <div class="col-md-12 text-center" id="imagen4" ></div>


                            <div class=" form-group mx-auto m-4 {{$errors->has('name') ? 'has-error' : ''}}">
                                <input type="file" name="image_name4" class="form-control btn btn-secondary" id="ImageFileInput">
                            </div>

                            <div class="form-group text-center">
                                @can('imagenes.create')
                                    <button id="boton_submit4" type="button" onclick="ChangeImage();" class="btn btn-primary">Subir Imagen </button>
                                @endcan
                            </div>
                        </form>
                    </div>
                </div>
{{--                pie del modal--}}
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    {{--Fin del modal--}}
    {{--inicio del modal 2 borrar imagen carusel--}}
    <div class="modal fade" id="imageDeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div id="mContent" class="modal-content">
{{--                cabezera del modal--}}
                <div id="mHead" class="modal-header">
                    <h5 class="modal-title mx-auto">Borrar Imagen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
{{--                Cuerpo del modal--}}
                <div id="mBody" class="modal-body">
                    <div class="" id="mensajeB2">

                    </div>
                    <p class="text-center"> <b> ¿Esta seguro/a que desea borrar la Imagen? </b> </p>
                </div>
{{--                pie del modal--}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    @can('imagenes.destroy')
                        <button id="borrarImagen2" type="button" class="btn btn-danger"
                            onclick="ImageDelete();"> Borrar Imagen </button>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    {{--Fin del modal--}}

    @include('layouts.cargando.cargando')
@stop

@section("script")
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jsF/imagenes.js")!!} 

    <script>
        $("#loading").css('z-index',2060);
    </script>
@endsection