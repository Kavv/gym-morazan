
<div class="col-md-4 mt-3">
    <div class="card bg-reservation">
        <div class="card-body text-center">
            <h5 class="card-title">{{$days[$s->day]}}</h5>
            <div class="row mb-2">
                <div class="col-md-6">
                    <input type="time" class="form-control text-center" value="{{$res[$index]->begin}}" disabled>
                </div>
                <div class="col-md-6">
                    <input type="time" class="form-control text-center" value="{{$res[$index]->end}}" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="" class="font-weight-bold m-0">Entrenador</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">{{$res[$index]->name . ' ' . $res[$index]->last_name}}</label>
                </div>
            </div>
            <button type="button" data-toggle="modal" data-target="#deleteModal" data-dia="{{$s->day}}"
            class="btn btn-danger" data-value="{{$res[$index]->res}}">Eliminar Reserva</button>
        </div>
    </div>
</div>  