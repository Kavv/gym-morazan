<?php $index = 0;
$aux_discipline = ""; ?>
@foreach ($lessons as $lesson)
    @if($aux_discipline != $lesson->discipline_id)
        <div class="col-md-12 text-light mt-3 titulos">
            <h3>{{$lesson->discipline}}</h3>
        </div>
        <?php $aux_discipline = $lesson->discipline_id;?>
    @endif
    <div class="col-md-4 mt-3">
        <div class="card bg-base">
            <div class="card-body text-center">
                <h5 class="card-title">{{$days[$lesson->day]}}</h5>
                <div class="row mb-2">
                    <div class="col-md-6">
                        <input type="time" class="form-control text-center" value="{{$lesson->begin}}" disabled>
                    </div>
                    <div class="col-md-6">
                        <input type="time" class="form-control text-center" value="{{$lesson->end}}" disabled>
                    </div>
                </div>
                
                <button type="button" data-toggle="modal" data-target="#m-detail" 
                data-dia="{{$lesson->day}}" data-t_dia="{{$days[$lesson->day]}}"
                data-disciplina="{{$lesson->discipline}}" value="{{$lesson->id}}"
                data-begin="{{$lesson->begin}}" data-end="{{$lesson->end}}"
                class="btn btn-secondary edit" onclick="show_athletes(this);" >Ver alumnos</button>

                <button type="button" class="btn btn-success edit" onclick="end_lesson(this);" 
                value="{{$lesson->calendar}}">Lección Finalizada</button>
            </div>
        </div>
    </div>  

@endforeach