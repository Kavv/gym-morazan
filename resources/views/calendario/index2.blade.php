@extends('layouts.dashboard')
    {!!Html::style("js/fullcalendar/fullcalendar.min.css")!!}
@section('content')
      <div class="col-12 pt-4">
          {!! $calendar->calendar() !!}
      </div>
      <!-- Modal -->
      <div class="modal fade" id="infore" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Informacion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="data" action="/reservacion/proforma" method="POST" target="_blank">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="id">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        <!--UNA VEZ TERMINADA LA CLASE DE IS REGRESAR LOS FORMULARIOS A SU TAMAñO GRANDE E IMPORTARLO AQUI-->

                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Cliente:')!!}
                                    {!!Form::label(null,null,['id' => 'cliente','class'=>'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Cedula:')!!}
                                    {!!Form::label(null,null,['id' => 'cedula','class'=>'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Direccion:')!!}
                                    {!!Form::label(null,null,['id' => 'direccion','class'=>'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!!Form::label('Fecha:')!!}
                                    {!!Form::label(null,null,['id' => 'fecha','class'=>'badge badge-pill badge-info', 'style'=>'font-size: 90%;'])!!}
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4>Descripcion</h4>
                                </div>
                            </div>
                        </div>


                        <div class="row mb-2">
                            <!-- botones para seleccionar las factura-->
                            <div class="input-group">                
                                <div class="col col-md-12" style="display:flex; justify-content:center">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-info fa fa-angle-left" onclick="cambiofac(-1);" type="button"></button>
                                    </div>
                                    <span class="input-group-text"><b id="numfac">Factura #1</b></span>
                                    <div class="input-group-append">
                                        <button class="btn btn-info fa fa-angle-right" onclick="cambiofac(1);" type="button"></button>
                                        <button class="btn btn-info fa fa-print" style="margin-left: 1px;" onclick="" type="submit"></button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row text-center">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table class="table text-center">
                                        <thead>
                                            <tr>
                                                <th>Cantidad</th>
                                                <th>Descripcion</th>
                                                <th>P/Unitario</th>
                                                <th>Días</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbdes">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="facactual" name="facactual" value="0">
                        <input type="hidden" id="reservation" name="reservation">
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <a href="#" id="proforma" class="btn btn-success" target="_blank">Proforma</a>
                        @can('reservacion.edit')
                        <button type="button" class="btn btn-primary" onclick="editar();">Editar</button>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @include('layouts.cargando.cargando')
@stop
@section('script')
    {!!Html::script("js/fullcalendar/lib/moment.min.js")!!} 
    {!!Html::script("js/fullcalendar/fullcalendar.min.js")!!} 
    {!!Html::script("js/fullcalendar/locale/es.js")!!}
    {!! $calendar->script() !!}

    <script>
        var calendar = new FullCalendar.Calendar(calendar, {
           locale: 'es'
        });
        var resinfo=new Object();
        var des = new Object();
        var reserva;
        var facturas;
        $("#loading").css('z-index',1060);
        function cargardatos(evento)
        {
            //limpiamos la descripcion
            $("#tbdes").empty();
            //Id de la reservación
            reserva = evento;
            //Bloqueamos la pantalla
            $("#editar").attr('disabled',true);
            $("#loading").css('display','block');
            
            var ruta = '/reservacion/show/'+evento;
            
            $.get(ruta, function(res){
                
                var data_reservation = res['reservacion'];
                var beginDate = data_reservation.begin_date;
                var endDate = data_reservation.end_date;

                //rellenamos los datos de la reservacion
                $("#cliente").text(res['cliente'].name+" "+res['cliente'].last_name);
                $("#cedula").text(res['cliente'].dni);
                $("#direccion").text(data_reservation.event_address);
                $("#fecha").text(beginDate + " / " + endDate);

                //Empezamos a realizar las operaciones para generar la descripcion de la reservacion
                var subTotal=0;//Variable que almacenara el precio total
                var unitPrice, quantity;

                var d_item = res['d_items'];
                var d_menu = res['d_menu'];
                var l_items = d_item.length;
                var l_menu = d_menu.length;
                var t_items = l_items + l_menu;
                var index_i = 0, index_m = 0;
                
                var index_row = 1;
                var pos_invoice = 0;
                var html = "";
                facturas = [];
                for(i = 0; i < t_items; i++)
                {
                    if(index_i < l_items)
                    {
                        if(d_item[index_i].index == i + 1)
                        {
                            quantity = d_item[index_i].qu;
                            unitPrice = d_item[index_i].up;
                            day = d_item[index_i].day;
                            //Agregamos una fila por cada descripcion
                            html +=
                                '<tr>'+
                                    '<td>' + quantity + '</td>'+
                                    '<td class="text-left">' + d_item[index_i].name + '</td>'+
                                    '<td>' + unitPrice + '</td>'+
                                    '<td>' + day + '</td>'+
                                    '<td>' + d_item[index_i].amount   + '</td>'+
                                '</tr>';
                            subTotal += d_item[index_i].amount * 1;
                            index_i++;
                        }
                    }
                    if(index_m < l_menu)
                    {
                        if(d_menu[index_m].index == i + 1)
                        {
                            quantity = d_menu[index_m].qu;
                            unitPrice = d_menu[index_m].up;
                            day = res['d_menu'][index_m].day;
                            html +=
                                '<tr>'+
                                    '<td>' + quantity + '</td>'+
                                    '<td class="text-left">' + d_menu[index_m].description + '</td>'+
                                    '<td>' + unitPrice + '</td>'+
                                    '<td>' + day + '</td>'+
                                    '<td>' + d_menu[index_m].amount + '</td>'+
                                '</tr>';
                            subTotal += d_menu[index_m].amount * 1;
                            index_m++;
                        }
                    }
                    index_row++;
                    //Si el indice de la fila es mayo a la cantidad de filas establecida en la reserva entonces
                    if(index_row > data_reservation.invoice_row)
                    {
                        //Definimos el detalle final de la factura recien acabada

                        // Almacenamos el iva                
                        var iva = res['reservacion'].tax * 1;
                        var descuento = res['reservacion'].discount * 1;
                        // Si iva es igual a 0 significa que no se aplico iva
                        
                        var temp_iva, temp_descuento, total;

                        if(iva == 0 && descuento == 0)
                        {
                            html += '<tr><td></td><td></td><td></td><td><b>Gran Total<b></td><td>'+subTotal+'</td></tr>';
                        }
                        if(iva != 0 && descuento == 0)
                        {   
                            temp_iva = (subTotal*iva).toFixed(2);
                            total = ((subTotal*1)+(temp_iva*1)).toFixed(2);
                            html += "</tr >"+
                            '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+subTotal+'</td> </tr>'+
                            '<tr id="tfiva"> <td></td> <td></td> <td> </td><td><b>IVA:</b></td><td>'+temp_iva+'</td> </tr>'+
                            '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+total+'</td> </tr>';
                        }
                        if(iva == 0 && descuento != 0)
                        {
                            temp_descuento = (subTotal*descuento).toFixed(2);
                            total = ((subTotal*1)-(temp_descuento*1)).toFixed(2);
                            html += "</tr >"+
                            '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+subTotal+'</td> </tr>'+
                            '<tr id="tfdecuento"> <td></td> <td></td> <td></td> <td> <b>Descuento:('+ (descuento*100) +'%)</b></td><td>-'+temp_descuento+'</td> </tr>'+
                            '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+total+'</td> </tr>';  
                        }
                        if(iva != 0 && descuento != 0)
                        {
                            temp_descuento = (subTotal*descuento).toFixed(2);
                            temp_iva = ((subTotal-(temp_descuento*1))*iva).toFixed(2);
                            total = ((subTotal*1)-(temp_descuento*1)+(temp_iva*1)).toFixed(2);
                            html += "</tr >"+
                            '<tr id="tfsubtotal"> <td></td> <td></td> <td></td> <td> <b>Sub Total:</b></td><td>'+subTotal+'</td> </tr>'+
                            '<tr id="tfdecuento"> <td></td> <td></td> <td></td> <td> <b>Descuento:('+ (descuento*100) +'%)</b></td><td>-'+temp_descuento+'</td> </tr>'+
                            '<tr id="tfiva"> <td></td> <td></td> <td> </td><td><b>IVA:</b></td><td>'+temp_iva+'</td> </tr>'+
                            '<tr id="tftotal"> <td></td> <td></td> <td> </td><td><b>Gran Total:</b></td><td>'+total+'</td> </tr>';
                        }
                        
                        //Se agrega un nuevo espacio
                        facturas.push([""]);
                        //Se almacena el html de la factura actual
                        facturas[pos_invoice][0] = html;
                        //Se vacia la variable html que contendra el html de la siguiente factura
                        html = "";
                        //El indice de filas actuales se reinicia
                        index_row = 0;
                        //Aumenta el indice que controla las facturas agregadas
                        pos_invoice++;
                    }
                }
                //Se agrega un nuevo espacio
                facturas.push([""]);
                facturas[pos_invoice][0] = html;
                //Mostramos la primer factura
                $("#tbdes").append(facturas[0][0]);
                $("#reservation").val(reserva);

                $("#editar").attr('disabled',false);
                $("#loading").css('display','none');
            });
            

            $("#infore").modal('show');
        }
        @can('reservacion.edit')
        function editar()
        {
            location.href="/reservacion/"+reserva+"/edit";
        }
        @endcan

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        facactual=0;
        function cambiofac(valor)
        {
            temp = facactual + valor * 1;
            if(temp >= 0 && temp < facturas.length)
            {
                facactual = temp;
                tf = $("#tbdes");
                tf.empty();
                tf.append(facturas[facactual][0]);
                //eliminardetalles();
                $("#numfac").text("Factura #"+(facactual+1));
                $("#facactual").val(facactual);
            }
        }
        $('#infore').on('hidden.bs.modal', function (e) {
            facactual = 0;
            $("#numfac").text("Factura #"+(facactual+1));
            $("#facactual").val(facactual);
        });
    </script>
@stop