@extends('layouts.dashboard')
@section('content')

    @include('layouts.cargando.cargando')

    {!!Html::style("css/gijgo2/css/gijgo.css")!!}
    
    <div class="card" id="main" style="display:none;">
        <h4 class="card-header text-center">REPORTES</h4>
        <div class="card-body">
            <div class="row mt-5 justify-content-center d-flex">
                <div class="col-md-6">
                    <label>Que tipo de reporte desea realizar?</label>
                    <select id="reporte" class="custom-select" onchange="Reporte('option');">
                        <option ></option>
                        <option value="clientes_agregados">Clientes Agregados</option>
                        <option value="clientes_eliminados">Clientes Eliminados</option>
                        <option value="personal_agregados">Personal Agregados</option>
                        <option value="personal_eliminados">Personal Eliminados</option>
                        <option value="reservaciones_agregadas">Reservaciones Agregadas</option>
                        <option value="eventos">Eventos</option>
                        <option value="ingresos">Ingresos</option>
                        <option value="inventario">Inventario</option>
                        <option value="art_mas_usado">Articulo mas usado</option>
                        <option value="buff_mas_usado">Buffet mas usado</option>
                    </select>
                </div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-3">
                    <label>Fecha de inicio</label>
                    <input id="start" class="datepicker border border-warning" name="start"   placeholder='año-mes-dia' readonly  onchange="Reporte('start');"/> 
                </div>
                <div class="col-md-3">
                    <label>Fecha de fin</label>
                    <input id="end" class="datepicker border border-warning" name="end"   placeholder='año-mes-dia' readonly  onchange="Reporte('end');"/> 
                </div>
            </div>

            <div class="row mt-5 justify-content-center">
                <div class="col-md-3 text-center btn-resize mt-1" style="transition: .4s;">
                    <a id="pdf" href="" class="btn btn-danger" target="_blank">
                        <i class="fa fa-file-pdf-o" aria-hidden="true">
                            PDF
                        </i>
                    </a>
                </div>
                <div id="p-btn-graphic" class="col-md-3 text-center btn-resize mt-1" style="transition: .4s; display:none;">
                    <a id="btn-graphic" href="" class="btn btn-info">
                        <i class="fa fa-file-pdf-o" aria-hidden="true">
                            Graficar
                        </i>
                    </a>
                </div>
                <div class="col-md-3 text-center btn-resize mt-1" style="transition: .4s;">
                    <a id="excel" href="" class="btn btn-success">
                        <i class="fa fa-file-excel-o" aria-hidden="true">
                            Excel
                        </i>
                    </a>
                </div>
            </div>
        </div>

        <div id="container" style="width:100%; height:400px;"></div>



    </div>
    @section('script')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    {!!Html::script("js/gijgo2/js/gijgo.js")!!}

    {!!Html::script("https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js")!!} 
    {!!Html::script("https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js")!!} 
    {!!Html::script("js/jskevin/tiposmensajes.js")!!} 
    {!!Html::script("js/jskevin/kavvdt.js")!!}   


    <script>



        var config = {
            chart: {
            renderTo: "container",
            },
            
            lang: {
                months: [
                    'Enero', 'Febrero', 'Marzo', 'Abril',
                    'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septembre', 'Octobre', 'Novembre', 'Décembre'
                ],
                weekdays: [
                    'Domingo', 'Lunes', 'Martes', 'Miercoles',
                    'Jueves', 'Viernes', 'Sabado'
                ],
                downloadCSV: "Descargar CSV",
                downloadJPEG: "Descargar JPEG image",
                downloadPDF: "Descargar PDF document",
                downloadPNG: "Descargar PNG image",
                downloadSVG: "Descargar SVG vector image",
                downloadXLS: "Descargar XLS",
                loading: "Cargando...",
                noData: "No hay informacion que mostrar",
                openInCloud: "Abrir en Highcharts",
                printChart: "Imprimir Grafica",
                shortMonths:["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
                viewData: "Ver tabla",
                viewFullscreen: "Ver pantalla completa",
            },
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ['viewFullscreen', 'printChart', 'separator', 'downloadPDF', 'downloadPNG','downloadJPEG', 'downloadSVG', 'separator', 'downloadCSV', 'downloadXLS']
                    }
                }
            },
            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },

            yAxis: {
                title: {
                    text: 'Cantidad'
                },/*,
                labels: {
                formatter: function() {
                    return this.value + ' %';
                }*/
            },
            xAxis: {
                title: {
                    text: 'Fechas'
                },
                categories:[],
                tickInterval: 1,
                layout: 'vertical',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            /*
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },*/

            series: [{
                name: '',
                data: [],
                extra: [],
                type: 'column',
            },
            {
                name: '',
                data: [],
                extra: [],
                type: 'spline',
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            },
            tooltip: {
            },
        };
        var pru;
        var diagrama;
        $(document).ready(function(){
            $("#main").css('display','flex');
        });

        var $start = $("#start");
        var $end = $("#end");
        var temp_start, temp_end;
        function Reporte(input = null)
        {
            if($start.val() != "" && $end.val() != "")
            {
                if($start.val() != temp_start && input == "start" || input == "option" || input == "end" && $end.val() != temp_end)
                {

                // if(input == "start")
                        temp_start = $start.val();
                    //if(input == "end")
                        temp_end = $end.val();
                    var caso;
                    var ruta = "";
                    var ruta2 = "";
                    if($("#reporte").val() != "ingresos")
                        ShowButtonReport(false);

                    switch ($("#reporte").val()) {
                        case "clientes_agregados":
                            ruta =  "/reportes/clientesAgregados/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excel/"+$start.val()+ "/" +$end.val()+ "/clientes_agregados";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //standard(ruta);
                        break;
                        case "clientes_eliminados":
                            ruta =  "/reportes/clientesEliminados/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excel/"+$start.val()+ "/" +$end.val()+ "/clientes_eliminados";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //standard(ruta);
                        break;
                        case "personal_agregados":
                            ruta =  "/reportes/personalAgregados/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excel/"+$start.val()+ "/" +$end.val()+ "/personal_agregados";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //standard(ruta);
                        break;
                        case "personal_eliminados":
                            ruta =  "/reportes/personalEliminados/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excel/"+$start.val()+ "/" +$end.val()+ "/personal_eliminados";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //standard(ruta);
                        break;
                        case "reservaciones_agregadas":
                            ruta =  "/reportes/reservacionesAgregadas/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelReser/"+$start.val()+ "/" +$end.val()+ "/reservaciones_agregadas";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //standard(ruta);
                        break;
                        case "eventos":
                            ruta =  "/reportes/eventos/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelEven/"+$start.val()+ "/" +$end.val()+ "/eventos";
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            //allReservations(ruta);
                        break;
                        case "ingresos":
                            ruta =  "/reportes/ingresos/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelReser/"+$start.val()+ "/" +$end.val()+ "/ingresos";
                            //ruta_grafica =  "/graficas/ingresos/"+$start.val()+"/"+$end.val();
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            ShowButtonReport();
                            //standard(ruta_grafica);
                        break;
                        case "inventario":
                            ruta =  "/reportes/inventario/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelInven/"+$start.val()+ "/" +$end.val()+ "/inventario";
                            //ruta_grafica =  "/graficas/ingresos/"+$start.val()+"/"+$end.val();
                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            // ShowButtonReport();
                            //standard(ruta_grafica);
                            break;
                        case "art_mas_usado":
                            ruta = "/reportes/artMasUsado/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelArtMasUsado/"+$start.val()+ "/" +$end.val()+ "/artMasUsado";

                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            break;
                        case "buff_mas_usado":
                            ruta = "/reportes/buffMasUsado/"+$start.val()+"/"+$end.val();
                            ruta2 = "/reportes/excelBuffMasUsado/"+$start.val()+ "/" +$end.val()+ "/buffMasUsado";

                            $("#pdf").attr("href", ruta);
                            $("#excel").attr("href", ruta2);
                            break;
                        default:
                            $("#pdf").attr("href", "");
                            $("#excel").attr("href", "");
                        break;  
                    }
                }
            }
        }
        
        $("#pdf").click(function(e){
            if($("#pdf").attr("href") == "")
                e.preventDefault(); 
        });

        $("#excel").click(function(e){
            if($("#excel").attr("href") == "")
                e.preventDefault();
        });

        $("#btn-graphic").click(function(e){
            if($("#btn-graphic").attr("href") == "")
                e.preventDefault();
            if(($("#reporte").val() != "ingresos"))
                standard("/graficas/ingresos/"+$start.val()+"/"+$end.val());
            else
                standard("/graficas/ingresos/"+$start.val()+"/"+$end.val());
        });

        function ShowButtonReport(value = true)
        {
            if(value == true)
            {
                $(".btn-resize").removeClass("col-md-3");
                $(".btn-resize").addClass("col-md-2");
                $("#p-btn-graphic").css("display","block");
            }
            else
            {
                $(".btn-resize").removeClass("col-md-2");
                $(".btn-resize").addClass("col-md-3");
                $("#p-btn-graphic").css("display","none");
                $("#btn-graphic").attr("href", "");
            }
        }
        
        //Cambiar esto a una personalizada con dos graficas en una, una es de barra tal como esta ahorita y la otra es de ganancia y sera por puntos
        function standard(ruta)
        {
            if(ruta != "")
            {
                $.get(ruta, function(res){
                    if(res.state != false)
                    {
                        if(diagrama)
                        {
                            diagrama.destroy();
                            //indice 0 es Reservaciones Agregadas
                            config.series[0].data=[];
                            //indice 1 es Ingresos
                            config.series[1].data=[];
                            config.xAxis.categories=[];
                            config.series[0].name = "";
                            config.series[1].name = "";
                            config.title.text = "";
                            config.title.subtitle = "";
                            delete config.tooltip.formatter;
                        }
                        config.series[0].name = res.name[0];
                        config.series[1].name = res.name[1];
                        config.title.text = res.title;
                        config.title.subtitle = "Desde: "+ $start.val() + " Hasta:" + $end.val();
                        for(data in res.data)
                        {
                            console.log(res);
                            config.series[0].data.push(res.data[data].cantidad);
                            config.xAxis.categories.push(data);
                        }
                        for(data in res.ingresos)
                        {
                            console.log(res);
                            config.series[1].data.push(res.ingresos[data].ingreso);
                            config.xAxis.categories.push(data);
                        }
                        diagrama = new Highcharts.chart(config);
                    }
                });
            }
        }

        $('#start').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });
        $('#end').datepicker({
            locale: 'es-es',
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            header: true,
        });

    </script>


@endsection
@stop
